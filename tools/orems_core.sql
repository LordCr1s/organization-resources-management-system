-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 11:45 AM
-- Server version: 10.3.12-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orems_core`
--

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE `activation_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `expire` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activation_codes`
--

INSERT INTO `activation_codes` (`id`, `activation_code`, `department_id`, `role_id`, `expire`, `created_at`, `updated_at`) VALUES
(2, 'OREMS-1-4-1552238691', 1, 3, 'No', '2019-03-10 14:24:51', '2019-03-10 14:24:51'),
(3, 'OREMS-1-4-1552238692', 1, 3, 'No', '2019-03-10 14:24:52', '2019-03-10 14:24:52'),
(4, 'OREMS-1-4-1552739426', 1, 3, 'No', '2019-03-16 09:30:26', '2019-03-16 09:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `category_department`
--

CREATE TABLE `category_department` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_department`
--

INSERT INTO `category_department` (`id`, `name`, `description`, `organization_id`, `created_at`, `updated_at`) VALUES
(3, 'Administration', 'adminstration', 1, NULL, NULL),
(4, 'Computer', 'computer', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_department_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `category_department_id`, `created_at`, `updated_at`) VALUES
(1, 'computer', 'computer', 4, NULL, NULL),
(2, 'pmu', 'pmu', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department_section`
--

CREATE TABLE `department_section` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `department_user`
--

CREATE TABLE `department_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_10_18_165818_create_organizations_table', 1),
(3, '2018_10_18_182511_create_organization_session_table', 1),
(4, '2018_10_18_183451_create_session_term_table', 1),
(5, '2018_10_18_184252_create_category_department_table', 1),
(6, '2018_10_18_185029_create_departments_table', 1),
(7, '2018_10_18_185200_create_users_table', 2),
(8, '2018_10_18_185629_create_department_section_table', 2),
(9, '2018_11_27_114144_create_roles_table', 2),
(10, '2018_11_27_114442_create_permissions_table', 2),
(11, '2018_11_27_115640_create_permission_role_table', 2),
(12, '2018_11_30_163509_create_role_user_table', 2),
(13, '2018_11_30_170433_create_activation_codes_table', 2),
(14, '2018_11_30_211030_create_department_user_table', 2),
(15, '2018_12_19_135126_create_processes_table', 2),
(16, '2018_12_19_135228_create_state_type_table', 2),
(17, '2018_12_19_135434_create_states_table', 2),
(18, '2018_12_19_142215_create_transitions_table', 2),
(19, '2018_12_19_142513_create_action_type_table', 2),
(20, '2018_12_19_142553_create_actions_table', 2),
(21, '2018_12_19_142747_create_targets_table', 2),
(22, '2018_12_19_142803_create_action_target_table', 2),
(23, '2018_12_19_142831_create_action_transition_table', 2),
(25, '2019_03_12_153754_create_templates_table', 3),
(26, '2019_03_12_154030_create_request_templates_table', 3),
(27, '2018_11_30_165429_create_user_permissions_table', 4),
(28, '2018_11_30_165429_create_permission_user_table', 5),
(29, '2019_03_16_163209_create_processes_table', 5),
(30, '2019_03_16_165810_create_states_table', 5),
(31, '2019_03_16_170722_create_transitions_table', 5),
(32, '2019_03_16_170800_create_transition_actions_table', 5),
(33, '2019_03_16_170819_create_transition_roles_table', 5),
(34, '2019_03_16_171116_create_request_transition_statuses_table', 1),
(35, '2019_03_16_171152_create_request_transition_dates_table', 1),
(36, '2019_03_11_102622_create_projects_table', 1),
(37, '2019_03_18_182113_create_notifications_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `organization_name`, `address`, `email`, `telephone`, `fax`, `created_at`, `updated_at`) VALUES
(1, 'DIT', 'P.O. Box 9528', 'omakei96@gmail.com', '+255656699895', '+255656699895', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization_session`
--

CREATE TABLE `organization_session` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_at` date NOT NULL,
  `end_at` date NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'activate-user', '', NULL, NULL),
(2, 'deactivate-user', '', NULL, NULL),
(3, 'suspend-user', '', NULL, NULL),
(4, 'add-user-role', '', NULL, NULL),
(5, 'remove-user-role', '', NULL, NULL),
(6, 'view-users', '', NULL, NULL),
(7, 'view-tb', '', NULL, NULL),
(8, 'view-ec', '', NULL, NULL),
(9, 'view-ac', '', NULL, NULL),
(10, 'delete-ac', '', NULL, NULL),
(11, 'add-tb', '', NULL, NULL),
(12, 'remove-tb', '', NULL, NULL),
(13, 'add-ec', '', NULL, NULL),
(14, 'remove-ec', '', NULL, NULL),
(15, 'view-user-manager', '', NULL, NULL),
(16, 'do-all', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 6, 1, NULL, NULL),
(2, 5, 16, NULL, NULL),
(3, 5, 6, NULL, NULL),
(4, 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'PROCUREMENT REQUISTION', '', NULL, NULL),
(2, 'STORE ISSUE NOTE', '', NULL, NULL),
(3, 'GOOD RECEIVED NOTE', '', NULL, NULL),
(4, 'STORE REQUISITION', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `serial_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `process_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `serial_number`, `process_id`, `state_id`, `user_id`, `data`, `created_at`, `updated_at`) VALUES
(1, 'SNo. 1-1552887145', 1, 1, 4, '{\"data\":\"{\\\"employees\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 02:32:25', '2019-03-18 02:32:25'),
(2, 'SNo. 1-051942', 1, 1, 4, '{\"data\":\"{\\\"employees\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 02:42:19', '2019-03-18 02:42:19'),
(3, 'SNo. 1-085223', 1, 1, 4, '{\"data\":\"{\\\"omakei\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 05:23:52', '2019-03-18 05:23:52'),
(4, 'SNo. 1-081628', 1, 1, 4, '{\"data\":\"{\\\"omakei\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 05:28:16', '2019-03-18 05:28:16'),
(5, 'SNo. 1-083033', 1, 1, 4, '{\"data\":\"{\\\"omakei\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 05:33:30', '2019-03-18 05:33:30'),
(6, 'SNo. 1-080138', 1, 1, 4, '{\"data\":\"{\\\"omakei\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 05:38:01', '2019-03-18 05:38:01'),
(7, 'SNo. 1-085139', 1, 1, 4, '{\"data\":\"{\\\"omakei\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 05:39:51', '2019-03-18 05:39:51'),
(8, 'SNo. 1-080442', 1, 1, 4, '{\"data\":\"{\\\"omakei\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 05:42:04', '2019-03-18 05:42:04'),
(9, 'SNo. 1-075103', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 16:03:51', '2019-03-18 16:03:51'),
(10, 'SNo. 1-072742', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 16:42:27', '2019-03-18 16:42:27'),
(11, 'SNo. 1-082101', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:01:22', '2019-03-18 17:01:22'),
(12, 'SNo. 1-083343', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:43:33', '2019-03-18 17:43:33'),
(13, 'SNo. 1-082544', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:44:25', '2019-03-18 17:44:25'),
(14, 'SNo. 1-082247', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:47:22', '2019-03-18 17:47:22'),
(15, 'SNo. 1-084447', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:47:44', '2019-03-18 17:47:44'),
(16, 'SNo. 1-080948', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:48:09', '2019-03-18 17:48:09'),
(17, 'SNo. 1-084348', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:48:43', '2019-03-18 17:48:43'),
(18, 'SNo. 1-084449', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 17:49:44', '2019-03-18 17:49:44'),
(19, 'SNo. 1-095303', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 18:03:53', '2019-03-18 18:03:53'),
(20, 'SNo. 1-091818', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 18:18:18', '2019-03-18 18:18:18'),
(21, 'SNo. 1-092120', 1, 1, 4, '{\"data\":\"{\\\"pase\\\":[\\n  { \\\"firstName\\\":\\\"John\\\", \\\"lastName\\\":\\\"Doe\\\" },\\n  { \\\"firstName\\\":\\\"Anna\\\", \\\"lastName\\\":\\\"Smith\\\" },\\n  { \\\"firstName\\\":\\\"Peter\\\", \\\"lastName\\\":\\\"Jones\\\" }\\n]}\",\"_token\":\"XSRF-TOKEN=eyJpdiI6IktCUzZBeXBVd3o4UmtjRWFBUzFGT1E9PSIsInZhbHVlIjoidEM0cFlxdlNOcFZvU2JkOG95QkJqQ3loeWxyeXhWdHA1YlJBNlMybzlzMmR5OTNvZ081dk12QkN5UVJ0ZTlMVCIsIm1hYyI6Ijg3MmI4ZDY5NTM5NzMwZGY1MDVhNTllMGNmMTkxMDE4ODc3ZWE2MjQyNzM1YTM5MmVmNTA3YzBmYjVjOTBhMzQifQ%3D%3D; orems_session=eyJpdiI6IjlSR1hwblA5S2NWREtTNUs5MG9SVkE9PSIsInZhbHVlIjoidk9IdEVWN2hEMkNIelVCVnFLTnhTRWNlamtOYUIwVTlJTzUwbWpVODlQMnZQU2NCbXNmUHBUaCtKV2ErOTFndyIsIm1hYyI6IjA4ZDhjNTExODgwOWRjZWUyNjc5NWU0NjdjM2JjY2ZmM2Y3ZTdlMThkNTRkNDZmYjFlYmZmMDY4NWY5YjVkODIifQ%3D%3D\"}', '2019-03-18 18:20:21', '2019-03-18 18:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `request_transition_dates`
--

CREATE TABLE `request_transition_dates` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `transition_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_transition_dates`
--

INSERT INTO `request_transition_dates` (`id`, `request_id`, `transition_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, '2019-03-18 05:42:04', '2019-03-18 05:42:04'),
(2, 9, 1, '2019-03-18 16:03:52', '2019-03-18 16:03:52'),
(3, 10, 1, '2019-03-18 16:42:29', '2019-03-18 16:42:29'),
(4, 11, 1, '2019-03-18 17:01:23', '2019-03-18 17:01:23'),
(5, 12, 1, '2019-03-18 17:43:33', '2019-03-18 17:43:33'),
(6, 13, 1, '2019-03-18 17:44:25', '2019-03-18 17:44:25'),
(7, 14, 1, '2019-03-18 17:47:23', '2019-03-18 17:47:23'),
(8, 15, 1, '2019-03-18 17:47:44', '2019-03-18 17:47:44'),
(9, 16, 1, '2019-03-18 17:48:09', '2019-03-18 17:48:09'),
(10, 17, 1, '2019-03-18 17:48:44', '2019-03-18 17:48:44'),
(11, 18, 1, '2019-03-18 17:49:44', '2019-03-18 17:49:44'),
(12, 19, 1, '2019-03-18 18:03:53', '2019-03-18 18:03:53'),
(13, 20, 1, '2019-03-18 18:18:20', '2019-03-18 18:18:20'),
(14, 21, 1, '2019-03-18 18:20:21', '2019-03-18 18:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `request_transition_statuses`
--

CREATE TABLE `request_transition_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `transition_id` int(10) UNSIGNED NOT NULL,
  `name` enum('Accepted','Rejected','None') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'None',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_transition_statuses`
--

INSERT INTO `request_transition_statuses` (`id`, `request_id`, `transition_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 'None', '2019-03-18 05:42:04', '2019-03-18 05:42:04'),
(2, 9, 1, 'None', '2019-03-18 16:03:52', '2019-03-18 16:03:52'),
(3, 10, 1, 'None', '2019-03-18 16:42:27', '2019-03-18 16:42:27'),
(4, 11, 1, 'None', '2019-03-18 17:01:23', '2019-03-18 17:01:23'),
(5, 12, 1, 'None', '2019-03-18 17:43:33', '2019-03-18 17:43:33'),
(6, 13, 1, 'None', '2019-03-18 17:44:25', '2019-03-18 17:44:25'),
(7, 14, 1, 'None', '2019-03-18 17:47:22', '2019-03-18 17:47:22'),
(8, 15, 1, 'None', '2019-03-18 17:47:44', '2019-03-18 17:47:44'),
(9, 16, 1, 'None', '2019-03-18 17:48:09', '2019-03-18 17:48:09'),
(10, 17, 1, 'None', '2019-03-18 17:48:43', '2019-03-18 17:48:43'),
(11, 18, 1, 'None', '2019-03-18 17:49:44', '2019-03-18 17:49:44'),
(12, 19, 1, 'None', '2019-03-18 18:03:53', '2019-03-18 18:03:53'),
(13, 20, 1, 'None', '2019-03-18 18:18:20', '2019-03-18 18:18:20'),
(14, 21, 1, 'None', '2019-03-18 18:20:21', '2019-03-18 18:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `department_id`, `created_at`, `updated_at`) VALUES
(1, 'TB', 'tender board', 2, NULL, NULL),
(2, 'EC', 'evaluation committie', 2, NULL, NULL),
(3, 'HOD', 'head of department', 1, NULL, NULL),
(4, 'PR', 'principal', 2, NULL, NULL),
(5, 'SA', 'supser admin', 1, NULL, NULL),
(6, 'AD', 'admin', 1, NULL, NULL),
(7, 'HOPMU', 'head of pmu', 2, NULL, NULL),
(8, 'PMU-O', 'pmu officer', 2, NULL, NULL),
(9, 'TC', 'technician', 1, NULL, NULL),
(10, 'SK', 'store keeper', 1, NULL, NULL),
(11, 'LE', 'lecture', 1, NULL, NULL),
(12, 'AO', 'acounting officer', 2, NULL, NULL),
(13, 'HF', 'head of finance', 1, NULL, NULL),
(14, 'HUD', 'head of user department', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(3, 5, 4, NULL, NULL),
(4, 13, 4, NULL, NULL),
(5, 7, 6, NULL, NULL),
(6, 14, 7, NULL, NULL),
(7, 12, 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `session_term`
--

CREATE TABLE `session_term` (
  `id` int(10) UNSIGNED NOT NULL,
  `term_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_at` date NOT NULL,
  `end_at` date NOT NULL,
  `organization_session_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `process_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `process_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'START', '', NULL, NULL),
(2, 1, 'CONFRIMATION OF FUND', '', NULL, NULL),
(3, 1, 'SUBMISSION OF THE REQUEST', '', NULL, NULL),
(4, 1, 'RECIEPT OF REQUEST OT PROCURE', '', NULL, NULL),
(5, 1, 'PROCUREMENT AUTHORIZATION', '', NULL, NULL),
(6, 1, 'FINISH', '', NULL, NULL),
(7, 1, 'REJECTED', '', NULL, NULL),
(8, 1, 'CANCEL', '', NULL, NULL),
(9, 3, 'START', '', NULL, NULL),
(10, 2, 'START', '', NULL, NULL),
(11, 4, 'START', '', NULL, NULL),
(12, 3, 'RECEIVED BY', '', NULL, NULL),
(13, 3, 'QUALITY, VERIFIED BY', '', NULL, NULL),
(14, 3, 'APPROVED BY', '', NULL, NULL),
(15, 3, 'FINISH', '', NULL, NULL),
(16, 3, 'CANCEL', '', NULL, NULL),
(17, 3, 'REJECTED', '', NULL, NULL),
(18, 2, 'ISSUED  BY', '', NULL, NULL),
(19, 2, 'RECEIVED BY', '', NULL, NULL),
(20, 2, 'FINISH', '', NULL, NULL),
(21, 2, 'REJECTED', '', NULL, NULL),
(22, 2, 'CANCEL', '', NULL, NULL),
(23, 4, 'PREPARED BY', '', NULL, NULL),
(24, 4, 'PASSED BY', '', NULL, NULL),
(25, 4, 'FINISH', '', NULL, NULL),
(26, 4, 'CANCEL', '', NULL, NULL),
(27, 4, 'REJECTED', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transitions`
--

CREATE TABLE `transitions` (
  `id` int(10) UNSIGNED NOT NULL,
  `current_state_id` int(10) UNSIGNED NOT NULL,
  `next_state_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transitions`
--

INSERT INTO `transitions` (`id`, `current_state_id`, `next_state_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 2, 3, NULL, NULL),
(3, 3, 4, NULL, NULL),
(4, 4, 5, NULL, NULL),
(5, 5, 6, NULL, NULL),
(6, 9, 12, NULL, NULL),
(7, 12, 13, NULL, NULL),
(8, 13, 14, NULL, NULL),
(9, 14, 15, NULL, NULL),
(10, 10, 18, NULL, NULL),
(11, 18, 19, NULL, NULL),
(12, 19, 20, NULL, NULL),
(13, 11, 23, NULL, NULL),
(14, 23, 24, NULL, NULL),
(15, 24, 25, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transition_actions`
--

CREATE TABLE `transition_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `transition_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transition_actions`
--

INSERT INTO `transition_actions` (`id`, `transition_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'ADD VOTE', '', NULL, NULL),
(2, 1, 'ADD PROJECT', '', NULL, NULL),
(3, 1, 'ADD BUDGET', '', NULL, NULL),
(4, 1, 'ADD BALANCE', '', NULL, NULL),
(5, 1, 'ADD SIGNATURE', '', NULL, NULL),
(6, 2, 'ADD SIGNATURE', '', NULL, NULL),
(7, 3, 'ADD SIGNATURE', '', NULL, NULL),
(8, 4, 'ADD SIGNATURE', '', NULL, NULL),
(9, 5, 'ADD SIGNATURE', '', NULL, NULL),
(10, 6, 'ADD SIGNATURE', '', NULL, NULL),
(11, 6, 'ADD ROLE', '', NULL, NULL),
(12, 7, 'ADD SIGNATURE', '', NULL, NULL),
(13, 7, 'ADD ROLE', '', NULL, NULL),
(14, 8, 'ADD SIGNATURE', '', NULL, NULL),
(15, 8, 'ADD ROLE', '', NULL, NULL),
(16, 10, 'ADD SIGNATURE', '', NULL, NULL),
(17, 10, 'ADD ROLE', '', NULL, NULL),
(18, 11, 'ADD SIGNATURE', '', NULL, NULL),
(19, 11, 'ADD ROLE', '', NULL, NULL),
(20, 13, 'ADD SIGNATURE', '', NULL, NULL),
(21, 14, 'ADD SIGNATURE', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transition_roles`
--

CREATE TABLE `transition_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `transition_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transition_roles`
--

INSERT INTO `transition_roles` (`id`, `role_id`, `transition_id`, `created_at`, `updated_at`) VALUES
(1, 13, 1, NULL, NULL),
(2, 14, 2, NULL, NULL),
(3, 7, 3, NULL, NULL),
(4, 12, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` enum('Active','Suspended','Deleted') COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `department_id`, `firstname`, `lastname`, `surname`, `email`, `email_verified_at`, `status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 1, 'Karolann', 'Carroll', 'Corkery', 'precious.dickinson@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'LteVM88hAv', '2019-03-04 08:33:59', '2019-03-10 14:41:00'),
(3, 1, 'Megane', 'Predovic', 'Konopelski', 'nicolas.lupe@example.net', '2019-03-04 08:33:59', 'Deleted', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'qR6bDEiF53', '2019-03-04 08:33:59', '2019-03-10 14:09:39'),
(4, 1, 'Jesse', 'Krajcik', 'Pouros', 'vhomenick@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Qq82nVMMLIIuI0JcFnteukRDwubThUuzxON1jet2A6UWSJl4HWSjXM1WXbo6', '2019-03-04 08:33:59', '2019-03-12 16:20:35'),
(5, 1, 'Maude', 'Hane', 'McClure', 'lewis50@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'WlBZ5PILoK', '2019-03-04 08:34:00', '2019-03-04 08:34:00'),
(6, 1, 'Maximo', 'Predovic', 'Hackett', 'nicholas29@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'SgLOheVbP1', '2019-03-04 08:34:00', '2019-03-04 08:34:00'),
(7, 1, 'Ezekiel', 'Murazik', 'Conn', 'bethel52@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'MEabI3Zm2H', '2019-03-04 08:34:00', '2019-03-04 08:34:00'),
(8, 1, 'Alice', 'Ankunding', 'Klein', 'vluettgen@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'jUKVTT82v0', '2019-03-04 08:34:00', '2019-03-04 08:34:00'),
(9, 1, 'Gennaro', 'Halvorson', 'Streich', 'durward89@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'FYrBGwcXnt', '2019-03-04 08:34:00', '2019-03-04 08:34:00'),
(10, 1, 'Destiney', 'Klein', 'Sawayn', 'bosco.kaya@example.org', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '8WNvCJ9ykJ', '2019-03-04 08:34:00', '2019-03-04 08:34:00'),
(11, 1, 'Billie', 'VonRueden', 'Kub', 'sigmund.dubuque@example.com', '2019-03-04 08:33:59', 'Active', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'knNEt6qo4u', '2019-03-04 08:34:00', '2019-03-04 08:34:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_codes_department_id_foreign` (`department_id`),
  ADD KEY `activation_codes_role_id_foreign` (`role_id`);

--
-- Indexes for table `category_department`
--
ALTER TABLE `category_department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_department_organization_id_foreign` (`organization_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departments_category_department_id_foreign` (`category_department_id`);

--
-- Indexes for table `department_section`
--
ALTER TABLE `department_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_section_department_id_foreign` (`department_id`);

--
-- Indexes for table `department_user`
--
ALTER TABLE `department_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_user_department_id_foreign` (`department_id`),
  ADD KEY `department_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_session`
--
ALTER TABLE `organization_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_session_organization_id_foreign` (`organization_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_user_id_foreign` (`user_id`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_transition_dates`
--
ALTER TABLE `request_transition_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_transition_statuses`
--
ALTER TABLE `request_transition_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_department_id_foreign` (`department_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `session_term`
--
ALTER TABLE `session_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_term_organization_session_id_foreign` (`organization_session_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_process_id_foreign` (`process_id`);

--
-- Indexes for table `transitions`
--
ALTER TABLE `transitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transitions_current_state_id_foreign` (`current_state_id`),
  ADD KEY `transitions_next_state_id_foreign` (`next_state_id`);

--
-- Indexes for table `transition_actions`
--
ALTER TABLE `transition_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transition_actions_transition_id_foreign` (`transition_id`);

--
-- Indexes for table `transition_roles`
--
ALTER TABLE `transition_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transition_roles_role_id_foreign` (`role_id`),
  ADD KEY `transition_roles_transition_id_foreign` (`transition_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_department_id_foreign` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activation_codes`
--
ALTER TABLE `activation_codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category_department`
--
ALTER TABLE `category_department`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `department_section`
--
ALTER TABLE `department_section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department_user`
--
ALTER TABLE `department_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organization_session`
--
ALTER TABLE `organization_session`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `request_transition_dates`
--
ALTER TABLE `request_transition_dates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `request_transition_statuses`
--
ALTER TABLE `request_transition_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `session_term`
--
ALTER TABLE `session_term`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `transitions`
--
ALTER TABLE `transitions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `transition_actions`
--
ALTER TABLE `transition_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `transition_roles`
--
ALTER TABLE `transition_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD CONSTRAINT `activation_codes_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `activation_codes_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_department`
--
ALTER TABLE `category_department`
  ADD CONSTRAINT `category_department_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_category_department_id_foreign` FOREIGN KEY (`category_department_id`) REFERENCES `category_department` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `department_section`
--
ALTER TABLE `department_section`
  ADD CONSTRAINT `department_section_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `department_user`
--
ALTER TABLE `department_user`
  ADD CONSTRAINT `department_user_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `department_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `organization_session`
--
ALTER TABLE `organization_session`
  ADD CONSTRAINT `organization_session_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `session_term`
--
ALTER TABLE `session_term`
  ADD CONSTRAINT `session_term_organization_session_id_foreign` FOREIGN KEY (`organization_session_id`) REFERENCES `organization_session` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_process_id_foreign` FOREIGN KEY (`process_id`) REFERENCES `processes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transitions`
--
ALTER TABLE `transitions`
  ADD CONSTRAINT `transitions_current_state_id_foreign` FOREIGN KEY (`current_state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transitions_next_state_id_foreign` FOREIGN KEY (`next_state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transition_actions`
--
ALTER TABLE `transition_actions`
  ADD CONSTRAINT `transition_actions_transition_id_foreign` FOREIGN KEY (`transition_id`) REFERENCES `transitions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transition_roles`
--
ALTER TABLE `transition_roles`
  ADD CONSTRAINT `transition_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transition_roles_transition_id_foreign` FOREIGN KEY (`transition_id`) REFERENCES `transitions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
