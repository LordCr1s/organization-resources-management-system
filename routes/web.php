<?php

Route::get('/', function () {
    return view('base');
});

Route::get('/{any}', function () {
    return view('base');
})->where('any', '.*');