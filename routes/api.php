<?php

use Illuminate\Http\Request;  
  
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login'); 
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('activation', 'ActivationController@compare');
    Route::post('register', 'AuthController@register');
});

Route::group([
    'middleware' => 'api'
], function($router) {

    //auth routes for email verification and password reset
    Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verificationapi.verify');
    Route::get('email/resend/{id}', 'VerificationApiController@resend')->name('verificationapi.resend');
    Route::post('forgetpassword', 'ApiPasswordReset@sendResetLinkEmail');
    Route::post('resetpassword', 'ApiPasswordReset@reset')->name('apipassword.reset');

});

Route::group([

    'middleware' => 'api',
    'prefix' => 'users'

], function ($router) {

    //routes for user managment i.e account managment, role managment and parmission managment
    Route::get('all', 'UserManagerController@all_users');
    Route::get('change/{action}/{id}', 'UserManagerController@update_account_status');
    Route::get('roles/all', 'UserManagerController@all_roles');
    Route::get('permissions/all', 'UserManagerController@all_permissions');
    Route::get('roles/change/{action}/{id}/{role}', 'UserManagerController@change_user_role');
    Route::get('permissions/change/{action}/{id}/{permission}', 'UserManagerController@change_user_permission');
    Route::get('activationc_codes', 'ActivationController@all_activation_codes');
    Route::post('activationc_codes/create', 'ActivationController@store');
    Route::get('activationc_codes/delete/{id}', 'ActivationController@destroy');

});

Route::group([

    'middleware' => 'api',
    'prefix' => 'requests'

], function ($router) {

    Route::get('all', 'RequestsController@index');
    Route::post('new', 'RequestsController@store_a_new_request');
    Route::get('view/approve/{id}', 'RequestsController@view_request_for_sign');
    Route::get('view/{id}', 'RequestsController@show_a_request');
    Route::post('update/status/{id}', 'RequestsController@update_request_status');
    Route::get('view/export/{id}', 'RequestsController@export');

});


Route::group([

    //'middleware' => 'api',
    'prefix' => 'department_store'

], function ($router) {

    Route::get('all', 'DepartmentRequestController@material_in_store');
    Route::get('pending/{role}', 'DepartmentRequestController@pending_request');
    Route::get('onpossession', 'DepartmentRequestController@on_possession_request');
    Route::get('completed', 'DepartmentRequestController@complited_request');
    Route::post('request_material', 'DepartmentRequestController@request_material');
    Route::post('accept_request', 'DepartmentRequestController@accept_request');
    Route::post('reject_request', 'DepartmentRequestController@reject_request');
    Route::post('possess_request', 'DepartmentRequestController@possess_request');
    Route::post('accept_request_for_return', 'DepartmentRequestController@accept_request_for_return_material');
    Route::post('returning_material', 'DepartmentRequestController@returning_material');
    Route::post('cancel_request', 'DepartmentRequestController@cancel_request_for_material');
    Route::post('request_for_return_store_keeper', 'DepartmentRequestController@request_for_return_by_store_keeper');
    Route::post('request_for_return_lecture', 'DepartmentRequestController@request_for_return_by_lecture');


});

Route::group([

    //'middleware' => 'api',
    'prefix' => 'chatroom'

], function ($router) {

    Route::get('all', 'ChatController@index');
    Route::post('new', 'ChatController@store');
    Route::get('view/{id}', 'ChatController@show_a_request');
    Route::post('update{id}', 'ChatController@update');
    Route::get('view/export/{id}', 'ChatController@delete');

});

Route::group([

    //'middleware' => 'api',
    'prefix' => 'files'

], function ($router) {

    Route::get('all', 'FileController@index');
    Route::post('new', 'FileController@store');
    Route::post('track', 'FileController@track');
    Route::get('show/{id}', 'FileController@show');
    Route::post('update{id}', 'FileController@update'); 
    Route::get('view/export/{id}', 'FileController@delete');

});

Route::group([

    //'middleware' => 'api',
    'prefix' => 'circle'

], function ($router) {

    Route::get('all', 'CircleController@index');
    Route::post('new', 'CircleController@store');
    Route::get('show/{id}', 'CircleController@show');
    Route::post('update{id}', 'CircleController@update'); 
    Route::get('view/export/{id}', 'CircleController@delete');

});

Route::group([

    //'middleware' => 'api',
    'prefix' => 'file/request'

], function ($router) {

    Route::get('all', 'FileRequestController@index');
    Route::post('new', 'FileRequestController@store');
    Route::post('process', 'FileRequestController@process');
    Route::get('show/{id}', 'FileRequestController@show');
    Route::post('update{id}', 'FileRequestController@update'); 
    Route::get('view/export/{id}', 'FileRequestController@delete');

});

Route::group([

    //'middleware' => 'api',
    'prefix' => 'file/transaction'

], function ($router) {

    Route::get('all', 'FileTransactionController@index');
    Route::get('incoming', 'FileTransactionController@incoming');
    Route::post('new', 'FileTransactionController@store');
    Route::get('reminder', 'FileTransactionController@reminder');
    Route::post('update{id}', 'FileTransactionController@update'); 
    Route::get('view/export/{id}', 'FileTransactionController@delete');

});

Route::get('/organizations','OrganizationController@index'); //show all
Route::post('/organization','OrganizationController@store'); //create new
Route::get('/organization/{id}','OrganizationController@show'); //get one
Route::put('/organization/{id}','OrganizationController@update'); //update
Route::delete('/organization/{id}','OrganizationController@destroy'); //delete

Route::get('/departments','DepartmentsController@index'); //show all
Route::post('/department','DepartmentsController@store'); //create new 
Route::get('/department/{id}','DepartmentsController@show'); //get one
Route::get('/{id}/departmentstores','DepartmentsController@getStores'); //get one
Route::put('/department/{id}','DepartmentsController@update'); //update
Route::delete('/department/{id}','DepartmentsController@destroy'); //delete


Route::get('/stores','StoreController@index'); //show all
Route::post('/store','StoreController@store'); //create new
Route::get('/store/{id}','StoreController@show'); //get one
Route::put('/store/{id}','StoreController@update'); //update
Route::delete('/store/{id}','StoreController@destroy'); //delete

Route::get('/acq_equipments','AcquisionRequestEquipmentController@index'); //show all
Route::post('/acq_equipment','AcquisionRequestEquipmentController@store'); //create new
Route::put('/acq_equipment/{id}','AcquisionRequestEquipmentController@update'); //update
Route::delete('/acq_equipment/{id}','AcquisionRequestEquipmentController@destroy'); //delete

Route::get('/acq_requests','AcquisionRequestController@index'); //show all
Route::post('/acq_request','AcquisionRequestController@store'); //create new
Route::get('/acq_request/{id}','AcquisionRequestController@show'); //get one
Route::put('/acq_request/{id}','AcquisionRequestController@update'); //update
Route::delete('/acq_request/{id}','AcquisionRequestController@destroy'); //delete

Route::get('/equipments','EquipmentController@index'); //show all
Route::get('/equipment/qrCode/{id}','EquipmentController@showQrCode'); //get one equipment qrCode
Route::post('/equipment/{id}','EquipmentController@show'); //get one equipment
Route::post('/equipment','EquipmentController@store'); //create new
Route::put('/equipment/{id}','EquipmentController@update'); //update
Route::delete('/equipment/{id}','EquipmentController@destroy'); //delete

Route::get('/store_transactions','StoreTransactionController@index'); //show all
Route::post('/store_transaction','StoreTransactionController@store'); //create new
Route::get('/store_transaction/{id}','StoreTransactionController@showTransaction'); //get one
Route::get('/store_transaction/{id}/equipments','StoreTransactionController@showTransactionEquipments'); //get one
Route::put('/store_transaction/{id}','StoreTransactionController@update'); //update
Route::delete('/store_transaction/{id}','StoreTransactionController@destroy'); //delete

Route::get('/grns','ReceivedNoteController@index'); //show all
Route::post('/grn','ReceivedNoteController@store'); //create new
Route::get('/grn/{id}','ReceivedNoteController@show'); //get one
Route::put('/grn/{id}','ReceivedNoteController@update'); //update
Route::delete('/grn/{id}','ReceivedNoteController@destroy'); //delete

Route::get('/tenders', 'TenderController@index');
Route::get('/tender/{id}', 'TenderController@show');
Route::post('/tender', 'TenderController@store');
Route::put('/tender/{id}', 'TenderController@update'); 
Route::delete('/tender/{id}', 'TenderController@delete');

Route::get('/suppliers', 'SupplierController@index');
Route::get('/supplier/{id}', 'SupplierController@show');
Route::post('/supplier', 'SupplierController@store');
Route::put('/supplier/{id}', 'SupplierController@update'); 
Route::delete('/supplier/{id}', 'SupplierController@delete');

Route::get('/bids', 'BidController@index');
Route::get('/bid/{id}', 'BidController@show');
Route::post('/bid', 'BidController@store');
Route::put('/bid/{id}', 'BidController@update'); 
Route::delete('/bid/{id}', 'BidController@delete');

Route::get('/signs', 'SignOffController@index');
Route::get('/sign/{id}', 'SignOffController@show');
Route::post('/sign', 'SignOffController@store');
Route::put('/sign/{id}', 'SignOffController@update'); 
Route::delete('/sign/{id}', 'SignOffController@delete');

Route::get('/categories', 'DepartmentCategoryController@index');
Route::get('/category/{id}', 'DepartmentCategoryController@show');
Route::post('/category', 'DepartmentCategoryController@store');
Route::put('/category/{id}', 'DepartmentCategoryController@update'); 
Route::delete('/category/{id}', 'DepartmentCategoryController@delete');

Route::get('/test', "EquipmentController@test");