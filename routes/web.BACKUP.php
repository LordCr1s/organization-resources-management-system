<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.base');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//->middleware('verified');
Route::get('/activation_code', 'activationController@index');
Route::get('/codes/{id}', 'activationController@destroy');
Route::post('/code', ['uses' => 'activationController@compare','as' => 'token']);
Route::post('/activation_code', ['uses' => 'activationController@store','as' => 'generate_code']);


$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');


// Registration Routes...
if ($options['register'] ?? true) {

    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('activation');
    $this->post('register', 'Auth\RegisterController@register');
}

// Password Reset Routes...
if ($options['reset'] ?? true) {
    $this->resetPassword();
}

// Email Verification Routes...
if ($options['verify'] ?? true) {
    $this->emailVerification();
}

Route::get('manage_user','UserManagerController@index');
Route::get('manage_file','FileManagerController@index');
Route::get('file_approval','FileApprovalController@index');
Route::get('make_request','RequestsController@index');
Route::get('user_settings','SettingsController@index');
Route::get('user_profile','ProfileController@index');

Route::get('users/{action}/{id}','UserManagerController@update_account_status');
Route::get('tb/{action}/{id}','UserManagerController@change_user_role_tb');
Route::get('ec/{action}/{id}','UserManagerController@change_user_role_ec');


Route::get('request/{id}','RequestsController@show_a_request');
Route::post('requestnew','RequestsController@store_a_new_request');

Route::get('/{any}', function () {
    return view('layouts.base');
})->where('any', '.*');