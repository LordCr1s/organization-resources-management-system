<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonPerishableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_perishable', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial_number');
            $table->string('qrCode_string');
            $table->string('brand');
            $table->unsignedInteger('equipment_category_id');
            $table->unsignedInteger('equipment_id');
            $table->foreign('equipment_category_id')->references('id')->on('equipment_information')->onDelete('cascade');
            $table->foreign('equipment_id')->references('id')->on('equipments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('non_perishable');
    }
}
