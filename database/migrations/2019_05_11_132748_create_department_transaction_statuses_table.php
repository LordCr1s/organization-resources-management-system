<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentTransactionStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_transaction_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['Borrowed', 'Returned']);
            $table->unsignedInteger('department_transaction_id');
            $table->foreign('department_transaction_id', 'department_transaction_id_foreign' )->references('id')->on('department_transactions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_transaction_statuses');
    }
}
