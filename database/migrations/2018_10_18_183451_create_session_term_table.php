<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_term', function (Blueprint $table) {
            $table->increments('id');
            $table->string('term_name');
            $table->date('start_at');
            $table->date('end_at');
            $table->unsignedInteger('organization_session_id');
            $table->foreign('organization_session_id')->references('id')->on('organization_session')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_term');
    }
}
