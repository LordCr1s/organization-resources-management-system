<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedNotesTable extends Migration
{
    /** 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('received_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grn_no');
            $table->unsignedInteger('department_id');
            $table->unsignedInteger('supplier_id');
            $table->unsignedInteger('approved_user');
            $table->unsignedInteger('received_user');
            $table->unsignedInteger('verified_user');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->foreign('approved_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('received_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('verified_user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('received_notes');
    }
}
