<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('processed', [1,0]);
            $table->unsignedInteger('from');
            $table->foreign('from')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('to');
            $table->foreign('to')->references('id')->on('users')->onDelete('cascade');
            $table->enum('status', ['Rejected', 'Fowarded', 'Returned', 'Received']);
            $table->unsignedInteger('circle_id');
            $table->foreign('circle_id')->references('id')->on('circles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_transactions');
    }
}
