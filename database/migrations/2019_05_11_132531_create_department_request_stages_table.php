<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentRequestStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_request_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('stage', ['Requesting Material', 'Reciving Material', 'Returning Material', 'Return Material', 'Rejected', 'Accepted', 'Canceled', 'On Possession']);
            $table->unsignedInteger('department_request_id');
            $table->foreign('department_request_id')->references('id')->on('department_requests')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_request_stages');
    }
}
