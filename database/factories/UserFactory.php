<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Organization::class, function( Faker $faker) use ($factory){
    return [
        'organization_name' => $faker->company,
        'address' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'telephone' => $faker->phoneNumber,
        'fax' => str_random(7),
    ];
});
$factory->define(App\CategoryDepartment::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->unique()->randomElement([
            'Academic','Social Service','Administration','Human Resource','Maintainance'
        ]),
        'description' => $faker->sentence,
    ];
});
$factory->define(App\Department::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->randomElement([
            'Civil studies','Finance','Administration','Registration','Estate','Computer studies',
            'Electronic and Telecommunications','Electrical Studies','General studies','Catering'
        ]),
        'description' => $faker->sentence,
        'category_department_id' => $faker->unique(true)->numberBetween(1, App\CategoryDepartment::count()),
    ];
});
$factory->define(App\Store::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->randomElement([
            'Civil','Estate','Computer','Mechanical','Electrical','Telecoms'
        ]),
        'description' => $faker->sentence,
        'department_id' => App\Department::count() > 1 ? $faker->numberBetween(1, App\Department::count()) :$factory->create(App\Department::class)->id,
    ];
});
$factory->define(App\AcquisionRequest::class, function( Faker $faker) use ($factory){
    return [
        'acquision_number' => str_random(12),
        'description' => $faker->sentence,
        'department_id' => App\Department::count() > 1 ? $faker->numberBetween(1, App\Department::count()) :$factory->create(App\Department::class)->id,
        'user_id' => $factory->create(App\User::class)->id,
    ];
});
$factory->define(App\AcquisionRequestEquipment::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->text(10),
        'description' => $faker->sentence,
        'quantity' => $faker->numberBetween(3,50),
        'acquision_request_id' => $factory->create(App\AcquisionRequest::class)->id,
    ];
});
$factory->define(App\Supplier::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->company,
        'category' => $faker->randomElement([
            'Multimedia','Electronics','Computer Electronics','Mechanical','Civil'
        ]),
        'status' => $faker->randomElement([
            'Active','Deactivated','Unavailable'
        ]),
        'address' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'phone_number' => $faker->phoneNumber,
    ];
});
$factory->define(App\ReceivedNote::class, function( Faker $faker) use ($factory){
    return [
        'grn_no' =>  str_random(10),
        'department_id' => App\Department::count() > 1 ? $faker->numberBetween(1, App\Department::count()) :$factory->create(App\Department::class)->id,
        'supplier_id' => $factory->create(App\Supplier::class)->id,
        'approved_user' => $factory->create(App\User::class)->id,
        'received_user' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'verified_user' => $faker->unique(true)->numberBetween(1, App\User::count()),
    ];
});
$factory->define(App\Equipment::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->randomElement(
            ['samsung monitor','huawei switch','toshiba computer unit',
            'nokia adapter','hp laptop','dell printer',
            'hp laser-jet printer','samsung projector',
            'Pens', 'pencils', 'erasers', 'pencil sharpener',
            'Permanent markers',
            'Highlighter pens',
            'Paper clips',
            'Binder clips',
            'Stapler',
            'staples',
            'Tape dispenser',
            'extra rolls of tape',
            'Glue sticks',
            'Rubber bands',
            'Scissors',
            'In/Out box for paperwork',
            '3 hole punch',
            'Calculator',
            'Desk drawer organizer',
            'Envelopes ',
            'Printer paper',
            'Stamps',
            'Return address labels',
            'Stationery',
            'thank you notes',
            'Sticky notes',
            'Notepads',
            'Printer ink',
            'toner',
            'cartridges',
            'File cabinet',
            'file box',
            'plus tabs',
            'File labels',
            '3 ring binders',
            'Index dividers',
            'Calendar ',
            'Planner',
            'To do list',
            'White board  ',
            'bulletin board',
            ]),
        'grn_id' => App\ReceivedNote::count() > 1 ? $faker->numberBetween(1, App\ReceivedNote::count()) :$factory->create(App\ReceivedNote::class)->id,
        'store_id' => App\Store::count() > 1 ? $faker->numberBetween(1, App\Store::count()) :$factory->create(App\Store::class)->id,
    ];
});

$factory->define(App\Perishable::class, function( Faker $faker) use ($factory){
    return [
        'quantity' => $faker->numberBetween(1,50),
        'unit_measure_id' => $factory->create(App\EquipmentInformation::class)->id,
        'equipment_id' => $factory->create(App\Equipment::class)->id,
    ];
});
$factory->define(App\EquipmentInformation::class, function( Faker $faker) use ($factory){
    return [
        'name' => $faker->text(10),
        'type' => $faker->randomElement(['Perishable','Non-Perishable','Others']),
        'description' => $faker->sentence,
    ];
});
$factory->define(App\NonPerishable::class, function( Faker $faker) use ($factory){
    return [
        'brand' => $faker->randomElement(['samsung','huawei','toshiba','nokia','hp','dell']),
        'qrCode_string' => 'OREMS'.str_random(12),
        'serial_number' => str_random(10),
        'equipment_id' => $factory->create(App\Equipment::class)->id,
        'equipment_category_id' => $factory->create(App\EquipmentInformation::class)->id,
    ];
});
$factory->define(App\StoreTransaction::class, function( Faker $faker) use ($factory){
    return [
        'transaction_number' => str_random(10),
        'acquision_id' => $factory->create(App\AcquisionRequest::class)->id,
        'department_id' => $factory->create(App\Department::class)->id,
        'receiving_user' => $factory->create(App\User::class)->id,
    ];
});

$factory->define(App\EquipmentStoreTransaction::class, function( Faker $faker) use ($factory){
    return [
        // 'store_transaction_id' => 12,
        // 'equipment_id' => $faker->numberBetween(1,50),
        'equipment_id' => $factory->create(App\Equipment::class)->id,
        'store_transaction_id' => $factory->create(App\StoreTransaction::class)->id,
    ];
});
$factory->define(App\User::class, function (Faker $faker) use ($factory){
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'surname' => $faker->lastName,
        'status' => 'Active',
        'department_id' => $faker->numberBetween(1, App\Department::count()),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' =>now(),
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\ActivationCode::class, function (Faker $faker) {
        foreach(range(1,6) as $id){
            return [
                'department_id' => $id,
                'role_id' => $id,
                'expire' =>'No',
                'activation_code' => str_random(7),
            ];
        }
});

$factory->define(App\DepartmentStore::class, function (Faker $faker) {
    return[
        'balance' => $faker->randomElement([50,200,38,56,45,39,67,43,12,69,68]),
        'unavilable' => $faker->randomElement([5,8,6,5,9,7,3,2,9,8]),
        'equipment_id' => $faker->unique(true)->numberBetween(1, App\Equipment::count()),
        'department_id' => $faker->unique(true)->numberBetween(1, App\Department::count()),
        
    ];
});


$factory->define(App\DepartmentRequest::class, function (Faker $faker) {
    
    return [
        'quantity' => $faker->randomElement([5,8,6,5,9,7,3,2,9,8]),
        'purpose' => $faker->text(80),
        'borrow_code' =>$faker->numberBetween(100000, 999999),
        'return_code' => $faker->numberBetween(100000, 999999),
        'user_id' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'equipment_id' => $faker->unique(true)->numberBetween(1, App\Equipment::count()),
    ];
});

$factory->define(App\DepartmentRequestStage::class, function (Faker $faker) {
    
    return [
        'stage' => $faker->randomElement(['Requesting Material', 'Reciving Material', 'Returning Material', 'Return Material', 'Rejected', 'Accepted', 'Canceled', 'On Possession']),
        'department_request_id' => $faker->unique(true)->numberBetween(1, App\DepartmentRequest::count()),
    ];
});

$factory->define(App\DepartmentTransaction::class, function (Faker $faker) {
    
    return[
        'quantity' => $faker->randomElement([5,8,6,5,9,7,3,2,9,8]),
        'user_id' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'equipment_id' => $faker->unique(true)->numberBetween(1, App\Equipment::count()),
        'department_store_id' => $faker->unique(true)->numberBetween(1, App\DepartmentStore::count()),
        'department_request_id'  => $faker->unique(true)->numberBetween(1, App\DepartmentRequest::count()),
    ];
});

$factory->define(App\DepartmentTransactionStatus::class, function (Faker $faker) {
    
    return [
        'status' => $faker->randomElement(['Borrowed', 'Returned']),
        'department_transaction_id' => $faker->unique(true)->numberBetween(1, App\DepartmentTransaction::count()),
    ];
});

$factory->define(App\Chat::class, function (Faker $faker) {
    
    return [
        'message' => $faker->text(30),
        'department_id' => $faker->unique(true)->numberBetween(1, App\Department::count()),
        'user_id' => $faker->unique(true)->numberBetween(1, App\User::count()),
    ];
});

$factory->define(App\Reply::class, function (Faker $faker) {
    
    return [
        'reply' => $faker->text(30),
        'chat_id' => $faker->unique(true)->numberBetween(1, App\Chat::count()),
        'user_id' => $faker->unique(true)->numberBetween(1, App\User::count()),
    ];
});

$factory->define(App\File::class, function (Faker $faker) {
    
    return [
        'name' => $faker->catchPhrase,
        'reference_no' => $faker->unique(true)->swiftBicNumber,
        'file_type' => $faker->randomElement(['Personal', 'Subject']),
        'access_level' => $faker->randomElement(['Public', 'Private']),
        'secrety_no' => $faker->str_random(7),
        'department_id'  => $faker->unique(true)->numberBetween(1, App\Department::count()),
    ];
}); 

$factory->define(App\Circle::class, function (Faker $faker) {
    
    return [
        'reason' => $faker->catchPhrase,
        'start_at' => $faker->dateTime,
        'end_at' => $faker->dateTime,
        'status' => $faker->randomElement(['On Progress', 'Complete']),
    ];
}); 

$factory->define(App\Attachment::class, function (Faker $faker) {
    
    return [
        'file_path' => $faker->catchPhrase,
        'file_id' => $faker->unique(true)->numberBetween(1, App\File::count()),
        'circle_id' => $faker->unique(true)->numberBetween(1, App\Circle::count()),
    ];
}); 

$factory->define(App\Comment::class, function (Faker $faker) {
    
    return [
        'comment' => $faker->sentence,
        'attachment_id' => $faker->unique(true)->numberBetween(1, App\Attachment::count()),
    ];
});

$factory->define(App\FileTransaction::class, function (Faker $faker) {
    
    return [
        'status' => $faker->randomElement(['Rejected', 'Fowarded', 'Returned', 'Received']),
        'from' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'to' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'circle_id' => $faker->unique(true)->numberBetween(1, App\Circle::count()),
    ];
});

$factory->define(App\FileRequest::class, function (Faker $faker) {
    
    return [
        'status' => $faker->randomElement(['In progress', 'Accepted', 'Rejected']),
        'sender_id' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'receiver_id' => $faker->unique(true)->numberBetween(1, App\User::count()),
        'file_id' => $faker->unique(true)->numberBetween(1, App\File::count()),
    ];
});

$factory->define(App\Role::class, function (Faker $faker) {
    
    return [
        'name' => $faker->randomElement(['LE', 'TC', 'PR', 'HOD','RG', 'SK']),
        'description' => $faker->randomElement(['head of department', 'Principal', 'Lecture', 'Store Keper', 'Registry', 'Technicianl']),
        'department_id' => $faker->unique(true)->numberBetween(1, App\Department::count()),
    ];
});

$factory->define(App\InitialAttachment::class, function (Faker $faker) {
    
    return [
        'name' => $faker->catchPhrase,
        'path' => $faker->url,
        'file_id' => $faker->unique(true)->numberBetween(1, App\File::count()),
    ];
});