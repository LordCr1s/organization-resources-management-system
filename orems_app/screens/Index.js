import React from 'react';
import HomeScreen from './Home/HomeScreen'
import PostScreen from './Home/PostScreen'
import Dashboard from './Home/Dashboard'
import { widthPercentageToDP as widthTodp, heightPercentageToDP as heightTodp } from 'react-native-responsive-screen'
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs"
import { Icon } from '@shoutem/ui'
import { AntDesign, MaterialIcons, Entypo, Feather } from '@expo/vector-icons'

export default createMaterialBottomTabNavigator({
    home: { screen: HomeScreen,  },
    dashboard: { screen: Dashboard, },
    posts: { screen: PostScreen, },
  }, {
    initialRouteName: 'home',
    activeColor: 'rgba(0, 0, 0, 0.7)',
    inactiveColor: 'rgba(0, 0, 0, 0.3)',
    barStyle: { height: heightTodp('6.7%'), backgroundColor: '#FFF' },
  });
  

HomeScreen.navigationOptions = {
    title: 'Materials',
    tabBarIcon: <Feather name="grid" size={20} color={'rgba(0, 0, 0, 0.7)'} />
}

Dashboard.navigationOptions = {
    title: 'Requests',
    tabBarIcon: <Feather name="feather" size={20} color={'rgba(0, 0, 0, 0.7)'}/>
}

PostScreen.navigationOptions = {
    title: 'Communication',
    tabBarIcon: <Feather name="message-circle" size={20} color={'rgba(0, 0, 0, 0.7)'}/>
}