import { combineReducers } from 'redux'
import { loginReducer } from './loginReducer'
import { registrationReducer } from './registrationReducer'

export const reducers_manager = combineReducers({
    login_provider: loginReducer,
    registration_provider: registrationReducer,
});

