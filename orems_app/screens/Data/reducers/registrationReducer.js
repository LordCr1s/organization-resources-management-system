// ACTIONS TYPES DECLARATIONS
const REGISTER_A_USER ='registration:registerAuser'
const CONFIRM_AN_ACCOUNT = 'registration:confirmUserAccount'

// creates an action that identifies what a user is requesting
// to be done
export const actionsCreator = (action, user, code=0) => ({
    type: action,
    payload: {
        // user credantials submited for registration
        // refer to register Component to understand this
        user: user,

        // for account activation, user verification
        code: code
    }
})

// ACTIONS
// take inputs from registration form and trigger an action to register
// a new user
const registerAuser = user => actionsCreator(REGISTER_A_USER, user)

// for taking a code from the user to verify them and activate their account
const confirmUserAccount = (user, code) => actionsCreator(CONFIRM_AN_ACCOUNT, user, code)


const registrationPage = {
    actions : {
        registerAuser: registerAuser,
        confirmUserAccount: confirmUserAccount,
    }
}

export const registrationReducer = (state = registrationPage, {
    type,
    payload
}) => {
    switch (type) {
        case REGISTER_A_USER:
            // on step one users provide their email to receive password reset code
            let stepOneState = {
                ...state,
            }
            registerNewUser(payload.user)
            return stepOneState

        case CONFIRM_AN_ACCOUNT:
            // on step two users provides code sent to the email they have provided at step one
            let step_two = {...state.viewControl}
            step_two.isPasswordResetCode = payload.value
            let stepTwoState = {
                ...state,
                viewControl: step_two
            }
            return stepTwoState

        default:
            return state
    }
}

makePostRequest = (request_url, request_data) => {
    return fetch(request_url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(request_data) 
    }).then(response => response).catch(error => console.log(error))
}


const registerNewUser = async user => {
    const response = await makePostRequest('http://172.20.10.5:8006/api/register', user)
    
}

// verify and activate user account
const verifyCode = (user, code) => {
 
}