import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, Easing} from 'react-native';
import { Feather } from '@expo/vector-icons'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen'


export default class SplashScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.app_name, {fontSize: heightPercentageToDP('6%')}]}>O</Text>
                <Text style={styles.app_name}>REMS</Text>
                <Feather name="database" color="#fcc100" size={36}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      backgroundColor: '#2e3e4e',
      alignItems: 'center',
      justifyContent: 'center',
      width: widthPercentageToDP('100%')
    },
    app_name : {
        //fontFamily: 'noto-bold',
        fontSize: heightPercentageToDP('4%'),
        color: '#FFF'
    }
});