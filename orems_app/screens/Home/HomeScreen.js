import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph , Snackbar, DefaultTheme} from 'react-native-paper'
import { home_screen, theme } from './Styles'
import API from './api'
import Feather from '@expo/vector-icons/Feather';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import Material from './Materials/Material'

export default class HomeScreen extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      materials: [],
      snackbar_message: 'default text',
      is_snackbar_visible: false,
    }
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    // fetch materials from the stores
    this.getMaterials()
  }

  getMaterials = () => {
    
    let materials = API.makeGETrequest('equipments')
    materials.then(response => {
      this.setState({
        isFetching: false,
        materials: response,
      })
    })
  }

  notifyUser = (message) => {
    this.setState({
      is_snackbar_visible: true,
      snackbar_message: message
    })
  }

  render() { 
    return (
        <View style={home_screen.container}>
          <View style={ home_screen.search_bar_container }>
            <Searchbar 
                placeholder="search materials"
                style={ home_screen.search_bar }
                theme={ { colors: {primary: '#FFF', accent: '#FFFFFF', text: '#FFFFFF', placeholder: '#FFFFFF'}} }
            />
          </View> 
          <View style={home_screen.contect_view}>
            <FlatList data={this.state.materials} renderItem={({item}) => 
              <Material material={item} notify={this.notifyUser}/>
            }/>
          </View>
          <Snackbar
            visible={this.state.is_snackbar_visible}
            onDismiss={() => this.setState({ is_snackbar_visible: false })}
            duration={4000}
            theme = {{
              ...DefaultTheme,
              colors: {
                ...DefaultTheme.colors,
                primary: '#a180da',
                background: '#a180da',
                accent: '#FFF',
              },
            }}>
            {this.state.snackbar_message}
          </Snackbar>
        </View>
    );
  }
}

export const _theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#a180da',
    accent: '#FFF',
  },
};