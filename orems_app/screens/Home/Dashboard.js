import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native';
import { Paragraph, Snackbar, DefaultTheme } from 'react-native-paper'
import { home_screen, theme } from './Styles'
import API from './api'
import {AntDesign, MaterialIcons, Feather} from '@expo/vector-icons';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import {Container, Tab, Tabs, Header, TabHeading, Icon } from "native-base";
import Request from './Request/Request'

import { Constants, Permissions, BarCodeScanner } from 'expo';

export default class Dashboard extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      requests: [],
      snackbar_message: 'default text',
      is_snackbar_visible: false,

      // qr code scanner variables
      hasCameraPermission: null,
    }
  }


  static navigationOptions = {
    header: null
  };


  async componentDidMount() {
    // fetch materials from the stores
    this.getRequests()
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  removeRequest = (request_id) => {
    this.getRequests()
  }

  getRequests = () => {
    fetch('http://172.20.10.5:8003/request/getrequests/1?ZkWE=2345&tGjx=1',{
        method: 'GET'
    })
    .then( response => response.json())
    .then( response => {
      this.setState({
        isFetching: false,
        requests: response,
      })
    })
}


  notifyUser = (message) => {
    this.setState({
      is_snackbar_visible: true,
      snackbar_message: message
    })
  }

  render() { 
    return (
      <View style={home_screen.container}>
          <View style={ home_screen.search_bar_container }>
            <Text style={{ textAlign: 'center', color: '#FFF', fontSize: heightPercentageToDP('2.4%')}}>Request management</Text>
          </View> 
          <View style={[home_screen.contect_view, {flex: 12}]}>
           





          <Container style={styles.container}>
                <Tabs style={styles.tab_bar_style} tabBarUnderlineStyle={styles.tab_underline_style}>
                  <Tab heading="in progress" tabStyle={styles.tab} 
                    activeTabStyle={styles.tab} textStyle={styles.tab_text} activeTextStyle={styles.active_tab_text}>
                       <FlatList data={this.state.requests} renderItem={({item}) => 
                          (item.status == "pending" || item.status == "accepted") ? 
                          <Request request={item} notify={this.notifyUser} removeRequest={this.removeRequest}
                          permission={this.state.hasCameraPermission}/>:
                          null
                        }/>
                  </Tab>
                  <Tab heading="completed" tabStyle={styles.tab} activeTabStyle={styles.tab} textStyle={styles.tab_text} activeTextStyle={styles.active_tab_text}>
                      <FlatList data={this.state.requests} renderItem={({item}) => 
                          (item.status == "pending" || item.status == "accepted") ? null :
                          <Request request={item} notify={this.notifyUser} removeRequest={this.removeRequest}
                          permission={this.state.hasCameraPermission}/>
                        }/>
                  </Tab>
                </Tabs>
              </Container>






          </View>
          <Snackbar
            visible={this.state.is_snackbar_visible}
            onDismiss={() => this.setState({ is_snackbar_visible: false })}
            duration={4000}
            theme = {{
              ...DefaultTheme,
              colors: {
                ...DefaultTheme.colors,
                primary: '#a180da',
                background: '#a180da',
                accent: '#FFF',
              },
            }}>
            {this.state.snackbar_message}
          </Snackbar>  
      </View>
    );
  }
}


const styles = StyleSheet.create({
  tab_bar_style: {
    width: widthPercentageToDP("100%"),
  },
  tab: {
    backgroundColor: "#2e3e4e",
    height: heightPercentageToDP('1%')

  },
  tab_text: {
    color: '#FFF'
  },
  active_tab_text: {
    color: '#FFF'
  },
  container: {
    height: heightPercentageToDP(".5%")
  },
  tab_underline_style: {
    borderRadius: 10,
    backgroundColor: '#47bac1',
    height: 2
  },
});