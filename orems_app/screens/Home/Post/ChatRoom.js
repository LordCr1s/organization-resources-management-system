import React from 'react';
import { View, FlatList, Text, TouchableOpacity,TextInput, StyleSheet} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph , Snackbar, DefaultTheme} from 'react-native-paper'
import { home_screen, theme } from '../Styles'
import API from '../api'
import Feather from '@expo/vector-icons/Feather';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class Communication extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      messages: [],
      snackbar_message: 'default text',
      is_snackbar_visible: false,
      typed_message: {}
    }
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    // fetch materials from the stores
    this.getMaterials()
  }

  getMaterials = () => {
    
    let materials = API.makeGETrequest('equipments')
    materials.then(response => {
      this.setState({
        isFetching: false,
        materials: response,
      })
    })
  }

  notifyUser = (message) => {
    this.setState({
      is_snackbar_visible: true,
      snackbar_message: message
    })
  }

  async componentDidMount() {
    // fetch materials from the stores
    this.getRequests()
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  updateMessages = (request_id) => {
    this.getRequests()
  }

  getRequests = () => {
    fetch('http://172.20.10.5:8003/communication/getmessages/1/complain?ZkWE=2345&tGjx=1',{
        method: 'GET'
    })
    .then( response => response.json())
    .then( response => {
      this.setState({
        isFetching: false,
        messages: response,
      })
    })
  }


  updateUserInput = (value, content) => {
    let message = {
      "sender": content.sender,
      "type": content.type,
      "department": content.department,
      "content": value
    }

    this.setState({
      typed_message: message
    })

  }

  sendMessage =  () => {
    API.makePOSTrequest('communication/postmessages?ZkWE=2345&tGjx=1', this.state.typed_message).then(response => {
      if (response.status == 200) {
        this.updateMessages()
      }
    })

    
  }

  render() { 
    return (

      

        <View style={home_screen.container}>
          <View style={ home_screen.search_bar_container }>
            <Text style={styles.header_text}>Chat room</Text>
          </View> 
          
          <View style={[home_screen.contect_view]}>
          <KeyboardAwareScrollView
            style={{ backgroundColor: 'rgb(249, 249, 250)' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={[home_screen.container, { backgroundColor: 'rgb(249, 249, 250)' }]}
            scrollEnabled={true}>
              
                <FlatList data={this.state.messages} inverted={true} renderItem={({item}) => 
                  
                    <View style={styles.message_container}>
                      <View style={styles.request}>
                          <Feather name="radio" size={24} />
                          <View style={styles.request_content}>
                              <Text style={styles.request_header}>{item.sender} </Text>
                              <Text style={styles.request_date}>
                                {item.sent_at}
                              </Text>
                          </View>
                      </View>
                      <View style={styles.message_body}>
                          <Text style={{color: 'rgba(0, 0, 0, 0.6)'}}>{item.content}</Text>
                      </View>
                  </View>

                }/>



            

            <View style={styles.textiput_container}>
              <TextInput
                  style={styles.inputs}
                  placeholder="type..."
                  onChangeText={(text) => this.updateUserInput(text, {sender: 1, type: "complain", department: 1})}
                  placeholderTextColor="rgba(0, 0, 0, .4)"
                  multiline={true}
                  textAlignVertical="top"
              />
              <TouchableOpacity onPress={() => this.sendMessage()} activeOpacity={0.7}>
                <View style={[styles.button, {backgroundColor: '#0cc2aa',}]}>
                  <Text style={styles.buttonText}>send</Text>
                </View> 
            </TouchableOpacity>
            </View>
          
           

</KeyboardAwareScrollView>
</View>
          
          <Snackbar
            visible={this.state.is_snackbar_visible}
            onDismiss={() => this.setState({ is_snackbar_visible: false })}
            duration={4000}
            theme = {{
              ...DefaultTheme,
              colors: {
                ...DefaultTheme.colors,
                primary: '#a180da',
                background: '#a180da',
                accent: '#FFF',
              },
            }}>
            {this.state.snackbar_message}
          </Snackbar>
        </View>
        
    );
  }
}


const styles = StyleSheet.create({
  header_text: {
      textAlign: "center",
      marginTop: heightPercentageToDP("1%"),
      color: "#FFF",
      fontSize: heightPercentageToDP("2.4%")
  },
  request : {
    flexDirection: 'row',
    backgroundColor: '#FFF',
    padding: 8,
    marginTop: 1,
    alignItems: 'center',
    width: widthPercentageToDP('100%'),
    
  },
  message_body : {
    backgroundColor: '#FFF',
    padding: 10,
    width: widthPercentageToDP('100%'),
    
  },
  request_content: {
      flexDirection: 'column',
      marginLeft: widthPercentageToDP('3%'),
  },
  request_header: {
      color: 'rgba(0, 0, 0, 0.8)',
      fontFamily: 'noto-bold'
  },
  request_date : {
      color: 'rgba(0, 0, 0, 0.4)',
      fontSize: heightPercentageToDP('1.4%'),
      width: widthPercentageToDP('85%')
  },
  message_container : {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: heightPercentageToDP('.2%')
  },
  accept_button: {
      color: 'white',
      height: heightPercentageToDP('3%'),
      backgroundColor: 'rgb(0, 152, 95)',
      borderColor: 'rgb(0, 152, 95)',
  },
  reject_button: {
      color: 'white',
      height: heightPercentageToDP('3%'),
      backgroundColor: 'rgb(229, 94, 91)',
      borderColor: 'rgb(229, 94, 91)',
      marginLeft: widthPercentageToDP('.8%')
  },
  white_text: {
      color: 'white',
      textAlign: 'center'
  },
  inputs: {
    width: widthPercentageToDP('75%'),
    paddingVertical: heightPercentageToDP('2%'),
    paddingHorizontal: 15,
    borderRadius: 5,
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center', 
    backgroundColor: 'rgba(0, 0, 0, .1)',
    color: 'rgba(0, 0, 0, .7)',
  },
  textiput_container: {
    backgroundColor: '#FFF',
    width: widthPercentageToDP('100%'),
    padding: 10,
    flexDirection: 'row'
  },
  buttonText: {
    color: '#FFF',
    textAlign: 'center', 
  },
  button: {
    width: widthPercentageToDP('17%'),
    paddingVertical: heightPercentageToDP('1.2%'),
    borderRadius: 5,
    marginLeft: widthPercentageToDP('3%')
  },
});

export const _theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#a180da',
    accent: '#FFF',
  },
};