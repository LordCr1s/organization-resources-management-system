import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph , Snackbar, DefaultTheme} from 'react-native-paper'
import { home_screen, theme } from './Styles'
import API from './api'
import Feather from '@expo/vector-icons/Feather';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import Material from './Materials/Material'


export default class PostScreen extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      materials: [],
      snackbar_message: 'default text',
      is_snackbar_visible: false,
    }
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    // fetch materials from the stores
    this.getMaterials()
  }

  getMaterials = () => {
    
    let materials = API.makeGETrequest('equipments')
    materials.then(response => {
      this.setState({
        isFetching: false,
        materials: response,
      })
    })
  }

  notifyUser = (message) => {
    this.setState({
      is_snackbar_visible: true,
      snackbar_message: message
    })
  }

  render() { 
    return (
        <View style={home_screen.container}>
          <View style={ home_screen.search_bar_container }>
            <Text style={styles.header_text}>Communication</Text>
          </View> 
          <View style={[home_screen.contect_view, {backgroundColor: '#FFF'}]}>
            
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Announcements')} activeOpacity={0.7}>
              <View style={styles.request}>
                  <Feather name="radio" size={34} />
                  <View style={styles.request_content}>
                      <Text style={styles.request_header}> Announcements </Text>
                      <Text style={styles.request_date}>
                        receive announcements from stor keepers, administrators, and head of departments concerning 
                        teaching materials in your departments
                      </Text>
                  </View>
              </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('ChatRoom')} activeOpacity={0.7}>
              <View style={styles.request}>
                  <Feather name="package" size={34} />
                  <View style={styles.request_content}>
                      <Text style={styles.request_header}> Chat room </Text>
                      <Text style={styles.request_date}>
                        if you have complains, suggestions or any thoughts you want to share, this is a place to post,
                        <Text style={{color: '#f4445580'}}> please do not post out of topic matters</Text>
                      </Text>
                  </View>
              </View>
          </TouchableOpacity>



          </View>
          <Snackbar
            visible={this.state.is_snackbar_visible}
            onDismiss={() => this.setState({ is_snackbar_visible: false })}
            duration={4000}
            theme = {{
              ...DefaultTheme,
              colors: {
                ...DefaultTheme.colors,
                primary: '#a180da',
                background: '#a180da',
                accent: '#FFF',
              },
            }}>
            {this.state.snackbar_message}
          </Snackbar>
        </View>
    );
  }
}


const styles = StyleSheet.create({
  header_text: {
      textAlign: "center",
      marginTop: heightPercentageToDP("1%"),
      color: "#FFF",
      fontSize: heightPercentageToDP("2.4%")
  },
  request : {
    flexDirection: 'row',
    backgroundColor: '#FFF',
    padding: 8,
    marginTop: 1,
    alignItems: 'center',
    width: widthPercentageToDP('100%')
  },
  request_content: {
      flexDirection: 'column',
      marginLeft: widthPercentageToDP('3%'),
  },
  request_header: {
      color: 'rgba(0, 0, 0, 0.8)',
      fontFamily: 'noto-bold'
  },
  request_date : {
      color: 'rgba(0, 0, 0, 0.4)',
      fontSize: heightPercentageToDP('1.4%'),
      width: widthPercentageToDP('85%')
  },
  request_place_name: {

  },
  accept_button: {
      color: 'white',
      height: heightPercentageToDP('3%'),
      backgroundColor: 'rgb(0, 152, 95)',
      borderColor: 'rgb(0, 152, 95)',
  },
  reject_button: {
      color: 'white',
      height: heightPercentageToDP('3%'),
      backgroundColor: 'rgb(229, 94, 91)',
      borderColor: 'rgb(229, 94, 91)',
      marginLeft: widthPercentageToDP('.8%')
  },
  white_text: {
      color: 'white',
      textAlign: 'center'
  }
});

export const _theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#a180da',
    accent: '#FFF',
  },
};