import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet} from 'react-native';
import { Searchbar, Avatar, Button, Card, Title, Paragraph, Snack } from 'react-native-paper'
import { home_screen, theme } from '../Styles'
import API from '../api'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import {Feather, AntDesign} from '@expo/vector-icons/'


export default class Material extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        // snackbar variables
        snackbar_visible : false,
        snackbar_message : 'default text',
      };
    }

    requestMaterial =  (material_id) => {
      API.makePOSTrequest(
        'request/makerequest?ZkWE=2345&tGjx=1',
        {
          source: 1,
          destination: 2,
          material: material_id,
          request_type: 'requesting',
          action: "is requesting for",
        }
      ).then(response => {
        if (response.status == 200) {
          this.props.notify("request sent successfull")
        }
      })

      
    }

    render() {
      const material = this.props.material
  
      return (
        <Card style={[home_screen.home_cards, {fontFamily: 'noto-regular', fontWeight: 'bold'}]}>
          <Card.Content style={home_screen.card_content}>
            <Card.Cover  source={""} style={home_screen.card_cover}/>
            <Card.Content>
                <Paragraph style={home_screen.card_side_content}>
                    <Title style={{fontFamily: 'noto-bold', color: '#47bac1', fontSize: heightPercentageToDP('2.3%')}}>{material.name}</Title>
                </Paragraph>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                    <AntDesign name="qrcode" size={15} color={'#47bac1'} />  indentifier
                </Paragraph>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                    <Feather name="tag" size={15} color={'#fcc100'} />  category
                </Paragraph>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                    <Feather name="link" size={15} color={'#fcc100'} />  {material.store.department.name}
                </Paragraph>
                <TouchableOpacity onPress={() => this.requestMaterial(material.id)} activeOpacity={0.7}>
                    <View style={styles.request_button}>
                        <Text style={{color: '#0cc2aa'}}>request</Text>
                    </View> 
                </TouchableOpacity>
            </Card.Content>
          </Card.Content>
        </Card>
      );
    }
  }

const styles = StyleSheet.create({
    request_button: {
        backgroundColor: "rgba(71, 186, 193, .1)",
        padding: 2,
        borderRadius: 5,
        width: widthPercentageToDP("15%"),
    }
});