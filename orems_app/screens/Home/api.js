const ROOT_API_URL = 'http://172.20.10.5:8002/api/'

export default class API {
    
    static makePOSTrequest = (request_url, request_data) => {
        return fetch('http://172.20.10.5:8003/' + request_url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                
            },
            body: JSON.stringify(request_data) 
        }).then(response => response).catch(error => console.log(error))
    }

    static makeGETrequest = (request_url) => {
        return fetch(ROOT_API_URL + request_url, {
            method: 'GET',
        }).then(response => response.json()).catch(error => console.log(error))
    }
}

