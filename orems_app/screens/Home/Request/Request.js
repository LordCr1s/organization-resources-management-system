import React from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, Modal, Button} from 'react-native';
import { Portal, Card, Title, Paragraph, Snack } from 'react-native-paper'
import { home_screen, theme } from '../Styles'
import API from '../api'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import {Feather, AntDesign} from '@expo/vector-icons/'
import QRCode from 'react-native-qrcode';
import { Constants, Permissions, BarCodeScanner } from 'expo';

export default class Request extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        // snackbar variables
        snackbar_visible : false,
        snackbar_message : 'default text',

        // modal visibility controll
        is_modal_visible: false,

        // qr code scanner state variables
        scanned: false,
      };
    }

 

    updateRequest =  (request, status, request_id=null) => {

      fetch('http://172.20.10.5:8003/request/updaterequest/'+ request_id +'?ZkWE=2345&tGjx=1', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ...request,
                status: status,
                message: "your request for " + request.material + " was rejected"
            }) 
        }).then(response => {
            if (response.status == 200) {
                this.props.removeRequest(request.id)
                this.props.notify("request "+ status +" successfull")
            }
        }).catch(error => console.log(error))

        
    }

    _showModal = () => this.setState({ is_modal_visible: true });
    _hideModal = () => this.setState({ is_modal_visible: false });

    // handle verification after qr code has been scanned
    handleBarCodeScanned = ({ type, data }) => {
        this.setState({ scanned: true });
        this.updateRequest(this.props.request, "verified", data)
        alert(`Request verified successfuly`)
      };

    render() {

      const request = this.props.request
      
      // check if the app hass camera permissions
      const { scanned } = this.state;

      if (this.props.permission === null) {
        return <Text>Requesting for camera permission</Text>;
      }
      if (this.props.permission === false) {
        return <Text>No access to camera</Text>;
      }

      return (
        <Card style={[home_screen.home_cards, {fontFamily: 'noto-regular', fontWeight: 'bold'}]}>
          <Card.Content style={home_screen.card_content}>
            <Card.Cover  source={""} style={home_screen.card_cover}/>
            <Card.Content>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                    { request.source + " " + request.action + " " + request.material}
                </Paragraph>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                    <Feather name="watch" size={15} color={'#47bac1'} />  {request.updated_at}
                </Paragraph>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                    {
                        (request.status == "pending") ? 
                        <React.Fragment><Feather name="loader" size={15} color={'#fcc100'} /> {request.status}</React.Fragment> :
                        (request.status == "verified") ?
                        <React.Fragment><Feather name="check" size={15} color={'#5fc27e'} /> {request.status}</React.Fragment> :
                        (request.status == "accepted") ?
                        <React.Fragment><AntDesign name="qrcode" size={15} color={'#5fc27e'} /> {request.status}</React.Fragment> :
                        (request.status == "rejected") ?
                        <React.Fragment><Feather name="slash" size={15} color={'#f44455'} /> {request.status}</React.Fragment> :
                        (request.status == "canceled") ?
                        <React.Fragment><Feather name="x" size={15} color={'#fcc100'} /> {request.status}</React.Fragment> :
                        null
                    }
                </Paragraph>
                <Paragraph style={[home_screen.card_list_text, {fontFamily: 'noto-regular'}]}>
                <AntDesign name="qrcode" size={15} color={'#47bac1'} />  indentifier
                </Paragraph>
                {/* { request.is_completed ? 
                    <View>
                        <Text style={{color: '#a180da'}}>completed</Text>
                    </View> 
                    :
                    <View style={{ flexDirection: 'row'}}>

                        <TouchableOpacity onPress={() => this.updateRequest(request, "canceled", request.id)} activeOpacity={0.7}>
                            <View style={styles.reject_button}>
                                <Text style={{color: '#f44455'}}>cancel</Text>
                            </View> 
                        </TouchableOpacity>
                        
                        <TouchableOpacity onPress={() => this._showModal(request.id)} activeOpacity={0.7}>
                            <View style={styles.show_qr_button}>
                                <Text style={{color: '#0cc2aa'}}>scan qrcode</Text>
                            </View> 
                        </TouchableOpacity>

                        <Modal
                            animationType="slide"
                            transparent={false}
                            visible={this.state.is_modal_visible}>
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    justifyContent: 'flex-end',
                                }}>
                                <BarCodeScanner
                                    onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                                    style={StyleSheet.absoluteFillObject}
                                />

                                {scanned && (
                                <Button
                                    title={'Tap to Scan Again'}
                                    onPress={() => this.setState({ scanned: false })}
                                />
                                )}
                                <TouchableOpacity onPress={() => this._hideModal()} activeOpacity={0.7}>
                                        <View style={styles.hide_qrcode_scanner}>
                                            <Text style={{color: '#0cc2aa'}}>CLOSE SCANNER</Text>
                                        </View> 
                                </TouchableOpacity>
                            </View>
                        </Modal>
                    </View>
                } */}

                { request.is_completed ? 
                    <View>
                        <Text style={{color: '#a180da'}}>completed</Text>
                    </View> 
                    :
                    <View style={{ flexDirection: 'row'}}>
                        {
                            (request.status == 'accepted') ? 
                            <TouchableOpacity onPress={() => this.updateRequest(request, "canceled", request.id)} activeOpacity={0.7}>
                                <View style={styles.reject_button}>
                                    <Text style={{color: '#f44455'}}>cancel</Text>
                                </View> 
                            </TouchableOpacity>
                            :
                            <React.Fragment>
                                <TouchableOpacity onPress={() => this.updateRequest(request, "accepted", request.id)} activeOpacity={0.7}>
                                    <View style={styles.request_button}>
                                        <Text style={{color: '#0cc2aa'}}>accept</Text>
                                    </View> 
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.updateRequest(request, "rejected", request.id)} activeOpacity={0.7}>
                                    <View style={styles.reject_button}>
                                        <Text style={{color: '#f44455'}}>reject</Text>
                                    </View> 
                                </TouchableOpacity>
                            </React.Fragment>
                        }
                        
                        <TouchableOpacity onPress={() => this._showModal(request.id)} activeOpacity={0.7}>
                            <View style={styles.show_qr_button}>
                                <Text style={{color: '#0cc2aa'}}>show qrcode</Text>
                            </View> 
                        </TouchableOpacity>

                        <Modal
                            animationType="slide"
                            transparent={false}
                            visible={this.state.is_modal_visible}>
                            <View style={{marginTop: heightPercentageToDP('37%')}}>
                                <View style={styles.qrcode}>

                                    <QRCode
                                        value={""+request.id}
                                        size={200}
                                        bgColor='#47bac1'
                                        fgColor='white'/>

                                    <TouchableOpacity onPress={() => this._hideModal()} activeOpacity={0.7}>
                                        <View style={styles.hide_qrcode_button}>
                                            <Text style={{color: '#0cc2aa'}}>hide qrcode</Text>
                                        </View> 
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                    </View>
                }
            </Card.Content>
          </Card.Content>
        </Card>
      );
    }
  }

const styles = StyleSheet.create({
    request_button: {
        backgroundColor: "rgba(71, 186, 193, .1)",
        padding: 2,
        borderRadius: 5,
        width: widthPercentageToDP("15%"),
        marginRight: widthPercentageToDP('2%')
    },
    reject_button: {
        backgroundColor: "#f4445533",
        padding: 2,
        borderRadius: 5,
        width: widthPercentageToDP("15%"),
        marginRight: widthPercentageToDP('2%')
    },
    show_qr_button: {
        backgroundColor: "rgba(71, 186, 193, .1)",
        padding: 2,
        borderRadius: 5,
        width: widthPercentageToDP("20%"),
        marginRight: widthPercentageToDP('2%')
    },
    hide_qrcode_button: {
        backgroundColor: "rgba(71, 186, 193, .1)",
        padding: 4,
        borderRadius: 5,
        width: widthPercentageToDP("20%"),
        marginRight: widthPercentageToDP('2%'),
        marginTop: heightPercentageToDP('30%'),
        height: heightPercentageToDP('3%')
    },
    hide_qrcode_scanner: {
        backgroundColor: "#FFF",
        padding: 4,
        borderRadius: 5,
        width: widthPercentageToDP("100%"),
        marginRight: widthPercentageToDP('2%'),
        marginTop: heightPercentageToDP('30%'),
        height: heightPercentageToDP('7%'),
        alignItems: 'center',
        justifyContent: 'center'
    },
    qrcode: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    }
});