import { StyleSheet } from 'react-native'
import { DefaultTheme } from 'react-native-paper'
import { heightPercentageToDP } from 'react-native-responsive-screen';

export const login_styles = StyleSheet.create({ 
    container : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#2e3e4e',
        alignItems: 'center',
    },
    buttonContainer : {
        
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        width: 300,
        paddingVertical: 13,
        marginTop: 15,
        borderRadius: 5,
        
        flexDirection: 'row',
        alignItems: 'center', 
    },
    buttonText: {
        color: '#FFF',
        marginLeft: 100 
    },
    bottomText: {
        fontSize: 15,
        fontWeight: '200',
        color : '#FFF',
        margin: 30,
    },
    inputs: {
        width: 300,
        paddingVertical: 13,
        paddingHorizontal: 15,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
        backgroundColor: 'rgba(0, 0, 0, .1)',
        color: '#FFF',
    }
});



export const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: '#FFF',
      accent: 'rgba(0, 0, 0, .6)',
    },
  };