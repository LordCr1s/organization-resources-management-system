export default class Toast {
    static printToast = (toastType, toastContent, component) => { 

        const setValue = {
            'error': () => {
                return {
                    show_error_toast : toastContent.value,
                    toast_message : toastContent.message
                }
            },
            'success': () => {
                return {
                    show_success_toast : toastContent.value,
                    toast_message : toastContent.message 
                }
            },
            'reset': () => {
                return {
                    show_success_toast : toastContent.value,
                    toast_message : toastContent.message
                }
            }
        }
 
        component.setState({
            ...component.state,
            toast: setValue[toastType]()
        })
    }

    static resetToast = (component) =>{
        this.printToast('reset', {value: false, message: 'N/A'}, component)
      }
}