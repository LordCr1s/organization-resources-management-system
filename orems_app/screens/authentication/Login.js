import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet} from 'react-native'
import { AntDesign, MaterialCommunityIcons, Feather } from '@expo/vector-icons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import { login_styles } from './styles'
import Toast from './toast';
import API from './api';
import Toaster, { ToastStyles } from 'react-native-toaster'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen'


export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user : {
                email: '',
                password: '',
            },
            toast: {
                show_error_toast: false,
                show_success_toast: false,
                toast_message: ''
            },
            verification_code: '',
            passwordResets: {
                password1: '',
                password2: ''
            }
        }
    }


    static navigationOptions = {
        header: null
      }; 

      updateUserInput = (type, value) => {
          let app_state = {
              ...this.state
          }

          const updateInput = {
              email: () => app_state.user.email = value,
              password: () => app_state.user.password = value,
              verification_code: () => app_state.verification_code = value
          }

          updateInput[type]()
          this.setState(app_state)
      }

      login = async () => {        
        const response = await API.makePOSTrequest('login', this.state.user)
        if (response.ok) {
            this.props.navigation.navigate('App')
            Toast.printToast('success', {
                value: true,
                message: "Success!! user is successfully loged in"
            }, this) 
        } else {
            // this.props.navigation.navigate('App')
            Toast.printToast('error', {
                value: true,
                message: "Error!! invalid email or password"
            }, this) 
        }
      }
 
  render() {

   
    

    return ( 
        <KeyboardAwareScrollView
            style={{ backgroundColor: '#2e3e4e' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={login_styles.container}
            scrollEnabled={true}>

            <View style={styles.container}>
               <Text style={[styles.app_name, {fontSize: heightPercentageToDP('6%')}]}>O</Text>
               <Text style={styles.app_name}>REMS</Text>
               <Feather name="database" color="#fcc100" size={36}/>
           </View>

            { this.state.toast.show_error_toast ? 
                <Toaster message = {
                    {
                        text: this.state.toast.toast_message,
                        styles: ToastStyles.error,
                        onHide: () => Toast.resetToast(this)
                    }
                } 
                />: 
                <Text> </Text> 
            }
 
            { this.state.toast.show_success_toast ? 
                < Toaster message = {
                    {
                        text: this.state.toast.toast_message,
                        styles: ToastStyles.success,
                        onHide: () => Toast.resetToast(this)
                    }
                }
                />: 
                <Text> </Text> 
            }
            
           <LoginSection updateUserInput = { this.updateUserInput } />
        </KeyboardAwareScrollView>
    )
  }
}



// default section, a section for user to login
class LoginSection extends Component { 

  render() {
    return (
        <View style={login_styles.buttonContainer}>
            <TextInput
                style={login_styles.inputs}
                placeholder="exmaple@email.com"
                keyboardType={'email-address'}
                onChangeText={(text) => this.props.updateUserInput('email', text)}
                placeholderTextColor="rgba(255, 255, 255, .4)"
            />
            <TextInput
                style={login_styles.inputs}
                placeholder="password" 
                secureTextEntry={true}
                onChangeText={(text) => this.props.updateUserInput('password', text)}
                placeholderTextColor="rgba(255, 255, 255, .4)"
            />
            <TouchableOpacity onPress={() => this.props.goToHomePage()} activeOpacity={0.7}>
                <View style={[login_styles.button, {backgroundColor: '#0cc2aa',}]}>
                    <AntDesign
                        name="login"
                        color="rgba(255, 255, 255, .9)"
                        size={18}
                        style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                    />
                    <Text style={login_styles.buttonText}>Log In</Text>
                </View> 
            </TouchableOpacity>
            <Text style={login_styles.bottomText}> @2019 DIT, Inc. </Text>
        </View>
    )
  }
}



const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      backgroundColor: '#2e3e4e',
      alignItems: 'center',
      justifyContent: 'center',
      width: widthPercentageToDP('100%'),
    },
    app_name : {
        fontFamily: 'noto-bold',
        fontSize: heightPercentageToDP('4%'),
        color: '#FFF'
    }
});