import React from 'react';
import { Platform, StatusBar, StyleSheet, View, AsyncStorage, TouchableOpacity } from 'react-native';
import SplashScreen from './screens/intro/SplashScreen';
import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import Login from './screens/authentication/Login'
import Index from './screens/Index'
import { horms_store } from './screens/Data/index'
import { Provider as PaperProvider } from 'react-native-paper'
import { theme } from './screens/authentication/styles'
import { Font } from 'expo'
import { setCustomText } from 'react-native-global-props'
import { Provider } from 'react-redux'
import ChatRoom from './screens/Home/Post/ChatRoom'
import Announcements from './screens/Home/Post/Announcements'

const AppStack = createStackNavigator({ Home: Index, ChatRoom: ChatRoom, Announcements: Announcements });
const AuthStack = createStackNavigator({Login: Login, });

AppStack.navigationOptions = {
  header: null
}

Index.navigationOptions = {
  header: null
}

const Routing = createAppContainer(createSwitchNavigator(
  {
      App: AppStack,
      Auth: AuthStack,
  },
  {
      initialRouteName: 'App'
  }
))

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isVisible: true,
      fontLoaded: false
    }
  }

  Hide_Splash_Screen=()=>{
    this.setState({ 
      isVisible : false 
    });
  }

  async componentDidMount(){
    await Font.loadAsync({
      'noto-regular': require('./assets/fonts/NotoSans-Regular.ttf'),
      'noto-bold': require('./assets/fonts/NotoSans-Bold.ttf'),
    })

    var that = this;
    setTimeout(function(){
      that.Hide_Splash_Screen();
    }, 1000);

    this.setState({ fontLoaded: true });
    this.setDefaultText()

    await AsyncStorage.setItem('HAS_CAMERA_PERMISSION', 'omakei')
  }

  setDefaultText = () => {
    const customTextProps = {
      style: {
        fontFamily: 'noto-regular',
        color: 'rgba(0, 0, 0, .8)'
      }
    }
    setCustomText(customTextProps)
  }

  render() {

    let Splash_Screen = <SplashScreen />;

    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          
        { (this.state.isVisible === true) ? Splash_Screen: 
            <Provider store={horms_store}>
              <PaperProvider theme={theme}>
                { this.state.fontLoaded ? <Routing style={{ fontFamily: 'noto-regular' }}/> : null }
              </PaperProvider>
            </Provider> 
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});














// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';
// import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
// import { Font } from 'expo'
// import { setCustomText } from 'react-native-global-props'
// import { Provider as PaperProvider } from 'react-native-paper'

// // application screens
// import Login from './screens/Login'
// import Home from './screens/app/Home'
// import SplashScreen from './screens/SplashScreen'

// // authentication and application routes
// const AuthStack = createStackNavigator({Login: Login}, {initialRouteName: 'Login'})
// const AppStack = createStackNavigator({Home: Home}, {initialRouteName: 'Home'})

// AuthStack.navigationOptions = {
//   header: null
// }

// AppStack.navigationOptions = {
//   header: null
// }


// // application main routers
// const Router = createAppContainer(createSwitchNavigator(
//   {
//     Auth: AuthStack,
//     App: AppStack
//   },
//   {
//     initialRouteName: 'App'
//   }
// ))


// export default class App extends React.Component {

//   constructor(props) {
//     super(props)
//     this.state = {
//       // splash screen controll
//       isVisible : true,

//       // loading fonts controll
//       isFontLoaded: false
//     }
//   }

//   hideSplashScreen=()=>{
//     this.setState({ 
//       isVisible : false 
//     });
//   }


//   setDefaultText = () => {
//     const customTextProps = {
//       style: {
//         fontFamily: 'noto-regular',
//         color: 'rgba(0, 0, 0, .8)'
//       }
//     }
//     setCustomText(customTextProps)
//   }


//   async componentDidMount(){

//     // load fonts
//     await Font.loadAsync({
//       'noto-regular': require('./assets/fonts/NotoSans-Regular.ttf'),
//       'noto-bold': require('./assets/fonts/NotoSans-Bold.ttf'),
//     })

//     // load splash screen
//     var that = this;
//     setTimeout(function(){
//       that.hideSplashScreen();
//     }, 1000);

//     this.setState({ fontLoaded: true });
//     this.setDefaultText()
//   }

//   render() {

//     let splashScreen = <SplashScreen />
//     let router = <Router />

//     return (
//       <View style={styles.container}>
//         { (this.state.isVisible === true) ? 
//             this.state.fontLoaded ? splashScreen :
//               null : <PaperProvider>{router}</PaperProvider>
//         }
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
