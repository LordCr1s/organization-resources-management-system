import React from 'react';
import { widthPercentageToDP as widthTodp, heightPercentageToDP as heightTodp } from 'react-native-responsive-screen'
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs"
import { AntDesign, MaterialIcons, Entypo } from '@expo/vector-icons'

// screens
import Materials from './Home/Materials'
import Requests from './Home/Requests'
import Communication from './Home/Communication'


export default createMaterialBottomTabNavigator({
    materials: { screen: Materials,  },
    communication: { screen: Communication, },
    requests: { screen: Requests, },
  }, 
  {
    initialRouteName: 'materials',
    activeColor: 'rgba(0, 0, 0, .6)',
    inactiveColor: 'rgba(0, 0, 0, .4)',
    barStyle: { height: heightTodp('6.7%') },
  });
  

Materials.navigationOptions = {
    title: 'Home',
    tabBarIcon: <MaterialIcons name="home" size={20} color={'rgba(0, 0, 0, .6)'} />
}

Communication.navigationOptions = {
    title: 'Dashboard',
    tabBarIcon: <MaterialIcons name="bookmark" size={20} color={'rgba(0, 0, 0, .6)'}/>
}

Requests.navigationOptions = {
    title: 'My Posts',
    tabBarIcon: <AntDesign name="star" size={20} color={'rgba(0, 0, 0, .6)'}/>
}
