import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, StyleSheet} from 'react-native'
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    updateUserInput = (type, value) => {
        
    }

    login = async () => {        
    
    }
 
  render() {

    return ( 
        <KeyboardAwareScrollView
            style={{ backgroundColor: 'red' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={styles.container}
            scrollEnabled={true}>
            <Text style={styles.title}> {"\tWelcome \n \tBack."} </Text>
            
            <View style={styles.buttonContainer}>
                <TextInput
                    style={styles.inputs}
                    placeholder="exmaple@email.com"
                    keyboardType={'email-address'}
                    onChangeText={(text) => this.state.updateUserInput('email', text)}
                />
                <TextInput
                    style={styles.inputs}
                    placeholder="password" 
                    secureTextEntry={true}
                    onChangeText={(text) => this.state.updateUserInput('password', text)}
                />
                <TouchableOpacity onPress={() => this.state.login()} activeOpacity={0.7}>
                    <View style={[styles.button, {backgroundColor: 'rgba(0, 0, 0, .6)',}]}>
                        <AntDesign
                            name="login"
                            color="rgba(255, 255, 255, .9)"
                            size={18}
                            style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                        />
                        <Text style={styles.buttonText}>Log In</Text>
                    </View> 
                </TouchableOpacity>
                <Text style={styles.bottomText}> @2019 DIT, Inc. </Text>
            </View>
        </KeyboardAwareScrollView>
    )
  }
}


const styles = StyleSheet.create({ 
    container : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    image: {
        width: 400,
        height: 350,
        resizeMode: 'center',
        position: 'absolute',
        top: 100, 
    },
    title: { 
        fontSize: 50, 
        fontWeight: '300',
        paddingHorizontal: 16,
        color: 'rgba(0, 0, 0, .6)',
        position: 'absolute', 
        top: 50,
        left: 10, 
    },
    buttonContainer : {
        flex : 1,
        flexDirection: 'column',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        alignItems: 'center',
    },
    button: {
        width: 300,
        paddingVertical: 13,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
    },
    buttonText: {
        color: '#fff',
        marginLeft: 100 
    },
    bottomText: {
        fontSize: 15,
        fontWeight: '200',
        color : 'rgba(0, 0, 0, .4)',
        margin: 30,
    },
    inputs: {
        width: 300, 
        paddingVertical: 13,
        paddingHorizontal: 15,
        marginTop: 15,
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center', 
        backgroundColor: 'rgba(0, 0, 0, .1)',
    }
});