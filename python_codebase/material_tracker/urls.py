from django.urls import path
from material_tracker.views import GetLogsByMaterial


urlpatterns = [
    path('getlogs/<int:pk>', GetLogsByMaterial.as_view()),
]
