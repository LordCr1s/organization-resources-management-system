from django.contrib import admin
from material_tracker.models import Log

# Register your models here.
@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('material_id', 'source', 'destination', 'logged_at')