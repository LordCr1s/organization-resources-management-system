from django.apps import AppConfig


class MaterialTrackerConfig(AppConfig):
    name = 'material_tracker'
