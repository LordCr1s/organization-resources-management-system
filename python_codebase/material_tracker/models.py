from django.db import models

class Log(models.Model):

    # this id will be used to fetch material information from php codebase
    material_id = models.IntegerField()
    
    # source and destination defines the users involved in a log text
    # they will be both used to refer to actual users from php codebase
    source = models.IntegerField()
    destination = models.IntegerField()

    # definition of what is done and when it was done
    action = models.CharField(max_length=300)
    logged_at = models.DateField(auto_now=True)


    logs = models.Manager()

    def __str__(self):
        return self.action