from rest_framework import serializers
from material_tracker.models import Log
from rest_framework.views import APIView
from rest_framework.response import Response
from authentication.views import is_authenticated


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = '__all__'
        read_only_fields = ('material_id', 'source', 'destination', 'action', 'logged_at')


# you can only get logs from api, you can not delete or update logs through api
class GetLogsByMaterial(APIView):

    def get(self, request, pk, format=None):
        if is_authenticated(request.GET['ZkWE'], int(request.GET['tGjx'])):
            logs = Log.logs.filter(material_id=pk)
            serialized_logs = LogSerializer(logs, many=True)
            return Response(serialized_logs.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


# this function is called when the request is verified with
# QR code scanning to create logs for tracking a material
def create_log(material_id, source, destination, action):

    # check if the source and destination are the same to validate logging
    if source == destination:
        return False

    log = Log.logs.create(material_id=material_id, source=source, destination=destination, action=action)
    log.save()
    return True