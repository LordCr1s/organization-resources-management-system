from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('material_tracker/', include('material_tracker.urls')),
    path('auth/', include('authentication.urls')),
    path('communication/', include('communication.urls')),
    path('request/', include('requests_manager.urls')),
    path('admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)