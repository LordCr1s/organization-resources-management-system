from rest_framework import serializers, status
from material_tracker.models import Log
from rest_framework.views import APIView
from rest_framework.response import Response
from requests_manager.models import Request, RejectedMessage, QRcode
from authentication.views import is_authenticated
from material_tracker.views import create_log

class RequestSerializer(serializers.ModelSerializer):

    message = serializers.CharField(required=False, allow_blank=True, max_length=100)
    class Meta:
        model = Request
        fields = '__all__'


class QRcodeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = QRcode
        fields = '__all__'


class RequestList(APIView):
     
    # requests are only retreived for specific users, pk is the user id
    def get(self, request, user_id):
        if is_authenticated(request.GET['ZkWE'], int(request.GET['tGjx'])):
            source_requests = Request.requests.filter(source=user_id)
            destination_requests = Request.requests.filter(destination=user_id)

            # merging the two querysets
            requests = (source_requests | destination_requests).order_by('-updated_at')

            serializer = RequestSerializer(requests, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
    
    def post(self, request):
        if is_authenticated(request.GET['ZkWE'], int(request.GET['tGjx'])):

            serializer = RequestSerializer(request.data)
            
            Request.requests.create(
                source = int(serializer.data['source']),
                destination = int(serializer.data['destination']),
                material = int(serializer.data['material']),
                request_type = serializer.data['request_type'],
                action = serializer.data['action'],
            ).save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
    
    def put(self, request, request_id):
        if is_authenticated(request.GET['ZkWE'], int(request.GET['tGjx'])):

            try:
                material_request = Request.requests.get(id=request_id)
            except Request.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

            serializer = RequestSerializer(request.data)
            material_request.status = serializer.data['status']
            material_request.is_replied = True

            if serializer.data['status'] == 'verified':
                material_request.is_verified = True
                material_request.is_completed = True

                # create log to track the position of the material
                create_log(material_request.material, material_request.destination, material_request.source, "has given material to")
            

            if serializer.data['status'] == 'rejected':
                RejectedMessage.messages.create(request=material_request, message=serializer.data['message']).save()
                material_request.is_completed = True
            
            if serializer.data['status'] == 'canceled':
                material_request.is_completed = True
            
            material_request.save()

            return Response(data=serializer.data, status=status.HTTP_200_OK)

        else:
            return Response(status=status.HTTP_403_FORBIDDEN)