from django.urls import path
from requests_manager.views import RequestList


urlpatterns = [
    path('getrequests/<int:user_id>', RequestList.as_view()),
    path('makerequest', RequestList.as_view()),
    path('updaterequest/<int:request_id>', RequestList.as_view()),
]
