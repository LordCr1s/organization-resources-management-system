from django.apps import AppConfig


class RequestsManagerConfig(AppConfig):
    name = 'requests_manager'
