from django.db import models


class Request(models.Model):

    # source and destination represents a user who sent a request and
    # a user who received a request, they both hold the id's referenced 
    # to php codebase users
    source = models.IntegerField()
    destination = models.IntegerField()

    # id of the material, will be used to reference actual material id from php codebase
    material = models.IntegerField()

    # type and status of request are used by material tracking app to create logs
    REQUEST_TYPES = [('requesting', 'requesting'), ('returning', 'returning')]
    REQUEST_STATUSES = [
        ('pending', 'pending'), 
        ('accepted', 'accepted'), 
        ('rejected', 'rejected'),
        ('canceled', 'canceled'), 
        ('verified', 'verified')
    ]

    request_type = models.CharField(max_length=30, choices=REQUEST_TYPES, default='requesting')
    status = models.CharField(max_length=30, choices=REQUEST_STATUSES, default='pending')

    # actin defines what request is intended for eg:- is user requesting
    action = models.CharField(max_length=200)
    updated_at = models.DateTimeField(auto_now=True)
    is_replied = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    
    requests = models.Manager() 


class RejectedMessage(models.Model):
    """
        This class is intended to give a user who has placed a request a reason to 
        why his/her request is rejected by the other user
    """
    request = models.OneToOneField(Request, on_delete=models.CASCADE)
    message = models.CharField(max_length=255)

    messages = models.Manager()

    def __str__(self):
        return self.message


class QRcode(models.Model):

    request = models.OneToOneField(Request, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="requests/qrcode/%Y/%M/%d")

    qr_codes = models.Manager()

    def __str__(self):
        return str(self.request.material)