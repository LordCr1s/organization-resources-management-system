from django.shortcuts import render
from rest_framework import serializers, status
from rest_framework.views import APIView
from rest_framework.response import Response
from communication.models import Message
from django import forms
from authentication.views import is_authenticated


class MessageSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Message
        fields = '__all__'

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ()


class MessageView(APIView):

    def get(self, request, department_id, message_type):

        if is_authenticated(request.GET['ZkWE'], int(request.GET['tGjx'])):
            # return messages for specific department and specific section type
            messages = Message.messages.filter(department=department_id, type=message_type).all().order_by('-sent_at')
            serialized_messages = MessageSerializer(messages, many=True)
            return Response(serialized_messages.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
    
    def post(self, request):
        
        if is_authenticated(request.GET['ZkWE'], int(request.GET['tGjx'])):

            message = MessageSerializer(request.data)
            Message.messages.create(
                sender=message.data['sender'],
                type=message.data['type'],
                department=message.data['department'],
                content=message.data['content']
            ).save()
            
            return Response(status=status.HTTP_200_OK, data=message.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)