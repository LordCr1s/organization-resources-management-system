from django.db import models
from ckeditor.fields import RichTextField

class Message(models.Model):

    # this field points to the user id from php codebase
    sender = models.IntegerField()

    # type defines the scope of the message
    CHOICES = [('announcement', 'announcement'), ('complain', 'complain')]
    type = models.CharField(max_length=30, choices=CHOICES, default='complain')

    # department id determines where the message should be viewed
    department = models.IntegerField()

    sent_at = models.DateTimeField(auto_now=True)
    content = models.TextField()

    messages = models.Manager()

    def __str__(self):
        return self.type