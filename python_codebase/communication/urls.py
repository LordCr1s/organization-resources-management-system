from django.urls import path
from communication.views import MessageView


urlpatterns = [
    path('getmessages/<int:department_id>/<str:message_type>', MessageView.as_view()),
    path('postmessages', MessageView.as_view()),
]
