from django.db import models

class Token(models.Model):

    # this user id will be used to fecth user information from php codebase
    user_id = models.IntegerField(unique=True)

    # this token will be used to authenticate user and it will be 
    # generated during login by php codebase
    token = models.CharField(max_length=300)
    is_active = models.BooleanField(default=True)

    tokens = models.Manager()

    def __str__(self):
        return self.token