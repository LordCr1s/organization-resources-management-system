from django.shortcuts import render
from authentication.models import Token
from rest_framework import serializers, status
from rest_framework.views import APIView
from rest_framework.response import Response
import requests


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = '__all__'


# when user logs in save the token and their id
class Login(APIView):

    def post(self, request):
        serialized_token_info = TokenSerializer(request.data, many=False)
        token_is_created = set_token(serialized_token_info.data['token'], serialized_token_info.data['user_id'])
        if token_is_created:
            return Response(serialized_token_info.data)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


# when user logs in save the token and their id
class Logout(APIView):

    def post(self, request):
        serialized_token_info = TokenSerializer(request.data, many=False)
        token_is_destroyed = destroy_token(serialized_token_info.data['token'], serialized_token_info.data['user_id'])
        if token_is_destroyed:
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


def is_authenticated(token, user_id):

    try:
        token = Token.tokens.get(token=token)
    except Token.DoesNotExist:
        return False
    
    # check if the token is valid and that the user is the 
    # owner of the token
    if token.is_active:
        if token.user_id == user_id:
            return True
            
    return False
    

def set_token(new_token, user_id):
    
    # check if a token with same user id exists and update it's token
    try:
        old_token = Token.tokens.get(user_id=user_id)
        return update_token(old_token, new_token)

    # if it doesn't exist create new token
    except Token.DoesNotExist:
        return create_token(new_token, user_id)


# update the existing token in the database
def update_token(old_token, new_token):

    old_token.token = new_token
    old_token.is_active = True
    old_token.save()
    return True


# create new token in the database
def create_token(new_token, user_id):
    
    # validate a provided token with php codebase before saving it
    validate_token = requests.get('url%s'%(new_token))

    if validate_token.status_code == 200:
        created_token = Token.tokens.create(user_id=user_id, token=new_token)
        created_token.save()
        return True

    return False


# this function is called when a user is loged out
def destroy_token(token, user_id):

    try:
        token = Token.tokens.get(token=token, user_id=user_id)
    except Token.DoesNotExist:
        return False

    token.is_active = False
    token.save()
    return True