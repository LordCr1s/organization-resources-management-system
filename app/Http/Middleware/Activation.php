<?php

namespace App\Http\Middleware;

use Closure;

class Activation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!($request->session()->exists('activation_code') && !$request->session()->exists('departiment')) ){
            return redirect('/activation_code');
        }
        return $next($request);
    }
}
