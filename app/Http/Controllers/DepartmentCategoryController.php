<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryDepartment;

class DepartmentCategoryController extends Controller
{
    public function index()
    {
        $categories = CategoryDepartment::all();
        return response()->json($categories);
    }

    public function store(Request $request)
    {
        $category_data = $request->validate([
            'name' => 'required|max:255',
            'description'  => 'required|max:255',
        ]);
        $category = new CategoryDepartment;
        $category->name = $request->name;
        $category->description = $request->description;

        if( $category->saveOrFail() )
            return response()->json($category, 200);
        else
            return response()->json("failed to store departments", 403);
    }

    public function show($id)
    {
        $category = CategoryDepartment::findOrfail($id);
        return response()->json($category, 200);
    }


    public function update(Request $request, $id)
    {
        $category_data = $request->validate([
            'name' => 'max:255',
            'description'  => 'max:255',
        ]);
        $category = CategoryDepartment::findOrfail($id);

        if( $category->update($category_data) )
            return response()->json($category, 200);
        else
            return response()->json("failed to update departments", 403);
    }

    public function destroy($id)
    {

        if( CategoryDepartment::destroy($id) )
            return response()->json("Deleted successfully", 200);
        else
            return response()->json("failed to delete departments", 403);
    }
}
