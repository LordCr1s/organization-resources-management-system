<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;

class DepartmentsController extends Controller
{
    
    public function index()
    {
        $departments = Department::orderBy('created_at','DESC')->get()->each(
            function($department){
               $department->category;
            }
        );
        return response()->json($departments);
    }

    public function store(Request $request)
    {
        $department_data = $request->validate([
            'name' => 'required|max:255',
            'description'  => 'required|max:255',
        ]);
        $department = new Department;
        $department->name = $request->name;
        $department->description = $request->description;
        $department->category_department_id = $request->category_department_id;

        if( $department->saveOrFail() )
            return response()->json($department, 200);
        else
            return response()->json("failed to store departments", 403);
    }

    public function show($id)
    {
        $department = Department::findOrfail($id)->category;
        return response()->json($department, 200);
    }


    public function update(Request $request, $id)
    {
        $department_data = $request->validate([
            'name' => 'max:255',
            'description'  => 'max:255',
            'category_department_id' => 'integer'
        ]);
        $department = Department::findOrfail($id);

        if( $department->update($department_data) )
            return response()->json($department, 200);
        else
            return response()->json("failed to update departments", 403);
    }

    public function getStores($id){
        $stores = Department::findOrfail($id)->stores;
        return response()->json($stores,200);
    }
    public function destroy($id)
    {

        if( Department::destroy($id) )
            return response()->json("Deleted successfully", 200);
        else
            return response()->json("failed to delete departments", 403);
    }
}
