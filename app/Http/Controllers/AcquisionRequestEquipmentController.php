<?php

namespace App\Http\Controllers;

use App\AcquisionRequestEquipment as AcqEquipment;
use Illuminate\Http\Request;

class AcquisionRequestEquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipments = AcqEquipment::all();
        return response()->json($equipments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $equipment_data = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'quantity' => 'required|max:255',
            'acquision_request_id' => 'integer',
        ]);
        // dd($equipment_data);
        $acq_equipments = new AcqEquipment;
        $acq_equipments->fill($equipment_data);

        if($acq_equipments->saveOrFail())
            return response()->json($acq_equipments, 200);
        else {
            return response()->json("failed", 403);
        }
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcquisionRequestEquipment  $acquisionRequestEquipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equipment_data = $request->validate([
            'name' => 'max:255',
            'description' => 'max:255',
            'quantity' => 'max:255',
            'acquision_request_id' => 'integer',
        ]);
        $acq_equipments = AcqEquipment::findOrfail($id);

        if($acq_equipments->update($equipment_data))
            return response()->json($acq_equipments, 200);
        else {
            return response()->json("failed", 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcquisionRequestEquipment  $acquisionRequestEquipment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(AcqEquipment::destroy($id))
            return response()->json("Deleted", 200);
        else {
            return response()->json("failed", 403);
        }
    }
}
