<?php

namespace App\Http\Controllers;

use App\ReceivedNote;
use Illuminate\Http\Request;

class ReceivedNoteController extends Controller
{
    /**
     * Display a ligrning of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grns = ReceivedNote::all()
        ->each(
            function($grn){
                $department =$grn->department;
                $grn->supplier;
                $grn->approved_user;
            }
        );
        return response()->json($grns, 200);
    } 

    /**
     * grnore a newly created resource in grnorage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grn_data = $request->validate([
            'grn_no' => 'required|max:255',
            'department_id' => 'required|max:255',
            'supplier_id' => 'integer|max:255',
            'received_user' => 'nullable',
            'approved_user' => 'nullable',
            'verified_user' => 'nullable'
        ]);

        $grn = new ReceivedNote;
        $grn->fill($grn_data);

        if($grn->saveOrFail())
            return response()->json($grn, 200);
        else {
            return response()->json("Failed to save transaction", 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReceivedNote  $receivedNote
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grn = ReceivedNote::findOrfail($id);
        $grn->department;
        $grn->supplier;
        return response()->json($grn, 200,['Message-from-developer'=>"watch ur back shoo"]);
    }

    /**
     * Update the specified resource in grnorage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReceivedNote  $receivedNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grn = ReceivedNote::findOrfail($id);

        if($grn->update($request->all()))
            return response()->json($grn, 200);
        else {
            return response()->json("Failed to update transaction", 402);
        }
    }

    /**
     * Remove the specified resource from grnorage.
     *
     * @param  \App\ReceivedNote  $receivedNote
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(ReceivedNote::destroy($id))
            return response()->json("Transaction Evidence destroyed ", 200);
        else {
            return response()->json("Failed to delete transaction you loser", 402);
        }
    }
}
