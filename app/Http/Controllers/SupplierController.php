<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
   
    public function index()
    {
        $suppliers = Supplier::all();
        return response()->json($suppliers, 200);
    }

    public function store(Request $request)
    {
        $supplier =  new Supplier;
        $supplier->fill($request->all());
        if($supplier->saveOrFail()){
            return response()->json('Added', 200);
        }
    }

    public function show($id)
    {
        $supplier = Supplier::findOrfail($id);
        return response()->json($supplier, 200);
    }

    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrfail($id);
        if($supplier->update($request->all())){
            return response()->json($supplier, 200);
        }
    }

    public function destroy($id)
    {
        if(Supplier::destroy($id)){
            return response()->json('deleted', 200);
        }
    }
}
