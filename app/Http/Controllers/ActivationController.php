<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ActivationCode;

class ActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all_activation_codes()
    {
        $activation_codes = ActivationCode::all();
        return response()->json(['codes'=> $activation_codes], 200);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( auth('api')->check()){
        $id = auth('api')->user()->id;
        $time = time();
        $activation_code = new ActivationCode;
        $activation_code->activation_code = "OREMS-" .auth('api')->user()->department->id. "-". $id ."-" . $time;
        $activation_code->department_id = auth('api')->user()->department->id;
        $activation_code->role_id = $request->role;
        $activation_code->expire = 'No';
        $activation_code->save();
        $res = "successful created";
                
        return Response()->json($res,200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function compare(Request $request)
    {
        $activation_codes = ActivationCode::where('activation_code', $request->activation_code)->first();
        date_default_timezone_set('Africa/Dar_es_salaam');
         //return Response()->json(date("Y-m-d h:i:s"));
         if($activation_codes != null){
            if(($activation_codes->activation_code == $request->activation_code) && ($activation_codes->expire == 'No' )){
                $data =
                [   'res'=>"your successiful activate the account",
                    'redirect'=> 'register',
                    'department'=> $activation_codes->department_id,
                    'role'=> $activation_codes->role_id,
                    'activation_code'=> $activation_codes->activation_code,
                ];
                $activation_codes->expire = 'Yes';
                $activation_codes->save();
                return Response()->json(['data'=> $data, 'status'=> 200],200);
            }
            elseif(($activation_codes->activation_code == $request->activation_code) && ($activation_codes->expire == 'Yes' )){
                $error = "Expired activation code";
                return Response()->json(['error'=>$error, 'status'=>401],401);
            }
         }else {
                $error = "Invalid activation code";       
                return Response()->json(['error'=>$error, 'status'=>401],401);
            }

       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code = ActivationCode::findOrFail($id);
        $code->delete();
        $res = 'deleted succesful';
        return Response()->json($res,200);
    }
}
