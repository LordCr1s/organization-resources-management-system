<?php

namespace App\Http\Controllers;

use App\StoreTransaction;
use App\Equipment;
use Illuminate\Http\Request;
use DB;

class StoreTransactionController extends Controller
{
    public $header = ['Message-from-developer'=>"watch ur back omakei"];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $st = DB::select(
            'SELECT ST.id,ST.transaction_number,AR.acquision_number,AR.description AS `Acquisition Description`,
            departments.name AS Department,U.firstname AS `Receiving User`,U.status AS `User Status`,ST.created_at,ST.updated_at
            FROM `store_transactions` AS ST
            JOIN departments ON  ST.department_id = departments.id
            JOIN users AS U ON  ST.receiving_user = U.id
            JOIN acquision_requests AS AR ON  ST.acquision_id = AR.id');
        return response()->json($st, 200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $st_data = $request->validate([
            // 'transaction_number' => 'required|max:255',
            'department_id' => 'required|integer',
            'acquision_id' => 'required|integer',
            'receiving_user' => 'required|integer',
        ]);

        $st = new StoreTransaction;
        $st->create([
            'transaction_number' => 'OREMS'.now()->year.now()->month.now()->day.$st->id,
            'department_id' => $request->department_id,
            'acquision_id' => $request->acquision_id,
            'receiving_user' => $request->receiving_user
        ]);

        if($st->save()){
            return response()->json($st, 200);
        }
        else {
            return response()->json("Failed to save transaction", 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StoreTransaction  $storeTransaction
     * @return \Illuminate\Http\Response
     */
    public function showTransaction($id)
    {
        $st = DB::table('store_transactions AS ST')->where('ST.id',$id)
        ->join('departments AS D','ST.department_id','=','D.id')
        ->join('users AS U','ST.receiving_user','=','U.id')
        ->join('acquision_requests AS AR','ST.acquision_id','=','AR.id')
        ->select('ST.id','ST.transaction_number',
                'AR.acquision_number','AR.description AS `Acquisition Description`',
                'D.id AS department_id','D.name AS `Department Name`',
                'U.id AS user_id','U.status AS `user_status`',
                'U.firstname','U.surname','U.lastname',
                'ST.created_at','ST.updated_at')
        // ->with('equipment_store_transactions')
        ->get();
        return response()->json($st, 200,$this->header);
    }
    public function showTransactionEquipments($id)
    {
        $stE = DB::select('SELECT EST.store_transaction_id,E.id As `equipment_id`,
        E.name AS `equipment_name`,S.name AS `store_name`,RN.grn_no,
        ST.transaction_number, D.name AS `department_name`,
        U.firstname ,U.surname, U.lastname,
        ST.created_at AS `transaction_created_at`,ST.updated_at AS `transaction_updated_at`
            FROM equipment_store_transactions AS EST
            JOIN store_transactions AS ST ON EST.store_transaction_id = ST.id
            JOIN equipments AS E ON EST.equipment_id = E.id
            JOIN departments AS D ON ST.department_id = D.id
            JOIN users AS U ON ST.receiving_user = U.id
            JOIN stores AS S ON E.store_id = S.id
            JOIN received_notes AS RN ON E.grn_id = RN.id
            WHERE EST.store_transaction_id='.$id);

        return response()->json($stE, 200,$this->header);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StoreTransaction  $storeTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $st_data = $request->validate([
            'transaction_number' => 'max:255',
            'department_id' => 'integer',
            'acquision_id' => 'integer',
            'receiving_user' => 'integer',
        ]);

        $st = StoreTransaction::findOrfail($id);

        if($st->update($st_data))
            return response()->json($st, 200);
        else {
            return response()->json("Failed to update transaction", 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoreTransaction  $storeTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(StoreTransaction::destroy($id))
            return response()->json("Transaction Evidence destroyed ", 200);
        else {
            return response()->json("Failed to delete transaction you loser", 402);
        }
    }
}
