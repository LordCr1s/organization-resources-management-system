<?php

namespace App\Http\Controllers;

use App\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgs = Organization::all();
        return response()->json($orgs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $organization_data = $request->validate([
            'organization_name' => 'required|max:255',
            'address'  => 'required|max:255',
            'email'  => 'required|max:255',
            'telephone'  => 'required|max:255',
            'fax' => 'required|max:255',
        ]);
        // dd($organization_data);
        $org = new Organization();
        $org->fill($organization_data);

        if( $org->saveOrFail() )
            return response()->json($org, 200);
        else
            return response()->json("failed to store organzition", 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organizaiton  $organizaiton
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizaiton = Organization::findOrfail($id);
        return response()->json(['organization' => $organizaiton]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organizaiton
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Request data must be through x-www-form-urlEncoded or Json
        $organization_data = $request->validate([
            'organization_name' => 'max:255',
            'address'  => 'max:255',
            'email'  => 'max:255',
            'telephone'  => 'max:255',
            'fax' => 'max:255',
        ]);
        // dd($organization_data);
        $org = Organization::findOrfail($id);
        if( $org->update($organization_data))
            return response()->json($org, 200);
        else
            return response()->json("failed to update organzition", 403);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organizaiton
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $organization = Organization::findOrfail($id);
        // return $organization;

        if(Organization::destroy($id))
            return response('success Organization deleted',200);
        else
            return response()->json("failed to delete organzition", 403);
    }
}
