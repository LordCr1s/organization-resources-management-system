<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ActivationCode;
use App\Department;
use App\Role;
use App\Organization;
use App\Permission;

class UserManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all_users()
    {
        $user = auth('api')->user();
        //dd($user->hasRole('PR'));
        if($user->hasRole('AD') || $user->hasRole('PR')){
            $users = User::all()->each(
                function($dep){
                    $dep->department;
                    $dep->roles;
                }
            );
            return response()->json(['users'=> $users, 'status'=>200], 200);
        }elseif ($user->hasRole('HOD') || $user->hasRole('HPMU')) {
            $users = User::where('department_id',auth('api')->user()->department_id )->get();
            return response()->json(['users'=> $users, 'status'=>200], 200);
        }
        else {
            return response()->json(['error'=>'you dont have role or permission', 'status'=> 412], 412);
        }

        
    }

    public function all_roles()
    {
        $user = auth('api')->user();
        if($user->hasRole('AD') || $user->hasRole('PR')){
            $roles = Role::all()->each(
                function ($role){
                    $role->permissions;
                }
            );
            return response()->json(['roles'=>$roles, 'status'=>200], 200);
        }elseif ($user->hasRole('HOD') || $user->hasRole('HPMU')) {
            $roles = Role::where('department_id',auth('api')->user()->department_id )->get();
            return response()->json(['roles'=>$roles, 'status'=>200], 200);
        }
        else {
            return response()->json('you dont have role or permission', 412);
        }
    }

    public function all_permissions()
    {
        $user = auth('api')->user();
        if($user->hasRole('AD') || $user->hasRole('PR')){
            $permissions = Permission::all();
            return response()->json(['permissions'=>$permissions, 'status'=>200], 200);
        }elseif ($user->hasRole('HOD') || $user->hasRole('HPMU')) {
            $permissions = Permission::where('department_id',auth('api')->user()->department_id )->get();
            return response()->json(['permissions'=>$permissions, 'status'=>200], 200);
        }
        else {
            return response()->json('you dont have role or permission', 412);
        }
       
    }

     /**
     * Update user account status in the database.
     * @param  int  $id
     * @param  string $action
     * @return \Illuminate\Http\Response
     */
    public function update_account_status($action, $id)
    {

        $user = auth('api')->user();
        if($user->hasRole('AD') || $user->hasRole('PR') || $user->hasRole('HOD') || $user->hasRole('HPMU')){
            $user_for_change = User::findOrFail($id);
                if($action=='activate'){
                    $user_for_change->status = 'Active';
                    $user_for_change->save();
                    return response()->json('user account activated successiful', 200);

                }elseif ($action=='suspend') {
                    $user_for_change->status = 'Suspended';
                    $user_for_change->save();
                    return response()->json('user account suspended successiful',200);

                }elseif($action=='delete') {
                    $user_for_change->status = 'Deleted';
                    $user_for_change->save();
                    return response()->json('user account deleted successiful', 200);
                }else {
                    return response()->json('there is an error in changing the user status',503);
                }
        }
        else {
            return response()->json('you dont have role or permission', 200);
        }
               
    }

     /**
     * Update user account role to tender board role in the database.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function change_user_role($action,$id, $role){
        $user = User::findOrFail($id);
        if($action=='add'){
            $user->roles()->attach(Role::where('name', $role)->first());
            return response()->json('user role added successiful', 200);

        }elseif ($action=='remove') {
            $user->roles()->detach(Role::where('name', $role)->first());
            return response()->json('user role removed successiful', 200);

        }else {
            return response()->json('an erroe occured during add or remove the role', 503);
        }

     }

       /**
     * Update user account role to evaluation committe role in the database.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function change_user_permission($action,$id, $permission){
        $user = User::findOrFail($id);
        if($action=='add'){
            $user->roles()->attach(Role::where('name', $permission)->first());
            return back()->with('user permission added successiful', 200);

        }elseif ($action=='remove') {
            $user->roles()->detach(Role::where('name', $permission)->first());
            return back()->with('user permission removed successiful', 200);
            
        }else {
            return back()->with('an erroe occured during add or remove the permission', 503);
        }

     }
}