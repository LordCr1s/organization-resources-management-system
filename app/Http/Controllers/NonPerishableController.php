<?php

namespace App\Http\Controllers;

use App\NonPerishable;
use Illuminate\Http\Request;

class NonPerishableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NonPerishable  $nonPerishable
     * @return \Illuminate\Http\Response
     */
    public function show(NonPerishable $nonPerishable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NonPerishable  $nonPerishable
     * @return \Illuminate\Http\Response
     */
    public function edit(NonPerishable $nonPerishable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NonPerishable  $nonPerishable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NonPerishable $nonPerishable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NonPerishable  $nonPerishable
     * @return \Illuminate\Http\Response
     */
    public function destroy(NonPerishable $nonPerishable)
    {
        //
    }
}
