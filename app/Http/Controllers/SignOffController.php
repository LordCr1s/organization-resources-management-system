<?php

namespace App\Http\Controllers;

use App\SignOff;
use Illuminate\Http\Request;

class SignOffController extends Controller
{
    public function index()
    {
        $signs = SignOff::all();
        return response()->json($signs, 200);
    }

    public function store(Request $request)
    {
        $sign =  new SignOff;
        $sign->fill($request->all());
        if($sign->saveOrFail()){
            return response()->json('Added', 200);
        }
    }

    public function show($id)
    {
        $sign = SignOff::findOrfail($id);
        return response()->json($sign, 200);
    }

    public function update(Request $request, $id)
    {
        $sign = SignOff::findOrfail($id);
        if($sign->update($request->all())){
            return response()->json($sign, 200);
        }
    }

    public function destroy($id)
    {
        if(SignOff::destroy($id)){
            return response()->json('deleted', 200);
        }
    }
}
