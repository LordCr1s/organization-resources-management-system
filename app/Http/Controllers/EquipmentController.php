<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\Perishable;
use App\NonPerishable as NP;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    public function index()
    {
        $equipments = Equipment::orderBy('created_at', 'DESC')->get()->each(
            function($eq){
                $eq->store->department;
                $grn = $eq->receivedNote;
                // $grn->approved_user;
            }
        );
        
        return response()->json($equipments,200);        
    }

    public function store(Request $request)
    {
        $eq_data = $request->all();
            
        $eq = new Equipment;
        
        if( $request->has('image_path')){
            // $fileExt = $request->image_path->getClientOriginalExtension;
            // $fileName =$fileExt = $request->image_path->getClientOriginalName;
            // $image_path = 'images/'.now().$eq_data->image_path;

            // $eq_data->image_path = $image_path;
            // dd($request->all());
            //save in dir
            foreach( $request->image_path as $image){
                $image->store('images');
            }
        }

        if($request->has('equipmentList')){
            $equipmentList = json_decode($request->equipmentList,true);
            $equipmentList->each(
                function($equipment){
                   $eq->name = $equipment->name;
                   $eq->store_id = $request->store_id;
                   $eq->grn_id = $request->grn_id;
                   $eq->saveOrFail();
                });
        }else{

            $eq->fill($eq_data);
            $eq->saveOrFail();
        }
        return response()->json($eq,200);  
        
    }
    
    public function show($id){
        $equipment = Equipment::find($id)->first();
        $equipment->store->department->category;
        $grn = $equipment->receivedNote;
        $grn->approved_user;
        return response()->json([$equipment]);
    }
    
    public function showQrCode($id)
    {
        $eq = Equipment::findOrfail($id);
        $depart =$eq->store->department->name;
        $st = $eq->store->name;
        $data = QRCode::text('OREMS/'.$depart.'/'.$st.'/'.$eq);
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $eq_data = $request->validate([
            'name' => 'max:255',
            'store_id' => 'integer',
            'grn_id' => 'integer|nullable',
        ]);

        $eq = Equipment::find($id);

        if($eq->update($eq_data))
            return response()->json($eq, 200);
        else {
            return response()->json("failed to Update equipment", 402);
        }
    }
    
    public function destroy($id)
    {
        // Perishable::where('equipment_id',$id)->firstOrFail()->delete(); //delete related perishable
        // NP::where('equipment_id', $id)->firstOrFail()->delete(); //delete related Non-perishable
        if(Equipment::destroy($id))
            return response()->json("deleted the equipment", 200);
        else
            return response()->json("failed to delete equipment", 402);
    }
}
