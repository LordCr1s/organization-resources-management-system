<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\FileRequested;
use App\Notifications\ProcessedRequest;
use App\User;
use App\Department;
use App\File;
use App\Attachment;
use App\Circle;
use App\Comment;
use App\FileRequest;
use App\FileTransaction;
use Validator;

class FileRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fileRequest = FileRequest::where('sender_id', auth('api')->user()->id)->orWhere('receiver_id', auth('api')->user()->id)->get()->each(function ($request){
            $request->sender;
            $request->receiver;
            $request->file;
        });
        return response()->json(['fileRequest'=> $fileRequest, 'status'=>200], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request)
    {
        $fileRequest= FileRequest::where('id', $request['request_id'])->update([
            'processed'=> 1,
            'status'=> $request['status']
        ]);
        $frequest = FileRequest::where('id', $request['request_id'])->first();
        $user = User::where('id', $frequest->sender_id)->first();
        $user->notify(new ProcessedRequest($frequest));

        return response()->json(['success'=> 'file request updated' ,'status'=>200], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'file_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            $file = $request['file_id'];
            $filecircle = Circle::all()->each(function($circle) use ($file){
                $circle->files->where('id', $file)->sortByDesc('created_at')->first();
            })->sortByDesc('created_at')->first();
            //dd($filecircle);
           // $circle = $filecircle->files()->wherePivot('file_id', $request['file_id'])->orderBy('created_at','DESC')->first();

            $filelog = FileTransaction::where('circle_id', $filecircle->id)->orderBy('created_at','DESC')->first();
            
            $fileRequest = FileRequest::create([
                'status' => 'In Progress',
                'receiver_id' => $filelog->to,
                'file_id' => $request['file_id'],
                'sender_id' => auth('api')->user()->id,
            ]);
            $user = User::where('id', $filelog->to)->first();

            $user->notify(new FileRequested($fileRequest));

            return response()->json(['success'=>'Request Added'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'status' => 'required|string',
            'request_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $fileRequest = FileRequest::where('id', $request['request_id'])->update([
                'status' => $request['status'],
            ]);
            
            return response()->json(['success'=>'Request Updated'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
