<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Permission;
use App\Role;
use App\Department;

class AuthController extends Controller
{
      /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * register a user to a system through activation code
     * @param \Illuminate\Http\Request
     *  
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request){
       $valid = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'department_id' => 'required|integer|min:0',
            'role_id' => 'required|integer|min:0',
        ]);
        
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
 
            $user = User::create([
                'firstname' => $request['firstname'],
                'lastname' => $request['lastname'],
                'surname' => $request['surname'],
                'email' => $request['email'],
                'status' =>1,
                'department_id' => $request['department_id'],
                'password' => Hash::make($request['password']),
                'email_verified_at'=> now()
    
            ]);
            $user->roles()->attach(Role::where('id', $request['role_id'])->first());
            $user->sendApiEmailVerificationNotification();
            return response()->json(['success' => 'successful registered', 'status'=>200], 200);
            
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        
        //$credentials = request(['email', 'password']);
        $user = User::where('email', $request->email)->first();
        $date = $user->email_verified_at;
        if($date != null){

            if (! $token = auth()->guard('api')->attempt(['email'=>$request->email,'password'=>$request->password, 'status'=>'Active'])) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            return $this->respondWithToken($token);

        }else{

            return response()->json(['error_info'=>'Email not Verified', 'error_code'=> 412], 412);
        }

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $id = auth('api')->user()->id;
        $user = User::where('id',$id)->with(['roles','permissions'])->first();
        $role = $user->roles;
        $permission = $user->permissions;
        $permissions = User::role_permissions($role);
        return response()->json(['user' => $this->guard('api')->user(), 'status' => 200,'role'=> $role, 'permission'=> $permission, 'permissionThroughRole'=> $permissions ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $roles = $this->guard('api')->user()->roles;
        return response()->json([
            'access_token' => $token,
            'user' => $this->guard('api')->user(),
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'roles' => $roles->pluck('name'),
            'status' => 200
        ]);
    }

    public function guard(){
        return \Auth::Guard('api');
    }

}
