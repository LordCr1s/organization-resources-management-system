<?php

namespace App\Http\Controllers;

use App\Tender;
use Illuminate\Http\Request;

class TenderController extends Controller
{
    public function index()
    {
        $tenders = Tender::all()->each(function($tender){
            $tender->bids;
        });
        return response()->json($tenders, 200);
    }

    public function store(Request $request)
    {
        $tender =  new Tender;
        $tender->fill($request->all());
        if($tender->saveOrFail()){
            return response()->json('Added', 200);
        }
    }

    public function show($id)
    {
        $tender = Tender::findOrfail($id);
        return response()->json($tender, 200);
    }

    public function update(Request $request, $id)
    {
        $tender = Tender::findOrfail($id);
        if($tender->update($request->all())){
            return response()->json($tender, 200);
        }
    }

    public function destroy($id)
    {
        if(Tender::destroy($id)){
            return response()->json('deleted', 200);
        }
    }
}
