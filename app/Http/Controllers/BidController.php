<?php

namespace App\Http\Controllers;

use App\Bid;
use Illuminate\Http\Request;

class BidController extends Controller
{
    public function index()
    {
        $bids = Bid::all()->each( function($bid){
            $bid->supplier;
        });
        return response()->json($bids, 200);
    }

    public function store(Request $request)
    {
        $bid =  new Bid;
        $bid->fill($request->all());
        if($bid->saveOrFail()){
            return response()->json('Added', 200);
        }
    }

    public function show($id)
    {
        $bid = Bid::findOrfail($id)->supplier;
        return response()->json($bid, 200);
    }

    public function update(Request $request, $id)
    {
        $bid = Bid::findOrfail($id);
        if($bid->update($request->all())){
            return response()->json($bid, 200);
        }
    }

    public function destroy($id)
    {
        if(Bid::destroy($id)){
            return response()->json('deleted', 200);
        }
    }
}
