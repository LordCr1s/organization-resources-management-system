<?php

namespace App\Http\Controllers;

use App\Exports\ProjectExport;
use App\Imports\ProjectImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Notifications\NewRequest;
use App\Notifications\CompleteRequest;
use App\Notifications\RejectedRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Process;
use App\State;
use App\Role;
use App\Transition;
use App\Departiment;
use App\RequestTransitionDate;
use App\RequestTransitionStatus;
use App\TransitionAction;
use App\TransitionRole;
use App\Project;

class RequestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function export($id) 
    {
      //dd(new ProjectExport($id));
        return Excel::download(new ProjectExport($id) , 'request.xlsx');
    }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('api')->user();

        if($user->hasRole('AD') || $user->hasRole('PR')){
            $requests = Project::all();
            return response()->json($requests, 200);
            
        }elseif ($user->hasRole('HOD') || $user->hasRole('HPMU')) {
            $requests = Project::pluck('user_id')->all();

            foreach ($requests as $request) {
                $users = User::where('id', $request)->get();
                foreach ($users as $user) {
                    if ($user->department_id == auth('api')->user()->department_id) {
                        $requests = Project::where('user_id', $user->id)->get();
                        return response()->json($requests, 200);
                    } 
                }
                 
            }  
           
        }elseif (!$user->hasRole('AD') || !$user->hasRole('PR')|| !$user->hasRole('HOD') || !$user->hasRole('HPMU')) {
            $requests = Project::where('user_id', $user->id)->get();
            return response()->json($requests, 200);
        }
        else {
            return response()->json('you dont have role or permission', 200);
        }
        
    }

    /**
     * Store a newly created request for approval in storage (database).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_a_new_request(Request $request)
    {
       
        $id = auth('api')->user()->id;
        $time = date('ihs'); //time();
        $current_state = State::where('process_id', $request->process_id)->where('name', 'START')->first();
        $project = new Project;
        $project->serial_number = "SNo."." ".$request->process_id."-" .$time;
        $project->process_id = $request->process_id;
        $project->state_id = $current_state->id;
        $project->user_id = $id;
        if($request->hasFile('file')){
            $array = Excel::toArray(new ProjectImport, $request->file('file'));
            $data_form_file = json_encode($array);
            //$data = 
            $project->data = json_encode($data);
            dd( $project->data);
        }else {
            $data = $request->except('process_id','user_id','_token');
            $project->data = json_encode($data);
        }
        dd( $project->data);
        $project->save();
        
        if($request->has('prepared_by') || $request->has('received_by')){
            //feed data to request transition table for status tracking
            $project_for_transition = Project::orderBy('created_at', 'DESC')->first();
            $transition = Transition::where('current_state_id', $current_state->id)->first();
            $transition_id = Transition::where('current_state_id', $transition->next_state_id)->first();
            $project_for_transition->state_id = $transition_id->next_state_id;
            $project_for_transition->save();
            $request_transition = new RequestTransitionStatus;
            $request_transition->request_id = $project_for_transition->id;
            $request_transition->transition_id = $transition->id;
            $request_transition->name = 'Accepted';
            $request_transition->save();

            //feed data to request transition date for time monitoring
            $transition_date = new RequestTransitionDate;
            $transition_date->request_id = $project_for_transition->id;
            $transition_date->transition_id = $transition->id;
            $transition_date->save();

            //pass to next transition after been sign by the creator of the request
            $request_transition_next = new RequestTransitionStatus;
            $request_transition_next->request_id = $project_for_transition->id;
            $request_transition_next->transition_id = $transition_id->id;
            $request_transition_next->name = 'None';
            $request_transition_next->save();

            //feed data to request transition date for time monitoring
            $transition_date_next = new RequestTransitionDate;
            $transition_date_next->request_id = $project_for_transition->id;
            $transition_date_next->transition_id = $transition_id->id;
            $transition_date_next->save();

            //get the role that is suporse to sign the request then send a notification through email, database and sms
            $transition_role = TransitionRole::where('transition_id', $transition_id->id)->first();
            $user_data = DB::select('SELECT users.firstname,users.surname, users.email ,roles.name, users.id
            FROM role_user
            INNER JOIN users
            ON users.id=role_user.user_id
            INNER JOIN roles
            ON roles.id=role_user.role_id
            WHERE roles.id = '. $transition_role->role_id);
            //dump($user_data);
            $user = User::find($user_data[0]->id);
            $user->notify(new NewRequest($project_for_transition));
            
            $result = "your request have been saved successiful!";

            return response()->json($result, 200);


        }else{
            //feed data to request transition table for status tracking
            $project_for_transition = Project::orderBy('created_at', 'DESC')->first();
            $transition = Transition::where('current_state_id', $current_state->id)->first();
            $request_transition = new RequestTransitionStatus;
            $request_transition->request_id = $project_for_transition->id;
            $request_transition->transition_id = $transition->id;
            //$request_transition->name = 'None';
            $request_transition->save();

            //feed data to request transition date for time monitoring
            $transition_date = new RequestTransitionDate;
            $transition_date->request_id = $project_for_transition->id;
            $transition_date->transition_id = $transition->id;
            $transition_date->save();

            //get the role that is suporse to sign the request then send a notification through email, database and sms
            $transition_role = TransitionRole::where('transition_id', $transition->id)->first();
            $user_data = DB::select('SELECT users.firstname,users.surname, users.email ,roles.name, users.id
            FROM role_user
            INNER JOIN users
            ON users.id=role_user.user_id
            INNER JOIN roles
            ON roles.id=role_user.role_id
            WHERE roles.id = '. $transition_role->role_id);
            //dump($user_data);
            $user = User::find($user_data[0]->id);
            $user->notify(new NewRequest($project_for_transition));
            
            $result = "your request have been saved successiful!";

            return response()->json($result, 200);
        }

    }

    /**
     * View the specified request from the database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_a_request($id)
    {
        $project = DB::select("SELECT processes.name AS process,states.name AS current_state,projects.data
        FROM projects
        INNER JOIN processes
        ON projects.process_id=processes.id
        INNER JOIN states
        ON projects.state_id=states.id
        WHERE projects.id =" . $id);
        $project[0]->data = json_decode($project[0]->data);
        return response()->json($project, 200);
        
    }

    /**
     * view request for sign or approval from the database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function view_request_for_sign($id)
     {
        
        $project = DB::select("SELECT processes.name AS process,states.name AS current_state,states.id AS state_id,projects.data
        FROM projects
        INNER JOIN processes
        ON projects.process_id=processes.id
        INNER JOIN states
        ON projects.state_id=states.id
        WHERE projects.id =" . $id);
        $project[0]->data = json_decode($project[0]->data);

        $transition = Transition::where('current_state_id',$project[0]->state_id)->first();
        $next_state = State::where('id', $transition->next_state_id)->first();
        $request_transition_status = RequestTransitionStatus::where('transition_id', $transition->id)->where('request_id', $id)->first();
        $request_transition_actions = TransitionAction::where('transition_id',$transition->id)->get();

        return response()->json(['Request' => $project,'Actions' => $request_transition_actions, 'transition status' => $request_transition_status,'next state' => $next_state], 200);
     }

    /**
     * Update the request status in storage(database).
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_request_status($id, Request $request)
    {
       // view_request_for_sign($id);
        $project = Project::where('id',$id)->first();
        $state = State::where('id',$project->state_id)->first();
        $transition = Transition::where('current_state_id',$state->id)->first();
        $next_state = State::where('id',$transition->next_state_id)->first();
        $transition_id = Transition::where('current_state_id', $transition->next_state_id)->first();

        if($request->status == 'Accepted' && $next_state->name == 'FINISH'){
            $data = $request->except('process_id','user_id','_token','status','signature');
            $datas = json_encode($data);
            $new_data = json_encode(array_merge(json_decode($project->data,true), json_decode($datas, true)));
            $project->data = $new_data; 
            $project->state_id = $next_state->id;
            $transition_status = RequestTransitionStatus::where('transition_id', $transition->id)->first();
            $transition_status->name = $request->status;
            $transition_status->save();

            //feed data to request transition date for time monitoring
            $transition_date = new RequestTransitionDate;
            $transition_date->request_id = $project->id;
            $transition_date->transition_id = $transition->id;
            $transition_date->save();

            $user = User::find($project->user_id);
            $user->notify(new CompleteRequest($project));
            $project->save();

            $result = "your request have been complete successiful!";

            return response()->json($result, 200);

        }elseif ($request->status == 'Accepted' && $next_state->name != 'FINISH') {
            $data = $request->except('process_id','user_id','_token','status', 'signature');
            $datas = json_encode($data);
            $new_data = json_encode(array_merge(json_decode($project->data,true), json_decode($datas, true)));
            $project->data = $new_data;
            $project->state_id = $next_state->id;
            $transition_status = RequestTransitionStatus::where('transition_id', $transition->id)->first();
            $transition_status->name = $request->status;
            $transition_status->save();

            //feed data to request transition date for time monitoring
            $transition_date = new RequestTransitionDate;
            $transition_date->request_id = $project->id;
            $transition_date->transition_id = $transition->id;
            $transition_date->save();
 
            //pass to next transition after been sign by the approver of the request
            $request_transition_next = new RequestTransitionStatus;
            $request_transition_next->request_id = $project->id;
            $request_transition_next->transition_id = $transition_id->id;
            $request_transition_next->name = 'None';
            $request_transition_next->save();

            //feed data to request transition date for time monitoring
            $transition_date_next = new RequestTransitionDate;
            $transition_date_next->request_id = $project->id;
            $transition_date_next->transition_id = $transition_id->id;
            $transition_date_next->save();
           
            //get the role that is suporse to sign the request then send a notification through email, database and sms
            $transition_role = TransitionRole::where('transition_id', $transition->id)->first();
            $user_data = DB::select('SELECT users.firstname,users.surname, users.email ,roles.name, users.id
            FROM role_user
            INNER JOIN users
            ON users.id=role_user.user_id
            INNER JOIN roles
            ON roles.id=role_user.role_id
            WHERE roles.id = '. $transition_role->role_id);
            //dump($user_data);
            $user = User::find($user_data[0]->id);
            $user->notify(new NewRequest($project));
            $project->save();
            $result = "your request have been sent to another approver successiful!";

            return response()->json($result, 200);

        }elseif ($request->status == 'Rejected') {
            $transition_status = RequestTransitionStatus::where('transition_id', $transition->id)->first();
            $transition_status->name = $request->status;
            $transition_status->save();

            //feed data to request transition date for time monitoring
            $transition_date = new RequestTransitionDate;
            $transition_date->request_id = $project->id;
            $transition_date->transition_id = $transition->id;
            $transition_date->save();

            $user = User::find($project->user_id);
            $user->notify(new RejectedRequest($project));

            $result = "your request have been rejected!";

            return response()->json($result, 200);
        }else {
            $result = "error with updating your request";

            return response()->json($result, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
