<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::orderBy('created_at','DESC')->get()->each(function($store){
            $store->department;
        });
        return response()->json($stores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $store_data = $request->validate([
            'name' => 'required|max:255',
            'description'  => 'required|max:255',
            'department_id' => 'integer'
        ]);
        $store = new Store();
        $store->name = $request->name;
        $store->description = $request->description;
        $store->department_id = $request->department_id;

        if($store->saveOrFail())
            return response()->json($store, 200);
        else
            return response()->json("failed to save the store information", 403);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Store::findOrfail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store_data = $request->validate([
            'name' => 'max:255',
            'description'  => 'max:255',
            'department_id' => 'integer'
        ]);
        $store = Store::findOrfail($id);

        if($store->update($store_data))
            return response()->json($store, 200);
        else
            return response()->json("failed to update the store information", 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Store::destroy($id))
            return response()->json("Store deleted!", 200);
        else {
            return response()->json("Store failed to delete!", 403);
        }
    }
}
