<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\FileReceived;
use App\User;
use App\Department;
use App\File;
use App\Attachment;
use App\Circle;
use App\Comment;
use App\FileRequest;
use App\FileTransaction;
use Validator;

class CircleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $circles = Circle::all()->each(function ($circle){
           $files = $circle->files;
           $files->each(function ($file){
               $attachments = $file->attachments;
               $attachments->each(function ($attachment){
                   $attachment->comments;
               });
               $file->department;
           });
            $transactions = $circle->fileTransactions;
            $transactions->each(function ($transaction){
                $transaction->fromo;
                $transaction->too;
            });
        });
        return response()->json(['circles'=> $circles, 'status'=>200], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'reason' => 'required|string',
            'file_id' => 'required|integer', 
            'user_id' => 'required|integer'
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $circle = Circle::create([
                'reason' => $request['reason'],
                'start_at' => now(),
                'status' => 'On Progress',
            ]);
            $circle->files()->attach(File::where('id', $request['file_id'])->first());
            $transaction = new FileTransaction;
            $transaction->status = 'Received';
            $transaction->from = auth('api')->user()->id;
            $transaction->to = $request['user_id'];
            $transaction->circle_id = $circle->id;
            $transaction->save();
            
            $user = User::where('id', $request['user_id'])->first();
            $user->notify(new FileReceived($circle));

            return response()->json(['success'=>'Circle Added'], 200);
            
        }
    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $circles = Circle::where('id', $id)->with('files', 'fileTransactions', 'attachments.comments')->first();

        return response()->json(['circles'=> $circles], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'reason' => 'required|string',
            'start_at' => 'required|string',
            'file_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $circle = Circle::where('id', $id)->update([
                'reason' => $request['reason'],
                'start_at' => $request['start_at'],
                'status' => "Complete",
            ]);

            $circle->files()->attach(File::where('id', $request['file_id'])->first());

            return response()->json(['success'=>'Circle Updated'], 200);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(File::destroy($id))
            return response()->json("Circle deleted!", 200);
        else {
            return response()->json("Circle failed to delete!", 403);
        }
    }
}
