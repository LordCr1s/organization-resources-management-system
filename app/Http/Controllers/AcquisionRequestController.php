<?php

namespace App\Http\Controllers;

use App\AcquisionRequest as AR;
use Illuminate\Http\Request;

class AcquisionRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $acq = AR::all();
        $acq = \DB::table('acquision_requests AS AR')
                ->join('departments AS D','AR.department_id','=','D.id')
                ->join('users AS U','AR.user_id','=','U.id')
                ->select('AR.acquision_number','AR.description','D.name AS `department_name`',
                        'U.firstname AS `user_name`','AR.created_at','AR.updated_at')
                ->get();
        return response()->json($acq);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $acq_data = $request->validate([
            'description' => 'required|max:255',
            'department_id' => 'required|integer',
            'user_id' => 'required|integer',
            'acquision_number' => 'required|max:255'
        ]);
        $acq = new AR();
        $acq->acquision_number = 'OREMS'.now()->year.now()->month.$acq->id;
        $acq->description = $request->description;
        $acq->department_id = $request->department_id;
        $acq->user_id = $request->user_id;//Auth::user();

        if($acq->saveOrFail())
            return response()->json($acq);
        else
            return redirect('/error')->with('error','failed to store acquision request');
    }

    /**
     * Display the specified resource.
     * @param  \App\AcquisionRequest  $acquisionRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acq = AR::findOrfail($id);
        return response()->json($acq, 200, ['year' => now()->year . now()->month]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AcquisionRequest  $acquisionRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $acq_data = $request->validate([
            'description' => 'max:255',
            'department_id' => 'integer',
            'user_id' => 'integer',
            'acquision_equipments_id' => 'integer',
            'acquision_number' => 'max:255'
        ]);
        $acq =  AR::findOrfail($id);

        if( $acq->update($acq_data) )
            return response()->json($acq);
        else
            return redirect('/error')->with('error','failed to update acquision request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AcquisionRequest  $acquisionRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if( AR::destroy($id))
            return response()->json("Acquisition Destroyed");
        else
            return redirect('/error')->with('error','failed to update acquision request');   
    }
}
