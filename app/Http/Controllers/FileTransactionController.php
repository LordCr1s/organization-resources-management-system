<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Notifications\FowardedRequest;
use App\User;
use App\Department;
use App\File;
use App\Attachment;
use App\Circle;
use App\Comment;
use App\FileRequest;
use App\FileTransaction;
use App\Reminder;
use Validator;

class FileTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fileTransactions = fileTransaction::where('from', auth()->user()->id)->where('processed', 1)->get()->each(
            function ($transaction){
                $transaction->fromo;
                $transaction->too;
                $files = $transaction->circle->files;
                $files->each(function($file){
                    $file->attachments;
                    $file->comments;
                    $file->initialAttachments;
                });
            }
        );

        return response()->json(['fileTransactions' => $fileTransactions, 'status'=>200], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function incoming()
    {
        $fileTransactions = fileTransaction::where('to', auth()->user()->id)->where('processed', '0')->get()->each(
            function ($transaction){
                $transaction->fromo;
                $transaction->too;
                $transaction->circle;
                $files = $transaction->circle->files;
               $files->each(function($file){
                    $file->attachments;
                    $file->comments;
                    $file->initialAttachments;
                });
            }
        );

        return response()->json(['fileTransactions' => $fileTransactions, 'status'=> 200], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'status' => 'required|string',
            'to' => 'required|integer',
            'circle_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            $file = File::where('id', 12)->first();
            if ($request['status'] === 'Fowarded') {
                $update = FileTransaction::where('id', $request['transaction_id'])->update([
                    'processed'=> 1
                ]);
                $fileRequest = FileTransaction::create([
                        'status' => $request['status'],
                        'from' => auth('api')->user()->id,
                        'to' => $request['to'],
                        'circle_id' => $request['circle_id'],
                        'processed' => 1,
                    ]); 
                    
                    foreach ($request['attachments'] as $attachment) {
                        //  dd($request->file($attachment['file']));
                          if($request['attachments']){
                              $folder = public_path('/storage/app/public/' . $file->name . '/');
                              if (!Storage::exists($folder)) {
                                  Storage::makeDirectory($folder, 0775, true, true);
                              }         
                              // $filename = $request->file($attachment['file'])->getClientOriginalName();
                              // $filename_ext = $request->file($attachment['file'])->getClientOriginalExtension();
                              // $filetostore = $filename.'_'. time().'.'.$filename_ext;
                              // $path = $request->file($attachment['file'])->storeAs($folder, $filetostore);
                              $initialattachments = Attachment::create([
                                  'name'=> $attachment['name'],
                                  'path'=>$attachment['file'],
                                  'file_id'=> $file->id,
                                  'circle_id' => $request['circle_id']
                              ]);

                              $comment = Comment::create([
                                  'comment'=> $request['comment'],
                                  'attachment_id'=> $initialattachments->id
                              ]);
                          }
                          
                      }

                $user = User::where('id', $request['to'])->first();
                //notification goes here
                $user->notify(new FowardedRequest($fileRequest));
                
                return response()->json(['success'=>'Transaction Added'], 200);

            }elseif ($request['status'] === 'Rejected') {
                $update = FileTransaction::where('id', $request['transaction_id'])->update([
                    'processed'=> 1
                ]);
                $fileRequest = FileTransaction::create([
                    'status' => $request['status'],
                    'from' => auth('api')->user()->id,
                    'to' => $request['to'],
                    'circle_id' => $request['circle_id'],
                    'processed' => 1,
                ]);
                
                foreach ($request['attachments'] as $attachment) {
                    //  dd($request->file($attachment['file']));
                      if($request['attachments']){
                          $folder = public_path('/storage/app/public/' . $file->name . '/');
                          if (!Storage::exists($folder)) {
                              Storage::makeDirectory($folder, 0775, true, true);
                          }         
                          // $filename = $request->file($attachment['file'])->getClientOriginalName();
                          // $filename_ext = $request->file($attachment['file'])->getClientOriginalExtension();
                          // $filetostore = $filename.'_'. time().'.'.$filename_ext;
                          // $path = $request->file($attachment['file'])->storeAs($folder, $filetostore);
                          $initialattachments = Attachment::create([
                              'name'=> $attachment['name'],
                              'path'=>$attachment['file'],
                              'file_id'=> $file->id,
                              'circle_id' => $request['circle_id']
                          ]);

                          $comment = Comment::create([
                            'comment'=> $request['comment'],
                            'attachment_id'=> $initialattachments->id
                        ]);

                      }
                      
                  }   

            $user = User::where('id', $request['to'])->first();
            //notification goes here
            $user->notify(new FowardedRequest($fileRequest));
            
            return response()->json(['success'=>'Transaction Added'], 200);

            }elseif ($request['status'] === 'Returned') {
                $update = FileTransaction::where('id', $request['transaction_id'])->update([
                    'processed'=> 1
                ]);
                $fileRequest = FileTransaction::create([
                    'status' => $request['status'],
                    'from' => auth('api')->user()->id,
                    'to' => $request['to'],
                    'circle_id' => $request['circle_id'],
                    'processed' => 1,
                ]);
                foreach ($request['attachments'] as $attachment) {
                    //  dd($request->file($attachment['file']));
                      if($request['attachments']){
                          $folder = public_path('/storage/app/public/' . $file->name . '/');
                          if (!Storage::exists($folder)) {
                              Storage::makeDirectory($folder, 0775, true, true);
                          }         
                          // $filename = $request->file($attachment['file'])->getClientOriginalName();
                          // $filename_ext = $request->file($attachment['file'])->getClientOriginalExtension();
                          // $filetostore = $filename.'_'. time().'.'.$filename_ext;
                          // $path = $request->file($attachment['file'])->storeAs($folder, $filetostore);
                          $initialattachments = Attachment::create([
                              'name'=> $attachment['name'],
                              'path'=>$attachment['file'],
                              'file_id'=> $file->id,
                              'circle_id' => $request['circle_id']
                          ]);

                          $comment = Comment::create([
                            'comment'=> $request['comment'],
                            'attachment_id'=> $initialattachments->id
                        ]);
                      }
                      
                  }   

            $user = User::where('id', $request['to'])->first();
            //notification goes here
            $user->notify(new FowardedRequest($fileRequest));
            
            return response()->json(['success'=>'Transaction Added'], 200);

            }else {
                
               return response()->json(['error'=>'Transaction Not Added'], 401); 
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reminder()
    {
        $reminders = Reminder::where('user_id', auth('api')->user()->id)->get();

        return response()->json(['reminders' => $reminders, 'status'=> 200], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
