<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\Reply;
use App\User;
use App\Department;
use Validator;

class ChatController extends Controller
{
    public function index()
    {
        $chats = Chat::with('replies', 'user', 'department')->get();
        
        return response()->json(['chats' => $chats ], 200);
    }

    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'message' => 'required|string',
            'department_id' => 'required|integer',
            'user_id' => 'required|integer' 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = Chat::create([
                'message' => $request['message'],
                'department_id' => $request['department_id'],
                'user_id' => $request['user_id']
            ]);
            
            return response()->json(['success'=>'Message Added'], 200);
            
        }
    }
}
