<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DepartmentRequest;
use App\DepartmentRequestStage;
use App\DepartmentStore;
use App\DepartmentTransaction;
use App\DepartmentTransactionStatus;
use App\User;
use App\Notifications\RequestingMaterial;
use App\Notifications\RequestAccepted;
use App\Notifications\RequestRejected;
use App\Notifications\ReturnMaterial;
use App\Notifications\AcceptReturnMaterial;
use Validator;

class DepartmentRequestController extends Controller
{

    public function material_in_store(){
        $equipment = DepartmentStore::with([
        'equipment' => function($o){$o->select('name', 'id')->get();},
        'department' => function($o){$o->select('name', 'id')->get();}
        ])->get();
        return response()->json($equipment, 200);  
    }

    public function request_material(Request $request){
        $valid = Validator::make($request->all(), [
            'requested_quantity' => 'required|integer', 
            'user_id' => 'required|integer',
            'purpose' => 'required|string',
            'department_store_id' => 'required|integer',
            'equipment_id' => 'required|integer',
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequest::create([
                'quantity' => $request['requested_quantity'],
                'user_id' => $request['user_id'],
                'purpose' => $request['purpose'],
                'department_store_id' => $request['department_store_id'],
                'equipment_id' => $request['equipment_id'],
                'borrow_code' => mt_rand(100000, 999999),
                'return_code' => mt_rand(100000, 999999)
            ]);

            $department_request = DepartmentRequest::orderBy('created_at', 'DESC')->first();
            $stage = new DepartmentRequestStage;
            $stage->stage = 'Requesting Material';
            $stage->department_request_id = $department_request->id; 
            $stage->save();

            $transaction = new DepartmentTransaction;
            $transaction->quantity = $department_request->quantity;
            $transaction->user_id = $department_request->user_id;
            $transaction->equipment_id = $department_request->equipment_id;
            $transaction->department_store_id = $department_request->department_store_id;
            $transaction->department_request_id = $department_request->id;
            $transaction->save();
            $department_store = DepartmentStore::where('equipment_id', $transaction->equipment_id )->first();
            if ($department_store->balance > $transaction->quantity ) {
                $new_balance = $department_store->balance - $transaction->quantity;
                $department_store->balance = $new_balance;
                $department_store->save();
                $last_transaction = DepartmentTransaction::orderBy('created_at', 'DESC')->first();
                $transaction_status = new DepartmentTransactionStatus;
                $transaction_status->status = 'Borrowed';
                $transaction_status->department_transaction_id = $last_transaction->id;
                $transaction_status->save();

                 
            }else {
                return response()->json(['error'=>'Material For Request Are Not Enough'], 401);
            }
            $store = DepartmentStore::where('id',$request['department_store_id'])->first();
            $users = User::where('department_id', $store->id)->get();

            foreach ($users as $user) {
                if($user->hasRole('SA')){

                    $user->notify(new RequestingMaterial($department_request));
                    return response()->json(['success'=>'Material Requested Successful'], 200);
                }
                
            }
        }
        
    }

    public function accept_request(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequestStage::where('department_request_id')->update([
                'stage' => 'Reciving Material',
                'updated_at' => now(),
            ]);

            $department_request = DepartmentRequest::where('id',$request['department_request_id'])->first();
            $user = User::where('id', $department_request->id)->first();
            $user->notify(new RequestAccepted());
            return response()->json(['success'=>'Material Request Accepted Successful'], 200);
            
        }

    }

    public function reject_request(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequestStage::where('department_request_id')->update([
                'stage' => 'Rejected',
                'updated_at' => now(),
            ]);

            $transaction = DepartmentTransaction::where('department_request_id', $request['department_request_id'])->first();
            $department_store = DepartmentStore::where('equipment_id', $transaction->equipment_id )->first();
            $new_balance = $department_store->balance + $transaction->quantity;
            $department_store->balance = $new_balance;
            $department_store->save();
            $transaction_status = DepartmentTransactionStatus::where('department_transaction_id', $transaction->id)->first();
            $transaction_status->status = 'Returned';
            $transaction_status->save();

            $department_request = DepartmentRequest::where('id',$request['department_request_id'])->first();
            $user = User::where('id', $department_request->id)->first();
            $user->notify(new RequestRejected());
            return response()->json(['success'=>'Material Request Rejected Successful'], 200);
            
        }

    }

    public function possess_request(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer',
            'borrow_code' => 'required|integer'
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            $department_request = DepartmentRequest::where('id',  $request['department_request_id'])->first();
            if ($request['borrow_code'] == $department_request->borrow_code) {
                $academic = DepartmentRequestStage::where('department_request_id')->update([
                    'stage' => 'On Possession',
                    'updated_at' => now(),
                ]);
                return response()->json(['success'=>'Request On possession'], 200);
            }else {
                return response()->json(['error'=>'Wrong Code Try Again'], 200);
            }
        }

    }

    public function request_for_return_by_store_keeper(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequestStage::where('department_request_id')->update([
                'stage' => 'Return Material',
                'updated_at' => now(),
            ]);
            $message = 'Your Store Kepeer request you to return the teaching material that you possess';
            $department_request = DepartmentRequest::where('id',$request['department_request_id'])->first();
            $user = User::where('id', $department_request->id)->first();
            $user->notify(new ReturnMaterial($message));
            return response()->json(['success'=>'Material Request for Return'], 200);
            
        }
    }

    public function request_for_return_by_lecture(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequestStage::where('department_request_id')->update([
                'stage' => 'Return Material',
                'updated_at' => now(),
            ]);
            $department_request = DepartmentRequest::where('id',$request['department_request_id'])->first();
            $store = DepartmentStore::where('id',$department_request->department_store_id)->first();
            $users = User::where('department_id', $store->id)->get();
            $message = 'The lecture who request materails from your store want to return them';

            foreach ($users as $user) {
                if($user->hasRole('SA')){
                    $user->notify(new ReturnMaterial($message));
                    return response()->json(['success'=>'Return Material Successful'], 200);
                }
                
            }
        }
    }

    public function accept_request_for_return_material(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequestStage::where('department_request_id')->update([
                'stage' => 'Returning Material',
                'updated_at' => now(),
            ]);

            $department_request = DepartmentRequest::where('id',$request['department_request_id'])->first();
            $user = User::where('id', $department_request->id)->first();
            $user->notify(new AcceptReturnMaterial());
            return response()->json(['success'=>'Material Request Accepted Successful'], 200);
        }
    }    

    public function returning_material(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer',
            'return_code' => 'required|integer'
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            $department_request = DepartmentRequest::where('id',  $request['department_request_id'])->first();
            if ($request['return_code'] == $department_request->return_code) {
                $academic = DepartmentRequestStage::where('department_request_id')->update([
                    'stage' => 'Accepted',
                    'updated_at' => now(),
                  
                ]);
                $transaction = DepartmentTransaction::where('department_request_id', $request['department_request_id'])->first();
                //dd($transaction);
                $department_store = DepartmentStore::where('equipment_id', $transaction->equipment_id )->first();
                $new_balance = $department_store->balance + $transaction->quantity;
                $department_store->balance = $new_balance;
                $department_store->save();
                $transaction_status = DepartmentTransactionStatus::where('department_transaction_id', $transaction->id)->first();
                $transaction_status->status = 'Returned';
                $transaction_status->save();
                return response()->json(['success'=>'Material Request On Possession'], 200);
            }else {
                return response()->json(['error'=>'Wrong Code Try Again'], 200);
            }
        }
    }

    public function cancel_request_for_material(Request $request){
        $valid = Validator::make($request->all(), [
            'department_request_id' => 'required|integer', 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = DepartmentRequestStage::where('department_request_id')->update([
                'stage' => 'Canceled',
                'updated_at' => now(),
            ]);
            return response()->json(['success'=>'Material Request Canceled Successful'], 200);
        }
    }

    public function pending_request($role){
        $lecture_requests = DepartmentRequest::select('id', 'quantity','purpose', 'borrow_code','user_id','equipment_id','department_store_id','created_at')->
        with([
        'department_request_stages' => function($o){$o->select('stage', 'department_request_id')
        ->where('stage','!=', 'Rejected')->where('stage','!=', 'Accepted')->get()->isNotEmpty();},
        'equipment' => function($o){$o->select('name', 'id')->get();}
        ])->get();

        $store_requests = DepartmentRequest::select('id', 'quantity','purpose', 'return_code','user_id','equipment_id','department_store_id','created_at')->
        with([
        'department_request_stages' => function($o){$o->select('stage', 'department_request_id')
        ->where('stage','!=', 'Rejected')->where('stage','!=', 'Accepted')->get()->isNotEmpty();},
        'equipment' => function($o){$o->select('name', 'id')->get();}
        ])->get();

        if (($role == 'lecture') ) {
            return response()->json($lecture_requests, 200);
        }elseif ($role == 'store_kepeer') {
            return response()->json($store_requests, 200);
        }
    }

    public function on_possession_request(){
        $onpossession = DepartmentTransaction::
        with(['user' => function($o){$o->select('firstname', 'surname', 'id')->get();},
        'department_transaction_statuses' => function($o){$o->select('status', 'department_transaction_id')->where('status','borrowed')->get();},
        'department_request' => function($o){$o->select('borrow_code', 'return_code',  'created_at', 'id')->get();}
        ])->get();

        return response()->json($onpossession, 200);
    }

    public function complited_request(){
        $complited = DepartmentRequest::
        with([
        'department_request_stages' => function($o){$o->select('stage', 'department_request_id')->
        where('stage', 'Rejected')->orWhere('stage', 'Accepted')->get();},
        'equipment' => function($o){$o->select('name', 'id')->get();},
        'user' => function($o){$o->select('firstname', 'surname', 'id')->get();},
        ])->get();

        return response()->json($complited, 200);
    }
    
    
}
