<?php

namespace App\Http\Controllers;

use App\Perishable;
use Illuminate\Http\Request;

class PerishableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Perishable  $perishable
     * @return \Illuminate\Http\Response
     */
    public function show(Perishable $perishable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Perishable  $perishable
     * @return \Illuminate\Http\Response
     */
    public function edit(Perishable $perishable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Perishable  $perishable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Perishable $perishable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Perishable  $perishable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Perishable $perishable)
    {
        //
    }
}
