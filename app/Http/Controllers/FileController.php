<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Department;
use App\File;
use App\Attachment;
use App\Circle;
use App\Comment;
use App\FileRequest;
use App\FileTransaction;
use App\InitialAttachment;
use Validator;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::all()->each(function($file) {
            $file->department;
            $file->circles;
            $file->attachments;
            $file->initialAttachments;
           
        });
        return response()->json(['files' => $files, 'status'=>200], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function track(Request $request)
    {
            $security_no = $request['security_no'];
            $file = File::where('security_no', $security_no)->first();
            $filecircle = Circle::all()->each(function($circle) use ($file){
                $circle->files->where('id', $file->id)->sortByDesc('created_at')->first();
            })->sortByDesc('created_at')->first();
            //dd($filecircle);
           // $circle = $filecircle->files()->wherePivot('file_id', $request['file_id'])->orderBy('created_at','DESC')->first();

            $filelog = FileTransaction::where('circle_id', $filecircle->id)->orderBy('created_at','DESC')->first();
            $holder = User::where('id', $filelog->to)->with('department')->first();

            return response()->json(['holder' => $holder, 'status'=>200], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request['attachments']['name']);
        $valid = Validator::make($request->all(), [
            'name' => 'required|string',
            'reference_no' => 'required|string',
            'department_id' => 'required|integer',
            'file_type' => 'required|string',
            'access_level' => 'required|string' 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $file = File::create([
                'name' => $request['name'],
                'department_id' => $request['department_id'],
                'reference_no' => $request['reference_no'],
                'file_type' => $request['file_type'],
                'access_level' => $request['access_level'],
                'security_no' => rand(100000, 999999) .'-'. 'OREMS'. '-'.date('ihs')
            ]);
            foreach ($request['attachments'] as $attachment) {
              //  dd($request->file($attachment['file']));
                if($request['attachments']){
                    $folder = public_path('/storage/app/public/' . $file->name . '/');
                    if (!Storage::exists($folder)) {
                        Storage::makeDirectory($folder, 0775, true, true);
                    }         
                    // $filename = $request->file($attachment['file'])->getClientOriginalName();
                    // $filename_ext = $request->file($attachment['file'])->getClientOriginalExtension();
                    // $filetostore = $filename.'_'. time().'.'.$filename_ext;
                    // $path = $request->file($attachment['file'])->storeAs($folder, $filetostore);
                    $initialattachments = InitialAttachment::create([
                        'name'=> $attachment['name'],
                        'path'=>$attachment['file'],
                        'file_id'=> $file->id
                    ]);
                }
                
            }
 
            return response()->json(['success'=>'File Added'], 200);
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::where('id', $id)->with(
            ['department' => function ($query1) 
                {
                    $query1->get();
                } 
            ,
            'circles' =>function ($query) 
                {
                    $query->with(['fileTransactions' => function ($qry){
                        $qry->with('from', 'to')->get();
                    },  
                    'attachments' =>function ($o) 
                    {
                        $o->with('comments')->get();
                    } ])->get();
                } 
            ,
            'fileRequests' =>function ($o) 
                {
                    $o->with('sender', 'receiver')->get();
                } 
            ]
            )->first();

        return response()->json(['file'=> $file], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required|string',
            'reference_no' => 'required|string',
            'department_id' => 'required|integer',
            'file_type' => 'required|string',
            'access_level' => 'required|string' 
        ]);
        if($valid->fails()){
            return response()->json(['error'=>$valid->errors()], 401);

        }else{
            
            $academic = File::where('id', $id)->update([
                'name' => $request['name'],
                'department_id' => $request['department_id'],
                'reference_no' => $request['reference_no'],
                'file_type' => $request['file_type'],
                'access_level' => $request['access_level'],
            ]);
            
            return response()->json(['success'=>'File Updated'], 200);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(File::destroy($id))
            return response()->json("File deleted!", 200);
        else {
            return response()->json("File failed to delete!", 403);
        }
    }
    
}
