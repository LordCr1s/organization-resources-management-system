<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcquisionRequest extends Model
{
    protected $fillable = ['description','acquision_equipments_id','department_id','user_id'];
    protected $foreignKey = ['acquision_equipments_id','department_id','user_id'];
    protected $table = 'acquision_requests';

    public function AcquisionEquipment()
    {
        return $this->hasMany('App\AcquisionRequestEquipment', 'acquision_equipments_id');
    }
    public function ReceivedNote()
    {
        return $this->hasMany('App\ReceivedNote');
    }
    public function storeTransaction()
    {
        return $this->hasOne('App\StoreTransaction');
    }
    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
