<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    protected $fillable = ['reason','start_at','end_at', 'status'];

    public function files()
    {
        return $this->belongsToMany('App\File');
    }

    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    public function fileTransactions()
    {
        return $this->hasMany('App\FileTransaction');
    }
}
