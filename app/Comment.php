<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['comment','attachment_id'];

    public function attachment()
    {
        return $this->belongsTo('App\Attechment');
    }
}
