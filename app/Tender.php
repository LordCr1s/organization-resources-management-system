<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function bids(){
        return $this->hasMany('App\Bid');
    }
    public function suppliers(){
        return $this->hasMany('App\Supplier','supplier_id','id');
    }
    public function supplier(){
        return $this->hasManyThrough('App\Supplier', App\Bid);
    }
}
