<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $fillable = ['name','store_id','grn_id'];
    protected $primaryKey = 'id';
    protected $foreignKey = ['store_id','grn_id'];
    protected $table = 'equipments';

    public function perishable()
    {
        return $this->hasMany('App\Perishable');
    }
    public function nonPerishable()
    {
        return $this->hasMany('App\NonPerishable');
    }
    public function store()
    {
        return $this->belongsTo('App\Store', 'store_id');
    }
    
    public function storeTransaction()
    {
        return $this->belongsToMany('App\StoreTransaction','equipment_store_transactions','equipment_id','store_transaction_id');
    }
    public function receivedNote()
    {
        return $this->belongsTo('App\ReceivedNote', 'grn_id','id');
    }

    public function department_stores()
    {
        return $this->hasMany('App\DepartmentStore');
    }

    public function department_requests()
    {
        return $this->hasMany('App\DepartmentRequest');
    }
    
    public function department_transactions()
    {
        return $this->hasMany('App\DepartmentTransaction');
    }
}
