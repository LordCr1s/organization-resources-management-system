<?php

namespace App\Providers;
use App\Permission;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        // //helper for using can('arg') to check permission in blade template
        // Permission::get()->map(function($permission){
        //     Gate::define($permission->name, function($user) use ($permission){
        //        return $user->hasAnyPermission($permission);
        //     });
        // });

        //directive for use @canany('arg|arg') multiple permission in blade
       /* Blade::directive('canany', function ($arguments) {
            list($permissions, $guard) = explode(',', $arguments.',');
        
            $permissions = explode('|', str_replace('\'', '', $permissions));
        
            $expression = "<?php if(auth({$guard})->check() && ( false";
            foreach ($permissions as $permission) {
                $expression .= " || auth({$guard})->user()->can('{$permission}')";
            }
        
            return $expression . ")): ?>";
        });
        
         Blade::directive('endcanany', function () {
            return '<?php endif; ?>';
        });*/

        Blade::directive('canany', function($permissions){
            return dd(Auth::user()->hasAnyPermission($permissions));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
