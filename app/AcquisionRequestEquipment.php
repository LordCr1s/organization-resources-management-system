<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcquisionRequestEquipment extends Model
{
    protected $fillable = ['name','description','quantity'];
    protected $table = 'acquision_request_equipments';

    public function AcquisionRequest()
    {
        return $this->belongsTo('App\AcquisionRequest');
    }
}
