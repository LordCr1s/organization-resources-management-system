<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name','description','category_department_id'];
    protected $foreignKey = 'category_department_id';
    protected $table = 'departments';
    
    public function users(){
        return $this->hasMany('App\User');
    }

    public function activation_code(){
        return $this->hasOne('App\ActivationCode');
    }
    public function organization(){
        return $this->belongsTo('App\Organization', 'organization_id');
    }
    public function stores()
    {
        return $this->hasMany('App\Store');
    }
    public function category(){
        return $this->belongsTo('App\CategoryDepartment','category_department_id','id');
    }

    public function department_stores()
    {
        return $this->hasMany('App\DepartmentStore');
    }

    public function chats()
    {
        return $this->hasMany('App\Chat');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }
}
