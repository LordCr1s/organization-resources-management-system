<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileRequest extends Model
{
    protected $fillable = ['status','sender_id','receiver_id', 'file_id'];

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function file()
    {
        return $this->belongsTo('App\File');
    }
}
