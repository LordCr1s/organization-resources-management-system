<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonPerishable extends Model
{

    protected $fillable = ['brand','qrCode_string','serial_number','equipment_id','equipment_category_id'];
    protected $foreignKey = ['equipment_id','equipment_category_id'];
    protected $primaryKey = 'id';
    protected $table = 'non_perishable';

    public function equipment()
    {
        return $this->belongsTo('App\Equipment', 'equipment_id');
    }
    public function equipmentInformation()
    {
        return $this->belongsTo('App\EquipmentInformation', 'equipment_category_id');
    }
}
