<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileTransaction extends Model
{
    protected $fillable = ['status','from','to', 'circle_id'];

    public function fromo()
    {
        return $this->belongsTo('App\User', 'from');
    }

    public function too()
    {
        return $this->belongsTo('App\User', 'to');
    }

    public function circle()
    {
        return $this->belongsTo('App\Circle', 'circle_id');
    }
}
