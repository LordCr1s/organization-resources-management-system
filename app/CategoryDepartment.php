<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDepartment extends Model
{
    protected $table = 'category_department';
    
    public function departments(){
        return $this->hasMany('App\Department','department_id','id');
    }
}
