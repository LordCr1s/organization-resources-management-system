<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name','reference_no','file_type', 'access_level', 'department_id','security_no'];

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function circles()
    {
        return $this->belongsToMany('App\Circle', 'circle_file');
     
    }

    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    public function fileRequests()
    {
        return $this->hasMany('App\FileRequest', 'file_id');
    }

    public function comments()
    {
        return $this->hasManyThrough('App\Comment', 'App\Attachment','file_id','attachment_id');
    }

    public function fileTransactions()
    {
        return $this->hasManyThrough('App\FileTransaction', 'App\Circle');
    }

    public function initialAttachments()
    {
        return $this->hasMany('App\InitialAttachment');
    }
}
