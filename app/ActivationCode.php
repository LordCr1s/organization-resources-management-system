<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    protected $table = 'activation_codes';

    public function department(){
        return $this->BelongsTo('App\Department');
    }

    public function role(){
        return $this->BelongsTo('App\Role');
    }
}
