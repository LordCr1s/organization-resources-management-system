<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perishable extends Model
{
    protected $fillable = ['quantity','equipment_id','unit_measure_id',];
    protected $foreignKey = ['equipment_id','unit_measure_id'];
    protected $primaryKey = 'id';
    protected $table = 'perishable';

    public function equipmentInformation()
    {
        return $this->belongsTo('App\EquipmentInformation','unit_measure_id');
    }
    public function equipment()
    {
        return $this->hasOne('App\Equipment', 'equipment_id');
    }
}
