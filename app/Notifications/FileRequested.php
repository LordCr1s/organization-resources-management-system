<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class FileRequested extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($fileRequest)
    {
        $this->fileRequest = $fileRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = User::where('id', $this->fileRequest->receiver_id)->first();
        return (new MailMessage)
                    ->subject('File Request')
                    ->line($user->firstname .' '. $user->surname .' '.'request the file(s) which you hold.')
                    ->line('Go to OREMS to Accept or Reject the request.')
                    ->action('Go To OREMS', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $user = User::where('id', $this->fileRequest->receiver_id)->first();
        return [
            'title' => 'File Request',
            'body'=> $user->firstname .' '. $user->surname .' '.'request the file(s) which you hold.',
            'date' => $this->fileRequest->created_at
        ];
    }
}
