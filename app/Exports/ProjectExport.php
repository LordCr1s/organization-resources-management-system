<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProjectExport implements FromCollection
{

    public function __construct($id)
    {
        $this->id = $id;
        $this->file = Project::where('id', $id)->get();
        //$this->item  = ;
    }

    //
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->file;
    
    }
}
