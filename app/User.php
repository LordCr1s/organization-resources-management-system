<?php

namespace App;

//use App\Parmissions\HasPermissionsTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\VerifyApiEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail , JWTSubject
{ 
    use Notifiable;
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','surname', 'email', 'password', 'department_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function department_requests(){
        return $this->hasMany('App\DepartmentRequest');
    }
    public function acquisionRequests(){
        return $this->hasMany('App\AcquisionRequest','acquision_id','id');
    }
    public function ReceivedNotes(){
        return $this->hasMany('App\ReceivedNote');
    }

    public function department_transactions(){
        return $this->hasMany('App\DepartmentTransaction');
    }

    public function chats()
    {
        return $this->hasMany('App\Chat');
    }

    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    public function fileRequests()
    {
        return $this->hasMany('App\FileRequest');
    }

    public function fileTransctions()
    {
        return $this->hasMany('App\FileTransaction');
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role){
                if ($this->hasRole($role)){
                    return true;
                }
            }
        } 
        else {
            if ($this->hasRole($roles)){
            return true;
            }
        }
        return false;
    }
    public function hasRole($role){
        if ($this->roles()->where('name', $role)->first()){
            return true;
        }
        return false;
    }

  
     public function permissions() {
        return $this->belongsToMany('App\Permission');
  
     }
  
     public function hasPermissionTo($permission) {
        return $this->hasPermission($permission) || $this->hasPermissionThroughRole($permission);
     }
   
     public function hasPermission($permission) {
        return (bool) $this->permissions()->where('name', $permission)->first();
     }
  
     public function hasPermissionThroughRole($permission) {
        foreach ($permission->roles as $role){
           if($this->roles->contains($role)) {
              return true;
           }
        }
        return false;
     }

     public function hasAnyPermission($permissions)
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permission){
                if ($this->hasPermissionTo($permission)){
                    return true;
                }
            }
        } 
        else {
            if ($this->hasPermissionTo($permissions)){
            return true;
            }
        }
        return false;
    }

    static function role_permissions($roles){
        foreach($roles as $role){
            return $role->permissions;
            
        }
    }
     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Send email verification notification via api
     *
     * @return notification
     */
    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail); 
    }

}
