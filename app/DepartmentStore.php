<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentStore extends Model
{
    protected $fillable = [ 'equipment_id', 'department_id', 'unavilable', 'balance'];

    public function equipment(){
        return $this->belongsTo('App\Equipment');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function department_transactions(){
        return $this->hasMany('App\DepartmentTransaction');
    }

    public function department_requests(){
        return $this->hasMany('App\DepartmentRequest');
    }
}
