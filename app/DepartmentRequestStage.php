<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentRequestStage extends Model
{
    protected $fillable = ['stage', 'department_request_id'];

    public function department_request(){
        return $this->belongsTo('App\DepartmentRequest');
    }
}
