<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InitialAttachment extends Model
{
    protected $fillable = [
        'name','path', 'file_id'
    ];

    public function file()
    {
       return $this->belongsTo('App\File');
    }
}
