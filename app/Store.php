<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = ['name','description',];
    protected $foreignKey = 'department_id';
    protected $table = 'stores';

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id','id');
    }
    public function equipments()
    {
        return $this->hasMany('App\Equipment');
    }
}
