<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentTransaction extends Model
{
    protected $fillable = ['quantity', 'user_id', 'equipment_id', 'department_store_id', 'department_request_id'];

    public function equipment(){
        return $this->belongsTo('App\Equipment');
    }

    public function department_store(){
        return $this->belongsTo('App\DepartmentStore');
    }

    public function department_transaction_statuses(){
        return $this->hasMany('App\DepartmentTransactionStatus');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function department_request(){
        return $this->belongsTo('App\DepartmentRequest');
    }
}
