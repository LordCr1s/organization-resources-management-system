<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentRequest extends Model
{
    
    protected $fillable = [ 'quantity', 'purpose', 'borrow_code', 'return_code', 'user_id', 'equipment_id', 'department_store_id'];

    public function equipment(){
        return $this->belongsTo('App\Equipment');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function department_request_stages(){
        return $this->hasMany('App\DepartmentRequestStage');
    }

    public function department_store(){
        return $this->belongsTo('App\DepartmentStore');
    }

    public function department_transactions(){
        return $this->hasMany('App\DepartmentTransaction');
    }
}
