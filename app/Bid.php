<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $guarded = [];

    public function tender(){
        return $this->belongsTo('App\Tender','tender_id','id');
    }
    public function supplier(){
        return $this->belongsTo('App\Supplier','supplier_id','id');
    }
}
