<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'organization_name','address',
        'email' ,'telephone','fax'
    ];
    protected $table = 'organizations';
    public function departments(){
        return $this->hasMany('App\Department');
    }
}
