<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentInformation extends Model
{
    protected $fillable = ['name','description',];
    protected $table = 'equipment_information';

    public function NonPerishable()
    {
        return $this->hasMany('App\NonPerishable');
    }
    public function Perishable()
    {
        return $this->hasMany('App\Perishable');
    }

}
