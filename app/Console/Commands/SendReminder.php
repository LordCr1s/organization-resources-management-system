<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\FileTransaction;
use App\Reminder;

class SendReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder for user how have unprocessed files in every 2 min';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unprocessedfiles = FileTransaction::where('processed', '0')->get();
        $users = User::whereIn('id', $unprocessedfiles->pluck('id'))->get();
        //dd($unprocessedfiles); 
        foreach ($users as $user) {
            $reminder= Reminder::create([
                'user_id' => $user->id,
                'message' => 'you have unprocessed file. please don\'t slow down other workers.'
            ]);
        }
    }
}
