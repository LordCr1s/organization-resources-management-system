<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentTransactionStatus extends Model
{
    protected $fillable = ['status', 'department_transaction_id'];

    public function department_transaction(){
        return $this->belongsTo('App\DepartmentTransaction');
    }
}
