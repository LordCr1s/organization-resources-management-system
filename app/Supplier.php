<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $guarded = [];

    public function bids(){
        return $this->hasMany('App\Bid');
    }
   
}
