<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreTransaction extends Model
{
    protected $fillable = ['transaction_number','department_id','acquision_id','receiving_user'];
    protected $foreignKey = ['department_id','acquision_id','receiving_user'];
    protected $primaryKey = 'id';
    protected $table = 'store_transactions';

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'receiving_user');
    }
    public function acquisionRequests()
    {
        return $this->hasMany('App\AcquisionRequest', 'acquision_id');
    }
    public function equipments(){
        return $this->hasManyThrough('App\Equipment', App\EquipmentStoreTransaction);
    }
}
