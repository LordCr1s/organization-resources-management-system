<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class EquipmentStoreTransaction extends Pivot
{
    protected $table='equipment_store_transactions';
}
