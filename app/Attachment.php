<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = ['name','path','file_id','circle_id'];

    public function file()
    {
        return $this->belongsTo('App\File');
    }

    public function circle()
    {
        return $this->belongsTo('App\Circle');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
