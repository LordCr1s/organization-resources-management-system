<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceivedNote extends Model
{

    protected $fillable = ['grn_no','equipment_id','supplier_id','department_id','approved_user'];
    protected $hidden = ['equipment_id','approved_user'];
    protected $primaryKey = 'id';
    protected $table = 'received_notes';

    public function equipments()
    {
        return $this->hasMany('App\Equipment');
    }
    public function approved_user()
    {
        return $this->belongsTpo('App\User', 'approved_user','id');
    }
    public function received_user(){
        return $this->belongsTo('App\User', 'received_user','id');
    }
    public function supplier(){
        return $this->belongsTo('App\Supplier', 'supplier_id','id');
    }
    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id','id');
    }
}
