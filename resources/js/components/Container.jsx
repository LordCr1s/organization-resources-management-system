import React, { Component } from 'react'
import Topnav from './Topnav'
import Sidenav from './Sidenav'
import Content from './Content'
import { connect } from 'react-redux'
import Login from './Auth/Login'
import Register from './Auth/Register'

class Container extends Component {

  componentDidMount = () => {
    // initialize user details
    this.props.dispatch(this.props.actions.initializeUser({
        is_technician: true
    }))
  }

  render() {
    return (
      <React.Fragment>
        { (localStorage.getItem('access_token') && localStorage.getItem('user')) ? 
        <div className="d-flex">
            <Sidenav />
            <div className="main">
              <Topnav />
              <main className="content">
                <div className="container-fluid p-0">
                  <Content />
                </div>
              </main>
            </div>
        </div> :
        <Login />
      }
      {/* <Register /> */}
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user_provider.user,
  user_manager: state.user_provider.user_manager,
  actions: state.user_provider.actions,
})

export default connect(mapStateToProps)(Container);