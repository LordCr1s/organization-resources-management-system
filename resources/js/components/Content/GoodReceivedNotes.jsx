import React, { Component } from 'react'
import SideBarBaseTemplate from './SideBarBaseTemplate'
import Note from './GoodReceivedNotes/Note'


export default class GoodReceivedNotes extends Component {

    componentDidMount() {
        // perform fetch here
        this.getGrn()
    }
    formValues = {}
    
    getGrn = () => {
        fetch('api/grns')
        .then( res => res.json())
        .then( response => {
            console.log(response)
            this.setState({
                isFetching: false,
                reports: response
            })
        })
    }
    getFormValues = (e) => {
        this.formValues = {
            ...this.formValues,
            [e.target.name]: e.target.value
        }
    }
   
    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }
    render() {
        const {isFetching,reports} = this.state
        const actions={
            getGrn: this.getGrn,
        }
        const formData={}

        const context = {
            heading: "Report generation",
            search_placeholder: "Search reports",
            isFetching: isFetching,
            setComponent: item => <Note key={item.id} report={item} actions={actions}/>
        }

        return (
            <React.Fragment>
                <SideBarBaseTemplate context={context} items={reports}/>
            </React.Fragment>
        )
    }

    state = {
        isFetching: true,
        // update this object with real data from the API
        reports: [
            {
                id: 2,
                received_by: "Mansoor Mansoor",
                received_at: "9 May 2018",
                status: "status",
                number: "4355453",
            },
        ],
    }
}
