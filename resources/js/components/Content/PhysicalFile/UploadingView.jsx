import React, { Component } from 'react'

export default class UploadingView extends Component {
  render() {
    return (
      <div>
        <form action="">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Request type</label>
                    <select class="form-control mt-1" style={{width: 80 + '%'}}>
                        {this.state.request_types.map(type => (
                            <option key={type}>{type}</option>
                        ))}
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPassword4" className="ml-3">Attach file</label>
                    <input type="file" class="form-control" id="inputPassword4" placeholder="Password" style={{border: 'none'}}/>
                </div>
                <div class="form-group col-md-4 pt-2">
                    <button className="btn btn-outline-primary  ml-5 mt-4">
                        <i className="fe fe-upload mr-2 align-midle"></i>
                        <span className="align-midle">Upload</span>
                    </button>
                </div>
            </div>
        </form>
        <hr />
        <div class="form-group col-md-4 mt-4">
            <label for="inputEmail4">Form options header</label>
            <select class="form-control mt-1" style={{width: 80 + '%'}}>
                {this.state.form_types.map(type => (
                    <option key={type.id} onClick={() => this.setState({...this.state, visible_form: type.id })}>{type.name}</option>
                ))}
            </select>
        </div>
        <hr />
        <div className="row">
            {
                (this.state.visible_form == 1 ) ? 
                    <div className="col-12"> form one </div> :
                    <div className="col-12"> form two </div>
            }
        </div>
      </div>
    )
  }

  state = {
      request_types: ['type one', 'type two', 'type three'],
      form_types: [{id: 1, name: 'type one'}, {id: 2, name:'type two'}],
      visible_form: 1
  }
}
