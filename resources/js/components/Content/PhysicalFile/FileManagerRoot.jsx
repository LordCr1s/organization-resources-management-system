import React, { Component } from 'react'
import UploadingView from './UploadingView'
import FileList from './FileList'

export default class FileManagerRoot extends Component {

  render() {
    return (
      <div className="row">
        <div className="col-12">
            <div className="card mb-2">
                <div className="card-header">
                    <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                        <li className="nav-item">
                            <a className="nav-link active" data-toggle="tab" 
                                href="#tab-4">
                                <i className="align-middle fe fe-moon"></i>
                                <span className="align-middle ml-2">New Request</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-5">
                                <i className="align-middle fe fe-star"></i>
                                <span className="align-middle ml-2">My Requests</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-6">
                                <i className="align-middle fe fe-trending-down"></i>
                                <span className="align-middle ml-2">Incoming Request</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-7">
                                <i className="align-middle fe fe-check-circle"></i>
                                <span className="align-middle ml-2">Request Approved By Me</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="card-body">
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id="tab-4" role="tabpanel">
                            <UploadingView />
                        </div>
                        <div className="tab-pane fade" id="tab-5" role="tabpanel">
                            {this.state.requests.map(request => (
                              <FileList request={request}/>
                            ))}
                        </div>
                        <div className="tab-pane fade" id="tab-6" role="tabpanel">
                          {this.state.requests.map(request => (
                              <FileList request={request} for_tab={this.state.for_tab}/>
                            ))}
                        </div>
                        <div className="tab-pane fade" id="tab-7" role="tabpanel">
                          {this.state.requests.map(request => (
                              <FileList request={request}/>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
  }

  state = {
    requests: [
      {id: 1, name: 'store requisition', serial_number: '22423232', state: 'on progress', created_at: '09 july 2019'},
      {id: 1, name: 'store acquisition', serial_number: '98090900', state: 'aproved', created_at: '07 April 2019'},
    ],
    for_tab: 'incoming'
  }
}
