import React, { Component } from 'react'

export default class FileList extends Component {
  render() {

    const request = this.props.request
    return (
        <div className="card mb-2">
            <div className="card-body p-2">
                <div className="row">
                    <div className="col-1">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src="img/pdf.svg" alt="Placeholder" width="80" height="80" />
                        </div>
                    </div>
                    <div className="col p-1 ml-3">
                        <h5 className="card-title mb-1 mt-1 text-primary ml-1"> {request.name} </h5>
                        <h6 className="text-dark mb-1">
                            <span className="text-muted ml-1 small"> serial number : {request.serial_number}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            <span className="text-muted ml-1 small">state : {request.state}</span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{request.created_at}</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-auto">
                        {
                            (this.props.for_tab == 'incoming') ?
                            <React.Fragment>
                                <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                        data-target={"#verificationModal" + request.id}>
                                    <i className="fe fe-thumbs-up mr-2 align-midle"></i>
                                    <span className="align-midle">Approve</span>
                                </button>

                                <VerificationModal request={request}/>

                                <button className="btn btn-outline-primary  ml-3 mt-4">
                                    <i className="fe fe-eye mr-2 align-midle"></i>
                                    <span className="align-midle">view</span>
                                </button>
                            </React.Fragment>
                            :
                            <button className="btn btn-outline-primary  ml-5 mt-4">
                                <i className="fe fe-eye mr-2 align-midle"></i>
                                <span className="align-midle">view</span>
                            </button>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

class VerificationModal extends Component {

    approveRequest = (request) => {
        // make api request here
        
    }

    render() {

        const request = this.props.request

        return (
            <div className="modal fade" id={"verificationModal" + request.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-sm" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <p>Are you sure you want to approve a request for {request.name}?</p>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">No</button>
                                <button type="button" className="btn btn-danger" data-dismiss="modal"
                                    onClick={() => this.approveRequest(request)}>
                                    Yes
                            </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}