import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class Conversation extends Component {
  render() {

    const conversation = this.props.conversation

    return (
        <Link to='/conversation'>
            <div className="card mb-2">
                <div className="card-header p-2">
                    <div className="row">
                        <div className="col-auto">
                            <div className="text-left">
                                <img className="avatar img-fluid rounded-circle mt-2 ml-2" src={conversation.image} alt="Placeholder" width="42" height="42" />
                            </div>
                        </div>
                        <div className="col-auto p-1">
                                <span className="align-middle">{conversation.name}</span>
                                                    
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-watch text-warning">
                                    <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{conversation.last_chat}</span>
                                </span>
                            </h6>
                            
                        </div>
                    </div>
                </div>
            </div>
        </Link>
    )
  }
}
