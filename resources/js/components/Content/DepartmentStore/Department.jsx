import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { notify } from '../Utilities'

export default class Department extends Component{
    componentDidMount(){
        this.getDepartmentStores()
        // this.getDepartments()
    }
   
    getDepartmentStores = async () => {
        const {department} = this.props.location

        console.log("Fetching stores...",department)
        await fetch(`api/${department.id}/departmentstores`)
        .then( res => res.json())
        .then( response => {
            console.log('The stores :', response)
                this.setState({
                stores:response
            })
        })
    }
    state ={
        department: {},
        stores:[],
    }
    render() {
        const {department} = this.props.location
        return(
            <React.Fragment>
            <div className="card mb-2">
                <div className="card-body p-2">
                    <div className="row">
                    <div className="col-auto">
                            <div className="text-left">
                                <h4 className="text-muted">{department.id}</h4>
                            </div>
                        </div>
                        <div className="col p-1">
                            <h5 className="card-title mb-1 mt-1 text-primary ml-1">
                            {department.name} </h5>
                            <h6 className="text-muted mb-1 ml-1">
                                <i className="align-middle fe fe-download text-warning"></i>
                                <span className="text-muted ml-2">category : {department.category.name}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-link text-warning"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Brief description: {department.description}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-clock text-warning"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Joined at :{department.created_at  }</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-dollar-sign text-success"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Annual Budget : 5,000,000 Tshs.</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-dollar-sign text-success"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Remaining Budget Amount: 5,000,000 Tshs.</span>
                            </h6>
                            
                        </div>
                        <div className="col-auto mr-3 pt-4">
                           <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                    data-target={"#departmentmodal"+department.id}>
                                <i className="fe fe-edit-3 mr-2 align-midle"></i>
                                <span className="align-midle">edit</span>
                            </button>
                            <UpdateDepartmentModal 
                                key={department.id} 
                                department={department} 
                            />
                            <button className="btn btn-outline-danger  ml-3 mt-4" data-toggle="modal" 
                                    data-target={"#verificationModal" + department.id}>
                                <i className="fe fe-trash-2 mr-2 align-midle"></i>
                                <span className="align-midle">delete</span>
                            </button>
                            <VerificationModal department={department}/>
                        </div>
                    </div>
                </div>
            </div>
            </React.Fragment>
        )
    }
}
class VerificationModal extends Component {

    deleteDepartment = (deletedDepartment) => {
        // perform api request here

        fetch(`api/department/${deletedDepartment.id}`,{
            method: 'DELETE',
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then( res => {
            if (res.status == 200) {
                // this.props.actions.getDepartments()
                {<Link to="/departments" push />}
                notify('success', 'deleted '+ deletedDepartment.name +' successfull', 'success')
            }else{
                notify('Failed', 'request could not be updated', 'error')
            }
        })
    }

    render() {

        const department = this.props.department

        return (
            <div className="modal fade" id={"verificationModal" + department.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-sm modal-center" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <p>Are you sure you want to delete {department.name}?</p>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" id="closeBtn" data-dismiss="modal">No</button>
                                <button 
                                    type="button" 
                                    className="btn btn-danger"
                                    data-dismiss="modal"
                                    onClick={ () => this.deleteDepartment(department) }>
                                    Yes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class UpdateDepartmentModal extends React.Component{
addDepartment = (e) => {
    e.preventDefault()
    const id = e.target.id.value
    let body = {
        ...this.formValues
    }
    // console.log(id)
    fetch(`api/department/${id}`,{
        body: JSON.stringify(body),
        method: 'PUT',
        headers:{
            'Content-Type': 'application/json',
        }
    })
    .then((response) => {
            if (response.status == 200) {
                $(`#close`+id).trigger('click')
                this.props.actions.getDepartments()
                notify('success', ' department updated successful ' , 'success')
                
            }
            else {
                $(`#close`+id).trigger('click')
                notify('Failed', 'request could not be updated', 'error')
            }
        },
        (error) => (console.error(error))
    )
}
render(){
    const {stores,receivedNotes} = this.props.formData
    const {department,categories} = this.props
    return(
        <div className="modal fade" id={"departmentmodal"+department.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-sm" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                            <h4 className="modal-title"> Edit department {department.name +" "+department.id}</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" id={"close"+department.id}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form onSubmit={(e) => this.addDepartment(e)}>
                            <div className="modal-body m-3">
                                <div className="form-group">
                                    <label className="form-label">Department Name</label>
                                    <input type="text" name="name" className="form-control"
                                        id="name" onChange={(e) => this.getFormValues(e)} defaultValue={department.name}/>
                                    <input type="hidden" name="id" defaultValue={department.id}/>
                                </div>
                                <div className="form-group">
                                    <label className="form-label">description</label>
                                    <textarea name="description" id="" cols="30" rows="10" 
                                        className="form-control" onChange={(e) => this.getFormValues(e)}></textarea>
                                    
                                </div>
                                <div className="form-group">
                                    <label className="form-label">category</label>
                                    <select name="category" id="" className="form-control select2" onChange={(e) => this.getFormValues(e)}>
                                    {categories.map( (category) => 
                                            <option 
                                                key={category.id} 
                                                value={category.id} 
                                                selected={(category.id == department.category_department_id) ? true : false}>
                                                {category.name}
                                            </option>
                                        )}
                                    </select>
                                </div>
                                <div className="mt-3">
                                    <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                                    <button className="btn btn-primary" type="submit">
                                        <i className="fe fe-upload mr-2 align-midle"></i>
                                        <span className="align-midle">Update</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                
                    </div>
                </div>
            </div>
        )
    }
}