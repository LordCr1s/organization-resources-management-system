import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import { notify } from '../Utilities'

export default class Store extends Component{
    render(){
        const {store} = this.props
        return(
            <React.Fragment>
            <div className="card mb-2">
                <div className="card-body p-2">
                    <div className="row">
                        <div className="col-auto">
                            <div className="text-left">
                                {/* <img className="rounded mt-2 ml-2" src={material.image} alt="Placeholder" width="110" height="110" /> */}
                                <h4 className="text-muted">{store.id}</h4>
                            </div>
                        </div>
                        <div className="col p-1">
                            <h5 className="card-title mb-1 mt-1 text-primary ml-1"> {store.name} </h5>
                            <h6 className="text-dark mb-1 ml-1">
                                <i className="fe fe-check-circle text-success"></i>
                                <span className="text-muted ml-2" style={{ fontFamily: 'cerebrisans-regular' }}>Status : Available</span>    
                                
                            </h6>
                            
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-link text-warning"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Department : {store.department.name}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-clock text-warning"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Date Added : {store.created_at  }</span>
                            </h6>
                            <div className="pt-1">
                                <a href="" data-toggle="modal" data-target={"#seeMoreModal" + store.id}>
                                    <span className="fe fe-eye text-primary ml-1"></span>
                                    <span className="ml-2" style={{ fontFamily: 'cerebrisans-regular', textDecoration: 'underline' }}>see more</span>
                                </a>
                                <SeeMoreModal key={store.id} item={store} />
                            </div>
                        </div>
                        <div className="col-auto mr-3 pt-4">
                            <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                    data-target={"#updateStoreModal"+store.id}>
                                <i className="fe fe-edit-3 mr-2 align-midle"></i>
                                <span className="align-midle">edit</span>
                            </button>
                            <UpdateMaterialModal 
                                key={store.id}
                                store={store} 
                                departments={this.props.departments}
                                actions={this.props.actions}/>

                            <button className="btn btn-outline-danger  ml-3 mt-4" data-toggle="modal" 
                                    data-target={"#verificationModal" + store.id}>
                                <i className="fe fe-trash-2 mr-2 align-midle"></i>
                                <span className="align-midle">delete</span>
                            </button>
                            <VerificationModal key={store.id} store={store} actions={this.props.actions}/>
                        </div>
                    </div>
                </div>
            </div>
            </React.Fragment>
        )
    }
}

class UpdateMaterialModal extends React.Component{
    formValues = {}
    getFormValues = (e) => {
        this.formValues ={
            ...this.formValues,//unpacking the existing contents
            [e.target.name] : e.target.value
        }
    }
    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }

    addStore = (e) => {
        e.preventDefault()
        const id = e.target.id.value
        let body = {
            ...this.formValues
        }
        // console.log(id)
        fetch(`api/store/${id}`,{
            body: JSON.stringify(body),
            method: 'PUT',
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then((response) => {
                if (response.status == 200) {
                    $(`#closeBtn`+id).trigger('click')
                    this.clearInputs()
                    this.props.actions.getStores()
                    notify('success', ' material updated successful ' , 'success')
                    
                }
                else {
                    $(`#close`+id).trigger('click')
                    notify('Failed', 'request could not be updated', 'error')
                }
            },
            (error) => (console.error(error))
        )
    }
    
    render(){
        const {departments,store} = this.props
        
        return(
            <React.Fragment>
            <div className="modal fade" id={"updateStoreModal"+store.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"> update {store.name} store Information</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id={'closeBtn'+store.id}>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body m-3">
                            <form onSubmit={(e) => this.addStore(e)}>
                                <div className="form-group">
                                    <label className="form-label">Store Name</label>
                                    <input type="hidden" name="id" defaultValue={store.id}/>
                                    <input type="text" name="name" className="form-control"
                                        defaultValue={store.name} onChange={(e) => this.getFormValues(e)}/>
                                </div>
                            
                                <div className="form-group">
                                    <label className="form-label">description</label>
                                    <textarea name="description" style={{resize:"none"}} cols="30" rows="10" defaultValue={store.description} 
                                        className="form-control" onChange={(e) => this.getFormValues(e)}></textarea>
                                </div>
                                <div className="form-group">
                                    <label className="form-label">Store department</label>
                                    <select name="department_id" className="form-control select2" 
                                        data-toggle="select2" onChange={(e) => this.getFormValues(e)}>
                                        <option defaultValue>~~departments~~</option>
                                        {departments.map( (department) => (
                                            <option value={department.id} 
                                                selected={(department.id == store.department_id) ? true : false} key={department.id}>
                                                {department.name +' of '+ department.category.name  }</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="mt-3">
                                    <button className="btn btn-primary" type="submit">
                                        <i className="fe fe-upload mr-2 align-midle"></i>
                                        <span className="align-midle">add</span>
                                    </button>
                                </div>
                            </form>                                     
                        </div>
                        <div className="modal-footer">
                            <div className="row mt-3">
                                <div className="col-12">
                                    <button type="button" className="btn btn-secondary float-right"
                                        data-dismiss="modal" onClick={() => this.clearInputs()}>Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </React.Fragment>
        )
        }
}
class VerificationModal extends Component {

    deleteMaterial = (deletedMaterial) => {
        // perform api request here

        fetch(`api/store/`+deletedMaterial.id,{
            method: 'DELETE',
            // method: 'PUT'
            // body: JSON.stringify({'status': 0}),
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then( res => {
            if (res.status == 200) {
                this.props.actions.getStores()
                notify('success', 'deleted '+ deletedMaterial.name +' successfull', 'success')
            }else{
                notify('Failed', 'request could not be updated', 'error')
            }
        })
    }

    render() {

        const {store} = this.props

        return (
            <div className="modal fade" id={"verificationModal" + store.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-sm modal-center" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <p>Are you sure you want to delete {store.name}?</p>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" id="closeBtn" data-dismiss="modal">No</button>
                                <button 
                                    type="button" 
                                    className="btn btn-danger"
                                    data-dismiss="modal"
                                    onClick={ () => this.deleteMaterial(store) }>
                                    Yes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class SeeMoreModal extends React.Component{
    render(){
        const {item} = this.props
        return(
        <div className="modal fade" id={"seeMoreModal" + item.id} tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title"> Information for {item.name +" "+ item.id}</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body m-3" style={{ fontFamily: 'cerebrisans-regular' }}>
                        <div className="row">
                            <div className="col-7">
                                <h3 className="mb-1 mt-1 text-primary ml-1"> {item.name} </h3>
                                <h6 className="text-dark mb-1 ml-1">
                                    <i className="fe fe-check-circle text-success"></i>
                                    <span className="text-muted ml-2" >Status : Active</span>                        
                                </h6>
                                <h6 className="text-muted mb-1 ml-1">
                                    <i className="align-middle fe fe-download text-warning"></i>
                                    <span className="text-muted ml-2">Store Keeper : Mansoor Rashid</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-1">
                                    <i className="align-middle fe fe-edit text-warning"></i>
                                    <span className="text-muted ml-2">Brief description : {item.description}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-1">
                                    <span className="fe fe-link text-warning"></span>
                                    <span className="ml-2 text-muted" >Department : {item.department.name}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-1">
                                    <span className="fe fe-clock text-warning"></span>
                                    <span className="ml-2 text-muted" >Date added : {item.created_at  }</span>
                                </h6>
                                
                            </div>                        
                            <div className="col-auto float-right">
                                <button className="btn btn-outline-secondary ml-1" 
                                    data-dismiss="modal" data-toggle="modal" 
                                    data-target={"#materialmodal"+item.id}>Edit
                                </button>
                                <button className="btn btn-outline-danger ml-2" 
                                    data-dismiss="modal" data-toggle="modal" 
                                    data-target={"#verificationModal"+item.id}>Delete
                                </button>
                            </div>
                        </div>

                        <div className="mt-3">
                            <button type="button" className="btn btn-outline-warning mr-3 float-right" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
}