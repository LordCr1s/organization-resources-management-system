import React, { Component } from 'react'
import Material from './Material'

export default class TechnicianMaterial extends Component {
  render() {
      const material = this.props.material
    return (
        <div className="row">
            <div className="col-12">
                <div className="card mb-2">
                    <div className="card-header">
                        <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" data-toggle="tab" 
                                    href={"#tab-4" + material.id} style={{padding: 0.4 + 'rem'}}>
                                    <i className="align-middle fe fe-send"></i>
                                    <span className="align-middle ml-2">Request material</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href={"#tab-5" + material.id} style={{padding: 0.4 + 'rem'}}>
                                    <i className="align-middle fe fe-sliders"></i>
                                    <span className="align-middle ml-2">Manage {material.name}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="card-body p-0 m-0">
                        <div className="tab-content">
                            <div className="tab-pane fade show active" id={"tab-4" + material.id} role="tabpanel">
                                <Material material={material}/>
                            </div>
                            <div className="tab-pane fade" id={"tab-5" + material.id} role="tabpanel">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}
