import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { notify } from '../Utilities'
import TechnicianMaterial from './TechnicianMaterial'


export default class Material extends Component {

    constructor(props) {
        super(props)

        this.state = {
            // initialize state with empty logs
            logs : []
        }
    }

    requestForMaterial = (material) => {
        const requested_quantity = $('#quantity' + material.id).val()
        const available_quantity = material.available

        const requested_quantity_is_valid = this.validateRequestedQuantity(requested_quantity, available_quantity)
        if (requested_quantity_is_valid) {
            // make api request here
        }
    }

    // if quantity requested is too low or too high do not send request, send request if otherwise
    validateRequestedQuantity = (requested_quantity, available_quantity) => {
        if (requested_quantity > available_quantity) {
            notify('Failed', 'request could not be sent, quantity set is too high', 'error')
            return false
        }
        else if (requested_quantity <= 0) {
            notify('Failed', 'request could not be sent, quantity set is too low', 'error')
            return false
        }
        else {
            notify('success', 'request sent  successfull', 'success')
        }
    }

    // balances between available and unavailable materials as the 
    // technician changes them
    materialQuantityBalancer = async (material) => {
        const unavailable_quantity = $('#unavailable_quantity' + material.id).val()
        const available_quantity = $('#available_material_label' + material.id)

        let unavailable = {
            is_greater: () => {
                if (unavailable_quantity > parseInt(material.available))
                    return true
                else 
                    return false
            },
            is_lesser: () => {
                if (unavailable_quantity < 0)
                    return true
                else 
                    return false
            },
            reset_to_default: () => {
                $('#unavailable_quantity' + material.id).val(material.unavailable)
                available_quantity[0].innerHTML = material.available
            }
        }

        if (unavailable.is_greater() || unavailable.is_lesser()) {
            unavailable.reset_to_default()
        } else {
            // make api request here
            available_quantity[0].innerHTML = (
                parseInt(material.available) - parseInt(unavailable_quantity)
            )
        } 
    }

    componentDidMount () {
        // get logs of this material
        fetch('http://172.20.10.5:8003/material_tracker/getlogs/'+ this.props.material.id +'?ZkWE=2345&tGjx=1',{
            method: 'GET'
        })
        .then( response => response.json())
        .then( response => {
            this.setState({
                logs: response
            })
        })
    }

    render() {
        const material = this.props.material
        return (
            <div className="card mb-2">
                <div className="card-body p-2">
                    <div className="row">
                        <div className="col-auto">
                            <div className="text-left">
                                <img className="rounded mt-2 ml-2" src={material.image} alt="Placeholder" width="110" height="110" />
                            </div>
                        </div>
                        <div className="col-auto p-1">
                            <h5 className="card-title mb-1 mt-1 text-primary ml-1"> {material.name} </h5>
                            <h6 className="text-dark mb-1">
                                <span className="text-muted ml-1">identifier &nbsp;{material.id}</span>
                            </h6>
                            <h6 className="text-muted mb-1">
                                <span className="text-muted ml-1">{material.category || 'category placeholder'}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-link text-warning">
                                    <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>
                                        {material.store.department.name}
                                    </span>
                                </span>
                            </h6>
                            <div className="pt-1">
                                {/* 
                                    <a href="" data-toggle="modal" data-target={"#requestForMaterialModal" + material.id}>
                                        <span className="fe fe-send text-primary ml-1">
                                            <span className="ml-2" style={{ fontFamily: 'cerebrisans-regular', textDecoration: 'underline' }}>
                                                request {material.name}
                                        </span>
                                        </span>
                                    </a>
                                    <a href="" data-toggle="modal" data-target={"#manageMaterialModal" + material.id}>
                                            <span className="ml-2 text-secondary" style={{ textDecoration: 'underline' }}>
                                                manage {material.name}
                                        </span>
                                    </a> 
                                */}

                                <a href="" data-toggle="modal" data-target={"#materialMovementModal" + material.id}>
                                    <span className="fe fe-send text-primary ml-1">
                                        <span className="ml-2" style={{ fontFamily: 'cerebrisans-regular', textDecoration: 'underline' }}>
                                            see {material.name} movements
                                    </span>
                                    </span>
                                </a>
                                <div className="modal fade" id={"materialMovementModal" + material.id} 
                                    tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                                    <div className="modal-dialog modal-md" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title"> <span className="text-primary">{material.name}</span> </h4>
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div className="modal-body m-3">
                                                <div className="card flex-fill w-100">
                                                    <div className="p-4 bg-light">
                                                        <h2>Tracking movements of {material.name}</h2>
                                                        <p className="mb-0 text-sm">Use this log to find the last user to receive this material</p>
                                                    </div>
                                                    <div className="card-body">
                                                        <ul className="timeline">
                                                            {
                                                                this.state.logs.map(log => (
                                                                    <li className="timeline-item" key={log.id}>
                                                                        <strong>{log.source + " " + log.action + " " + log.destination}</strong>
                                                                        <span className="float-right text-muted text-sm">{log.logged_at}</span>
                                                                        <ul>
                                                                            <li>{log.source}'s reg number 160120120227</li>
                                                                            <li>{log.destination}'s reg number 160220220345</li>
                                                                            <li>{log.material_id}'s serial number 43ER343ED</li>
                                                                        </ul>
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                    <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal fade" id={"manageMaterialModal" + material.id} 
                                    tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                                    <div className="modal-dialog modal-sm" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title"> Manage {material.name}</h4>
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div className="modal-body m-3">
                                                <div className="form-group">
                                                    <label className="form-label">Available quantity</label>
                                                    <label className="form-label"> :
                                                        <span className="ml-2" id={"available_material_label" + material.id}>{ material.available }</span>
                                                    </label>
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Unavailable quantity</label>
                                                    <input type="number" className="form-control" id={"unavailable_quantity" + material.id}
                                                        defaultValue={material.unavailable} max={material.available} min={0} 
                                                        onChange={() => this.materialQuantityBalancer(material)} />
                                                </div>
                                                <div className="form-group">
                                                    <label className="form-label">Total quantity</label>
                                                    <label className="form-label">:
                                                        <span className="ml-2">{ parseInt(material.available) + parseInt(material.unavailable) }</span>
                                                    </label>
                                                </div>
                                                <div className="mt-3">
                                                    <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                                                    <button type="button" className="btn btn-primary" data-dismiss="modal"
                                                        onClick={() => this.requestForMaterial(material)}>
                                                        save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}