import React, { Component } from 'react'
import SideBarBaseTemplate from '../SideBarBaseTemplate'
import Material from './Material'

export default class LecturerMaterials extends Component {

    render() {

      const context = {
          heading: "Materials in Computer Department",
          search_placeholder: "Search for materials",
          setComponent: item => <Material key={item.id} material={item} />
      }
      
      return (
        <React.Fragment>
            <SideBarBaseTemplate context={context} items={this.state.materials}/>
        </React.Fragment>
      )
    }
    
    componentDidMount() {
      this.getMaterials()
  }
 
  getMaterials = () => {
      fetch('api/equipments',{
          method: 'GET'
      })
      .then( res => res.json())
      .then( res => {
          this.setState({
              isFetching: false,
              materials: res
          })
      })
  }

    state = {
      materials: [
        //{
      //         id: 1,
      //         name: "Projector",
      //         image: "img/projector.jpg",
      //         available: "6",
      //         unavailable: "0",
      //         category: "Multimedia",
      //         department: "Computer Department"
      //     },
      //     {
      //         id: 2,
      //         name: "Switch",
      //         image: "img/switch.jpg",
      //         available: "12",
      //         unavailable: "0",
      //         category: "Networking",
      //         department: "Computer Department"
      //     },
      //     {
      //         id: 3,
      //         name: "Cable Tester",
      //         image: "img/cable_tester.JPG",
      //         available: "10",
      //         unavailable: "0",
      //         category: "Networking",
      //         department: "Computer Department"
      //     },
      //     {
      //         id: 4,
      //         name: "Crimping Tool",
      //         image: "img/crimping.jpg",
      //         available: "22",
      //         unavailable: "0",
      //         category: "Networking",
      //         department: "Computer Department"
      //     },
      //     {
      //         id: 5,
      //         name: "RJ 45 connectors",
      //         image: "img/rj45.jpg",
      //         available: "2",
      //         unavailable: "0",
      //         category: "Networking",
      //         department: "Computer Department"
      //     },
      //     {
      //       id: 6,
      //       name: "Camera (Nikon)",
      //       image: "img/camera.jpg",
      //       available: "3",
      //       unavailable: "0",
      //       category: "Multimedia",
      //       department: "Computer Department"
      //   },
      ],
  }
}