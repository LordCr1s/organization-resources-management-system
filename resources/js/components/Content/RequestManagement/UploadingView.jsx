import React, { Component } from 'react'
import {authHeader} from '../../helpers/authHeader'


export default class UploadingView extends Component {
    constructor() {
        super();
    this.displayData = [];

      this.state = {
        file: '',
        showdata : this.displayData,
        attachments : []
      }
    }
    appendData = (e)=> {
        e.preventDefault();
       
        
        let file = new FormData();
        let reader = new FileReader();
        reader.onload = ()=> (reader.readAsDataURL(ogfile))
        reader.onloadend = (e)=> (e.target.result)
        let ogfile = $('#file').val()
        // this.createImage(ogfile); 
        let data = {
            name: $('#attachment').val(),
            file: ogfile
        }
        this.displayData.push(data);
        this.setState({
           showdata : this.displayData,
           attachments : [
            //   {'attachmentName' + this.attachments.length : speechSynthesis} ,
           ]
        });
     }

     createImage(file) {

        let reader = new FileReader();
        
        reader.onload = (e) => {
        
        this.setState({
        
        image: e.target.result
        
        })
        
        };
        
        reader.readAsDataURL(file);
        
        }
    
     handleSubmit = (e) => {
        e.preventDefault();
        let requestBody = {
            name: $('input[name=filename]').val(),
            reference_no: $('input[name=reference_no]').val(),
            department_id: $('#department').val(),
            access_level: $('#access_level').val(),
            file_type: $('#file_type').val(),
            attachments: this.displayData
        }
        
        fetch('api/files/new',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            console.log('circle Added')
        })
    }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
            <div className="form">
                <div className="form-group col-md-6">
                    <label htmlFor="inputEmail4">File Name</label>
                   <input type="text" className="form-control" name="filename"  id="inputEmail4" placeholder="Enter File Name"/>
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="inputEmail5">File Reference Number</label>
                   <input type="text" className="form-control" name="reference_no" id="inputEmail5" placeholder="Enter File Reference Number"/>
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="inputEmail4">Department</label>
                    <select className="form-control mt-1" name="department" id="department" style={{width: 100 + '%'}}>
                        <option value="" disabled defaultValue> Select Department</option>
                       {this.props.departments.map(department=>(
                        <option value={department.id}>{department.name}</option>
                       ))}
                    </select>
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="inputEmail4">File Type</label>
                    <select className="form-control mt-1" name="file_type" id="file_type" style={{width: 100 + '%'}}>
                        <option value="" disabled defaultValue> Select File Type</option>
                        <option value="Personal">Personal</option>
                        <option value="Subject">Subject</option>
                    </select>
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="inputEmail4">File Access Level Type</label>
                    <select className="form-control mt-1" name="access_level" id="access_level" style={{width: 100 + '%'}}>
                    <option value="" disabled defaultValue>Select File Access Level Type</option>
                        <option value="Public">Public</option>
                        <option value="Private">Private</option>
                    </select>
                </div>
                <div id="add-attachment">
                    <div className="form-group col-md-6">
                        <label htmlFor="attachment">Attachment Name</label>
                        <input type="text" className="form-control" id="attachment" placeholder="Attachment Name"/>
                    </div>
                    <div className=" col-md-6">
                        <input type="file"  id="file" placeholder="Attachment Name" name="file" />
                    </div>
                    <div className="form-group col-md-4 pt-2">
                        <button type="submit" className="btn btn-primary mt-1" 
                            onClick={this.appendData}>
                            Add
                        </button>
                    </div>
                    <div className="form-group col-md-6" id="display-data-Container">
                        {this.displayData.map((data, index) => (
                            <div key={index}>
                                <small>Name: {data.name}</small><br/>
                                <small>File: {data.file}</small>
                            </div>
                        ))}
                    </div>
                </div>   
                <div className="form-group col-md-4 pt-2">
                    <button type="submit" className="btn btn-outline-primary mt-4">
                        <i className="fe fe-upload mr-2 align-midle"></i>
                        <span className="align-midle">Add File</span>
                    </button>
                </div>
            </div>
        </form>
<<<<<<< HEAD
    </div>
    )
  }

  state = {
      request_types: ['Acquisition request', 'Requisittion resquesst', 'type three'],
      form_types: [{id: 1, name: 'type one'}, {id: 2, name:'type two'}],
      visible_form: 1
  }
=======
      </div>
    )
  }

  
>>>>>>> 02c7bca671d52da1d8e16253190d28a6d6b01595
}
