import React, { Component } from 'react'
import { notify } from '../../Content/Utilities'


export default class Reminder extends Component {
  render() {

    const reminder = this.props.reminder
    // console.log(file)
    return (
        <div className="card mb-2">
            <div className="card-body p-2"> 
                <div className="row">
                    <div className="col-1">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src="img/folder.svg" alt="Placeholder" width="80" height="80" />
                        </div>
                    </div>
                    <div className="col p-1 ml-3">
                        <h5 className="card-title mb-1 mt-1 text-danger ml-1"> {reminder.message} </h5>
                        <h6 className="text-dark mb-1">
                            {/* <span className="text-muted ml-1 small"> Reference_No : {file.reference_no}</span> */}
                        </h6>
                        <h6 className="text-muted mb-1">
                            {/* <span className="text-muted ml-1 small">state : {file.circles.filter((circle)=>(circle.status === "On Progress")? 'On Progress': 'Complete')}</span> */}
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{reminder.created_at}</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-auto">
                      
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

