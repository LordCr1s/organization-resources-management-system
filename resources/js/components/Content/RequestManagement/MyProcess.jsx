import React, { Component } from 'react'
import { notify } from '../../Content/Utilities'


export default class MyProcess extends Component {
  render() {

    const file = this.props.file
    // console.log(file)
    return (
        <div className="card mb-2">
            <div className="card-body p-2"> 
                <div className="row">
                    <div className="col-1">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src="img/folder.svg" alt="Placeholder" width="80" height="80" />
                        </div>
                    </div>
                    <div className="col p-1 ml-3">
                        <h5 className="card-title mb-1 mt-1 text-primary ml-1"> File To: {file.too.firstname} {file.too.surname}</h5>
                        <h6 className="text-dark mb-1">
                            <span className="text-muted ml-1 small"> Status : {file.status}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            {/* <span className="text-muted ml-1 small">state : {file.circles.filter((circle)=>(circle.status === "On Progress")? 'On Progress': 'Complete')}</span> */}
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{file.created_at}</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-auto">
                      
                    <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                            data-target={"#ViewModal01" + file.id}>
                        <i className="fe fe-thumbs-up mr-2 align-midle"></i>
                        <span className="align-midle">View Info</span>
                    </button>
                    <ViewModal file={file}/>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

class ViewModal extends Component {

    approveRequest = (file) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                this.props.file.name +' successfully', 'success')
    }


    render() {

        const files = this.props.file

        return (
            <div className="modal fade" id={"ViewModal01" + files.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                        <div className="col-12  d-flex">
							<div className="card flex-fill w-100">
								<div className="card-header">
									<div className="card-actions float-right">
								
									</div>
									<h2 className="card-title mb-0">File Information</h2>
								</div>
                                {
                                    files.circle.files.map(file =>(
                                        <div>
                                    <div className="p-4 bg-light">
									<h5>{file.name}</h5>
									<p className="mb-0 text-sm">Reference No:- {file.reference_no}</p>
                                    <p className="mb-0 text-sm">Access Level:- {file.access_level}</p>
                                    <p className="mb-0 text-sm">File Type:- {file.file_type}</p>
                                    <p className="mb-0 text-sm">Tracking No:- {file.Security_no}</p>
                                    <p className="mb-0 text-sm">Department:- {file.name}</p>
								</div>
                                <div className="p-4 bg-light">
                                <h4 className="card-title mb-1">File attachments</h4>
                                <pre className="snippet">
                                    {file.name} / <br/>
                                    { file.attachments.map(attachment=>( 
                                        <div>
                                            <small> {'├── '}  {attachment.name} </small>
                                        </div>
                                         ))}
                                          { file.initial_attachments.map(attachment=>( 
                                        <div>
                                            <small> {'├── '}  {attachment.name} </small>
                                        </div>
                                         ))}
                                          <br/>
                                    <h6>comments</h6>
                                    {file.comments.map(attachment=>( 
                                       <div>
                                            <small>  {attachment.comment} </small>
                                        </div> 
                                     ))}
                                </pre>
                                </div>
								
                                </div>
                                    ))
                                }
								<div className="card-body">
                                <h4 className="card-title mb-2">File Circles</h4>
                                <div>
                                    <p>Reason: {files.circle.reason}</p>
                                    <p>Start: {files.circle.start_at}</p>
                                    <p>End: {files.circle.end_at}</p>
                                    <p>Status: {files.circle.status}</p>
                                </div>
									<ul className="timeline">
                                        {/* {file.circles.map(circle=>(
                                            <li className="timeline-item">
											<strong>{circle.reason}</strong>
											<span className="float-right text-muted text-sm">{circle.created_at}</span>
											<p>{circle.status}</p>
										</li>
                                        ))} */}
									</ul>
								</div>
							</div>
						</div>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}