import React, { Component } from 'react'
import { notify } from '../../Content/Utilities'


export default class FileCircle extends Component {
  render() {

    const circle = this.props.circle
    //console.log(circle)
    return (
        <div className="card mb-2">
            <div className="card-body p-2"> 
                <div className="row">
                    <div className="col-1">
                        <div className="text-left">
                        <img className="rounded mt-2 ml-2" src="img/planning.svg" alt="Placeholder" width="80" height="80" />
                        </div>
                    </div>
                    <div className="col p-1 ml-3">
                        <h5 className="card-title mb-1 mt-1 text-primary ml-1"> {circle.reason} </h5>
                        <h6 className="text-dark mb-1">
                            <span className="text-muted ml-1 small"> Start : {circle.created_at}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            <span className="text-muted ml-1 small">End : {circle.end_at}</span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{circle.status}</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-auto">
                      
                    <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                            data-target={"#ViewModalCircle" + circle.id}>
                        <i className="fe fe-eye mr-2 align-midle"></i>
                        <span className="align-midle">View Info</span>
                    </button>
                    <ViewModal circle={circle}/>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

class ViewModal extends Component {

    approveRequest = (circle) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                this.props.circle.reason +' successfully', 'success')
    }


    render() {

        const circle = this.props.circle

        return (
            <div className="modal fade" id={"ViewModalCircle" + circle.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                        <div className="col-12  d-flex">
							<div className="card flex-fill w-100">
								<div className="card-header">
									<div className="card-actions float-right">
								
									</div>
									<h2 className="card-title mb-0">Circle Information</h2>
								</div>
								<div className="p-4 bg-light">
									<h5>{circle.reason}</h5>
									<p className="mb-0 text-sm">Start At:- {circle.created_at}</p>
                                    <p className="mb-0 text-sm">End At:- {circle.end_at}</p>
                                    <p className="mb-0 text-sm">Status:- {circle.status}</p>
								</div>
                                <div className="p-4 bg-light">
                                <h4 className="card-title mb-1">Files Involved</h4>
                                {circle.files.map(file=>(
                                <ul>
                                    <li>
                                    <small><strong>{file.name}</strong></small>
                                    <span className="float-right text-muted text-sm">Created: {file.created_at}</span>
                                    <p className="text-muted text-sm mb-0">File Type:{file.file_type}</p>
                                    <p className="text-muted text-sm mb-0">Refrence No:{file.reference_no}</p>
                                    <p className="text-muted text-sm mb-0">Access Level:{file.access_level}</p>
                                    <p className="text-muted text-sm mb-0">Tracking No:{file.security_no}</p>
                                </li>
                                </ul>
                                ))}
                                </div>
								<div className="card-body">
                                <h4 className="card-title mb-2">File Transfer</h4>
									<ul className="timeline">
                                        {circle.file_transactions.map(transaction=>(
                                            <li className="timeline-item">
											<strong>Form: {transaction.fromo.firstname} {transaction.fromo.surname} To: {transaction.too.firstname} {transaction.too.surname}</strong>
                                            {(transaction.processed == 1)? <span className="float-right text-muted text-sm">Processed</span>:<span className="float-right text-muted text-sm">Not Processed</span> }
											
											<p>{transaction.status}</p>
										</li>
                                        ))}
									</ul>
								</div>
							</div>
						</div>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}