import React, { Component } from 'react'
import UploadingView from './UploadingView'
import MyRequest from './MyRequests'
import FileCircle from './FileCircle'
import {authHeader} from '../../helpers/authHeader'
import Reminder from './Reminder';
import FileRequest from './FileRequest'
import MyProcess from './MyProcess'
import IncomingFile from './IncomingFile'

export default class RequestManagerRoot extends Component {
    componentDidMount() {
        
        this.getDepartment()
        this.getCircles()
        this.getFiles()
        this.getFileRequests()
        this.getIncomingFiles()
        this.getMyProcess()
        this.getReminder()
        this.getUsers()
        this.requestUserInformation()

      }

    getFiles = ()=>{
        fetch('api/files/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    files: res.files
                })
            }
            // console.log(res.files)
        })
    }

    getCircles = ()=>{
        fetch('api/circle/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    circles: res.circles
                })
            }
            // console.log(res.circles)
        })
    }

    getFileRequests = ()=>{
        fetch('api/file/request/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    requests: res.fileRequest
                })
            }
            //console.log(res.fileRequest)
        })
    }

    getIncomingFiles = ()=>{
        fetch('api/file/transaction/incoming',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    incomings: res.fileTransactions
                })
            }
            // console.log(res.fileTransactions)
        })
    }

    getMyProcess = ()=>{
        fetch('api/file/transaction/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    Processed: res.fileTransactions
                })
            }
            // console.log(res.fileTransactions)
        })
    }

    getReminder = ()=>{
        fetch('api/file/transaction/reminder',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    reminders: res.reminders
                })
            }
            // console.log(res.files)
        })
    }

    getUsers = ()=>{
        fetch('api/users/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    users: res.users
                })
            }
            // console.log(res.files)
        })
    }

    getDepartment = () =>{
        fetch('api/departments',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    departments: res.departments
                })
            }
            // console.log(res.files)
        })
    }

    requestUserInformation = () => {
        fetch('api/auth/me',{
          method: 'POST',
          headers: authHeader()
      })
      .then( res => res.json())
      .then( res => {
        if(res.status == 200){
          this.setState({
              user: res.user,
              roles:res.role
          })
        }
         
          console.log(this.state.user)
      })
    }
      

  render() {
    return (
      <div className="row">
        <div className="col-12">
            <div className="card mb-2">
                <div className="card-header">
                    <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                      {
                         this.state.roles.map(role => (
                            role.name == 'RG'?
                            <React.Fragment>
                        <li className="nav-item">
                            <a className="nav-link active" data-toggle="tab" 
                                href="#tab-4">
                                <i className="align-middle fe fe-moon"></i>
                                <span className="align-middle ml-2">New File</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-5">
                                <i className="align-middle fe fe-star"></i>
                                <span className="align-middle ml-2">All Files</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-6">
                                <i className="align-middle fe fe-trending-down"></i>
                                <span className="align-middle ml-2">File Circle</span>
                            </a>
                         </li></React.Fragment>: null
                         ))
                       }  
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-7">
                                <i className="align-middle fe fe-message-circle"></i>
                                <span className="align-middle ml-2">File Requests</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-8">
                                <i className="align-middle fe fe-arrow-down-circle"></i>
                                <span className="align-middle ml-2">Incoming File</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-9">
                                <i className="align-middle fe fe-check-circle"></i>
                                <span className="align-middle ml-2">Processed By Me</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" 
                                href="#tab-10">
                                <i className="align-middle fe fe-bell"></i>
                                <span className="align-middle ml-2">Reminders</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="card-body">
                    <div className="tab-content">
                        {
                             this.state.roles.map(role => (
                                role.name == 'RG'?
                                <React.Fragment>
                                <div className="tab-pane fade show active" id="tab-4" role="tabpanel">
                                <UploadingView departments={this.state.departments} />
                            </div>
                            <div className="tab-pane fade" id="tab-5" role="tabpanel">
                            <div className="row mb-4">
                                <button className="btn btn-secondary" style={{float: 'right'}} 
                                    data-toggle="modal" data-target="#track-modal">
                                    <span className="fe fe-plus mr-2"></span>
                                    <span>Track File </span>
                                </button>
                                <TrackModal />
                            </div>
                                {this.state.files.map(file => (
                                  <MyRequest key={file.id} file={file}/>
                                ))}
                            </div>
                            <div className="tab-pane fade" id="tab-6" role="tabpanel">
                            <div className="row mb-4">
                                    <button className="btn btn-secondary" style={{float: 'right'}} 
                                        data-toggle="modal" data-target="#circle-modal">
                                        <span className="fe fe-plus mr-2"></span>
                                        <span>create Circle</span>
                                    </button>
                                    <CircleModal files={this.state.files} users={this.state.users}/>
                            </div>
                              {this.state.circles.map(circle => (
                                  <FileCircle key={circle.id} circle={circle} />
                                ))}
                            </div>
                            </React.Fragment>: null))
                        }

                        <div className="tab-pane fade" id="tab-7" role="tabpanel">
                        <div className="row mb-4">
                            <button className="btn btn-secondary mt-0" style={{float: 'right'}} 
                                data-toggle="modal" data-target="#request-modal">
                                <span className="fe fe-plus mr-2"></span>
                                <span>create New File Request</span>
                            </button>
                            <RequestModal files={this.state.files} users={this.state.users}/>
                        </div>
                          {this.state.requests.map(request => (
                              <FileRequest key={request.id} request={request}/>
                            ))}
                        </div>
                        <div className="tab-pane fade" id="tab-8" role="tabpanel">
                          {this.state.incomings.map(incoming => (
                              <IncomingFile key={incoming.id} file={incoming} users={this.state.users}/>
                            ))}
                        </div>
                        <div className="tab-pane fade" id="tab-9" role="tabpanel">
                          {this.state.Processed.map(process => (
                              <MyProcess key={process.id} file={process}/>
                            ))}
                        </div>
                        <div className="tab-pane fade" id="tab-10" role="tabpanel">
                          {this.state.reminders.map(reminder => (
                              <Reminder key={reminder.id} reminder={reminder}/>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
  }

  state = {
    files: [],
    circles:[],
    requests:[],
    incomings:[],
    Processed:[],
    reminders:[],
    users:[],
    departments:[],
    user:{},
    roles:[]
    
  }
}


class CircleModal extends Component {

    approveRequest = (circle) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                 +' successfully', 'success')
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let requestBody = {
            reason: $('#reason').val(),
            user_id: $('#userId').val(),
            file_id: $('#fileId').val(),
        }
        
        fetch('api/circle/new',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            console.log('circle Added')
        })
    }


    render() {
        return (
        <div className="modal fade" id="circle-modal" tabIndex="-1" role="dialog" style={{ zIndex: 9999 }}  aria-modal="true">
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Add New File Circle</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="modal-body m-3 form">
                            <div className="form-group col">
                                <label htmlFor="inputEmail4">Reason</label>
                                <input type="text" className="form-control" id="reason" placeholder="Reason For takeing the file"/>
                            </div>
                            <div className="form-group col">
                                <label htmlFor="inputEmail4">File</label>
                                <select className="form-control mt-1" id="fileId" style={{width: 100 + '%'}}>
                                <option value="" disabled selected>Select File </option>
                                  {this.props.files.map(file=>(
                                      <option value={file.id}>{file.name}</option>
                                  ))}
                                </select>
                            </div>
                            <div className="form-group col">
                                <label htmlFor="inputEmail4">User</label>
                                <select className="form-control mt-1" id="userId" style={{width: 100 + '%'}}>
                                <option value="" disabled selected>Select File How Take The File</option>
                                    {this.props.users.map(user=>(
                                        <option value={user.id}>{user.firstname} {user.surname}</option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" className="btn btn-primary">Save circle</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}


class RequestModal extends Component {

    approveRequest = (circle) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
               +' successfully', 'success')
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let requestBody = {
            file_id: $('#fileId2').val(),
        }
        
        fetch('api/file/request/new',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            console.log('circle Added')
        })
    }


    render() {
        return (
            <div className="modal fade" id="request-modal" tabIndex="-1" role="dialog" style={{ zIndex: 9999 }}  aria-modal="true">
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Request File</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="modal-body m-3 form"> 
                            <div className="form-group col">
                                <label htmlFor="inputEmail4">File</label>
                                <select className="form-control mt-1" id="fileId2" style={{width: 100 + '%'}}>
                                <option value="" disabled selected>Select File </option>
                                  {this.props.files.map(file=>(
                                      <option value={file.id}>{file.name}</option>
                                  ))}
                                </select>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" className="btn btn-primary">Request File</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}

class TrackModal extends Component {
    constructor(){
        super()
        this.state = {
            user:{}
        }

    }
        
    approveRequest = (circle) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
               +' successfully', 'success')
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let requestBody = {
            security_no: $('#security_no').val(),
        }
        
        fetch('api/files/track',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    user: res.holder
                })
            }
            console.log('data avilable')
        })
    }


    render() {
        return (
            <div className="modal fade" id="track-modal" tabIndex="-1" role="dialog" style={{ zIndex: 9999 }}  aria-modal="true">
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Track File</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="modal-body m-3 form"> 
                            <div className="form-group col">
                                <label htmlFor="inputEmail4">Track File</label>
                                <input type="text" className="form-control" name="security_no" id="security_no" placeholder="Enter File Tracking Id"/>
                            </div>
                            <div className="form-group col">
                                <h5>Current File Location</h5>
                                <p>User: {this.state.user.firstname} {this.state.user.surname}</p>
                                <p>department: {this.state.firstname != null?this.state.user.department.name:null}</p>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" className="btn btn-primary">Track File</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}

