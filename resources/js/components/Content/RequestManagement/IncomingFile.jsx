import React, { Component } from 'react'
import { notify } from '../../Content/Utilities'
import { authHeader} from './../../helpers/authHeader'


export default class IncomingFile extends Component {
  render() {

    const file = this.props.file
     //console.log(file)
    return (
        <div>{
            file.processed == 0 ?
            <div className="card mb-2">
            <div className="card-body p-2"> 
                <div className="row">
                    <div className="col-1">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src="img/folder.svg" alt="Placeholder" width="80" height="80" />
                        </div>
                    </div>
                    <div className="col p-1 ml-3">
                        <h5 className="card-title mb-1 mt-1 text-primary ml-1">File From: {file.fromo.firstname} {file.fromo.surname} </h5>
                        <h6 className="text-dark mb-1">
                            <span className="text-muted ml-1 small"> Status : {file.status}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            {/* <span className="text-muted ml-1 small">state : {file.circles.filter((circle)=>(circle.status === "On Progress")? 'On Progress': 'Complete')}</span> */}
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{file.created_at}</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-auto">
                        <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                data-target={"#ViewModal11" + file.id}>
                            <i className="fe fe-eye mr-2 align-midle"></i>
                            <span className="align-midle">View Info</span>
                        </button>
                        <ViewModal file={file}/>
                        <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                data-target={"#ProcessModal" + file.id}>
                            <i className="fe fe-thumbs-up mr-2 align-midle"></i>
                            <span className="align-midle">Process</span>
                        </button>
                        <ProcessModal file={file} users={this.props.users}/>
                    </div>
                </div>
            </div>
        </div>: null

        }</div>
        
    )
  }
}

class ViewModal extends Component {

    approveRequest = (file) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                this.props.file.name +' successfully', 'success')
    }

    


    render() {

        const files = this.props.file

        return (
            <div className="modal fade" id={"ViewModal11" + files.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                        <div className="col-12  d-flex">
							<div className="card flex-fill w-100">
								<div className="card-header">
									<div className="card-actions float-right">
								
									</div>
									<h2 className="card-title mb-0">File Information</h2>
								</div>
                                {
                                    files.circle.files.map(file =>(
                                        <div>
                                    <div className="p-4 bg-light">
									<h5>{file.name}</h5>
									<p className="mb-0 text-sm">Reference No:- {file.reference_no}</p>
                                    <p className="mb-0 text-sm">Access Level:- {file.access_level}</p>
                                    <p className="mb-0 text-sm">File Type:- {file.file_type}</p>
                                    <p className="mb-0 text-sm">Tracking No:- {file.Security_no}</p>
                                    <p className="mb-0 text-sm">Department:- {file.name}</p>
								</div>
                                <div className="p-4 bg-light">
                                <h4 className="card-title mb-1">File attachments</h4>
                                <pre className="snippet">
                                    {file.name} / <br/>
                                    { file.attachments.map(attachment=>( 
                                        <div>
                                            <small> {'├── '}  {attachment.name} </small>
                                        </div>
                                         ))}
                                          { file.initial_attachments.map(attachment=>( 
                                        <div>
                                            <small> {'├── '}  {attachment.name} </small>
                                        </div>
                                         ))}
                                          <br/>
                                    <h6>comments</h6>
                                    {file.comments.map(attachment=>( 
                                       <div>
                                            <small>  {attachment.comment} </small>
                                        </div> 
                                     ))}
                                </pre>
                                </div>
								
                                </div>
                                    ))
                                }
								<div className="card-body">
                                <h4 className="card-title mb-2">File Circles</h4>
                                <div>
                                    <p>Reason: {files.circle.reason}</p>
                                    <p>Start: {files.circle.start_at}</p>
                                    <p>End: {files.circle.end_at}</p>
                                    <p>Status: {files.circle.status}</p>
                                </div>
									<ul className="timeline">
                                        {/* {file.circles.map(circle=>(
                                            <li className="timeline-item">
											<strong>{circle.reason}</strong>
											<span className="float-right text-muted text-sm">{circle.created_at}</span>
											<p>{circle.status}</p>
										</li>
                                        ))} */}
									</ul>
								</div>
							</div>
						</div>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ProcessModal extends Component {
    constructor() {
        super();
    this.displayData = [];

      this.state = {
        file: '',
        showdata : this.displayData,
        attachments : []
      }
    }
    appendData1 = (e)=> {
        e.preventDefault();
       
        
        let file = new FormData();
        let reader = new FileReader();
        reader.onload = ()=> (reader.readAsDataURL(ogfile))
        reader.onloadend = (e)=> (e.target.result)
        let ogfile = $('#file11').val()
        // this.createImage(ogfile); 
        let data = {
            name: $('#attachment11').val(),
            file: ogfile
        }
        this.displayData.push(data);
        this.setState({
           showdata : this.displayData,
           attachments : [
            //   {'attachmentName' + this.attachments.length : speechSynthesis} ,
           ]
        });
     }

    approveRequest = (file) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                this.props.file.name +' successfully', 'success')
    }

    handleSubmit1 = (e) => {
        e.preventDefault();
        let requestBody = {
            status: $('#fileId11').val(),
            to: $('#userId11').val(),
            circle_id: this.props.file.circle.id,
            comment:$('#comment11').val(),
            attachments: this.displayData,
            transaction_id: this.props.file.id
        }
        
        fetch('api/file/transaction/new',{
            method: 'POST',
            headers: authHeader(), 
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            console.log('circle Added')
        })
    }


    render() {

        const file = this.props.file

        return (
            <div className="modal fade" id={"ProcessModal" + file.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Process File</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form onSubmit={this.handleSubmit1}>
                            <div className="modal-body m-3 form">
                                <div className="form-group col">
                                    <label htmlFor="inputEmail4">Status</label>
                                    <select className="form-control mt-1" id="fileId11" style={{width: 100 + '%'}}>
                                    <option value="" disabled selected>Select Status</option>
                                    <option value="Fowarded">Fowarded</option>
                                    <option value="Rejected">Rejected</option>
                                    <option value="Returned">Returned (the User Must be your Registry)</option>
                                    </select>
                                </div>
                                <div className="form-group col">
                                    <label htmlFor="inputEmail4">User</label>
                                    <select className="form-control mt-1" id="userId11" style={{width: 100 + '%'}}>
                                    <option value="" disabled selected>Select File How Take The File</option>
                                        {this.props.users.map(user=>(
                                            <option value={user.id}>{user.firstname} {user.surname}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="" className="col-form-label">comment</label>
                                    <div className="col">
                                        <textarea className="form-control" id="comment11" placeholder="Comment" rows="6"></textarea>
                                    </div>
                                </div>
                                <div id="add-attachment">
                                    <div className="form-group col">
                                        <label htmlFor="attachment">Attachment Name</label>
                                        <input type="text" className="form-control" id="attachment11" placeholder="Attachment Name"/>
                                    </div>
                                    <div className=" col">
                                        <input type="file"  id="file11"  placeholder="Attachment Name" name="file" />
                                    </div>
                                    <div className="form-group col pt-2">
                                        <button type="submit" className="btn btn-primary mt-1" 
                                            onClick={this.appendData1}>
                                            Add
                                        </button>
                                    </div>
                                    <div className="form-group col" id="display-data-Container">
                                        {this.displayData.map((data, index) => (
                                            <div key={index}>
                                                <small>Name: {data.name}</small><br/>
                                                <small>File: {data.file}</small>
                                            </div>
                                        ))}
                                    </div>
                                </div>   
                            </div>
                            
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" className="btn btn-primary" onClick={this.handleSubmit1}>Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}