import React, { Component } from 'react'
import { notify } from '../../Content/Utilities'
import {authHeader} from './../../helpers/authHeader'


export default class FileRequest extends Component {
    state = {
        user:{},
        
    }

    componentDidMount() {
      
        this.requestUserInformation()
          
      }
  
      requestUserInformation = () => {
          fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
          if(res.status == 200){
            this.setState({
                user: res.user
            })
          }
           
            console.log(this.state.user)
        })
      }

  render() {
    const request = this.props.request
    // console.log(file)
    return (
        <div className="card mb-2">
            <div className="card-body p-2"> 
                <div className="row">
                    <div className="col-1">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src="img/folder.svg" alt="Placeholder" width="80" height="80" />
                        </div>
                    </div>
                    <div className="col p-1 ml-3">
                        {
                          this.state.user.firstname != request.sender.firstname ? 
                          <h5 className="card-title mb-1 mt-1 text-primary ml-1">Request From {request.sender.firstname} {request.sender.surname} </h5> :
                          <h5 className="card-title mb-1 mt-1 text-primary ml-1">Request TO {request.receiver.firstname} {request.receiver.surname} </h5> 
                        }
                        <h6 className="text-dark mb-1">
                            <span className="text-muted ml-1 small"> File Name : {request.file.name}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            <span className="text-muted ml-1 small">state : {request.status}</span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{request.created_at}</span>
                            </span>
                        </h6>
                    </div>
                    {
                        this.state.user.firstname != request.sender.firstname && request.processed == 0 ? 
                        <div className="col-auto">
                             <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                    data-target={"#ViewModalreq" + request.id}>
                                <i className="fe fe-thumbs-up mr-2 align-midle"></i>
                                <span className="align-midle">Process</span>
                            </button>
                        <ViewRequestModal request={request} />
                        </div>:null
                    }
                    
                </div>
            </div>
        </div>
    )
  }
}


class ViewRequestModal extends Component {

    approveRequest = (file) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                this.props.file.name +' successfully', 'success')
    }

    


    approveRequest = (file) => {
        // make api request here
        notify('success', 'request has been rejected successfully '+ 
                this.props.file.name +' successfully', 'success')
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let requestBody = {
            status: $('#statusReq').val(),
            request_id: this.props.request.id
        }
        
        fetch('api/file/request/process',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            console.log('request updated')
        })
    }


    render() {

        const request = this.props.request

        return (
            <div className="modal fade" id={"ViewModalreq" + request.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Process Request</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form onSubmit={this.handleSubmit}>
                            <div className="modal-body m-3 form">
                                <div className="form-group col">
                                    <label htmlFor="inputEmail4">File</label>
                                    <select className="form-control mt-1" id="statusReq" style={{width: 100 + '%'}}>
                                    <option value="" disabled selected>Select Status</option>
                                    <option value="Fowarded">Accept</option>
                                    <option value="Rejected">Rejected</option>
                                    </select>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" className="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}