import React, { Component } from 'react'
import SideBarBaseTemplate from './SideBarBaseTemplate'
import Note from './GoodReceivedNotes/Note'


export default class AquisitionRequest extends Component {

    componentDidMount() {
        // perform fetch here
    }

  render() {

    const context = {
        heading: "Aquisition Requests",
        search_placeholder: "Search Aquisition Requests",
        setComponent: item => <Note key={item.id} aquisition_request={item} />
    }

    return (
        <React.Fragment>
            <SideBarBaseTemplate context={context} items={this.state.aquisition_requests}/>
        </React.Fragment>
    )
  }

  state = {

    // update this object with real data from the API
    aquisition_requests: [{
            id: 1,
            prepared_by: "Christopher Shoo",
            prepared_at: "9 May 2019",
            status: "status",
            number: "9973473",
        },
        {
            id: 2,
            prepared_by: "Mansoor Mansoor",
            prepared_at: "9 May 2018",
            status: "status",
            number: "4355453",
        },
        {
            id: 3,
            prepared_by: "Omakei Michael",
            prepared_at: "9 May 2017",
            status: "status",
            number: "6546454",
        },
        {
            id: 4,
            prepared_by: "Thor Odin son",
            prepared_at: "9 May 2016",
            status: "status",
            number: "1234465",
        },
        {
            id: 5,
            prepared_by: "John Wick",
            prepared_at: "9 May 2015",
            status: "status",
            number: "08976667",
        },
    ],
}
}
