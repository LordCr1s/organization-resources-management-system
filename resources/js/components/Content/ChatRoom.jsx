import React, { Component } from 'react'
import SideBarBaseTemplate from './SideBarBaseTemplate'
import Conversations from './ChatRoom/Conversation'

export default class ChatRoom extends Component {

    componentDidMount() {
        // perform fetch here
    }

  render() {

    const context = {
        heading: "Chat room",
        search_placeholder: "search conversations",
        setComponent: item => <Conversations key={item.id} conversation={item} />
    }

    return (
        <React.Fragment>
            <div className="row">
                <div className="col-12">
                    <div className="card mb-2">
                        <div className="card-body">
                            <SideBarBaseTemplate context={context} items={this.state.conversations}/>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
  }

  state = {
    conversations : [
        {id: 1, name: 'christopher shoo', image : "img/avatar.jpg", last_chat: '14 May 2019'},
        {id: 2, name: 'Michael Assey', image : "img/avatar.jpg", last_chat: '14 May 2019'},
        {id: 3, name: 'Mansoor Mansoor', image : "img/avatar.jpg", last_chat: '14 May 2019'},
        {id: 4, name: 'B-127', image : "img/avatar.jpg", last_chat: '14 May 2019'}
    ]
 }
}

