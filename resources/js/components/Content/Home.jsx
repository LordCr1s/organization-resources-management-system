import React, { Component } from 'react'
import { connect } from 'react-redux'
import LecturerMaterials from './Home/LecturerMaterials'

class Home extends Component {

  render() {
    return (
      <React.Fragment>
        <LecturerMaterials />
      </React.Fragment>
    )
  }

}

const mapStateToProps = state => ({
  user: state.user_provider.user,
  actions: state.user_provider.actions,
})

export default connect(mapStateToProps)(Home);

