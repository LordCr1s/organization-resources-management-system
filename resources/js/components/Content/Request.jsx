import React, { Component } from 'react'
import LecturerRequests from './Request/LecturerRequests'
import TechnicianRequests from './Request/TechnicianRequests'

export default class Request extends Component {
  render() {
    return (
      <React.Fragment>

        <TechnicianRequests />
        {/* <LecturerRequests /> */}
        <LecturerRequests />
      </React.Fragment>
    )
  }
}
