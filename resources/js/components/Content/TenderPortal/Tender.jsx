import React from 'react'
import {notify} from '../Utilities'

export default class Tender extends React.Component{
    formValues = {}
    state ={
        bids:[],
    }
    getBids = () => {
        fetch('api/bids')
        .then( res => res.json())
        .then( response => {
            console.log(response)
            this.setState({
                bids: response
            })
        })
    }
    getFormValues = (e) => {
        this.formValues = {
            ...this.formValues,
            [e.target.name]: e.target.value
        }
    }
   
    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }
    addBid = (e,tender) => {
        e.preventDefault()
        let body ={
            ...this.formValues,
            tender_id: tender.id,
            supplier_id: e.target.supplier_id.value
        }
        console.log(body)
        fetch('api/bid',{
            method:'POST',
            body: JSON.stringify(body),
            headers:{
                'Content-Type' : 'application/json'
            }
        })
        .then(res => res.json())
        .then( response => {
            if(response.status == 200){
                this.clearInputs()
                this.props.actions.getTenders()
                notify('success','Offer posted','success')
            }
            else{
                notify('success','Offer posted','success')
            }
        })
    }
    render(){
        const {tender} = this.props
        const {bids} = this.state
        
        return(
            <div className="card" style={{fontFamily: 'cerebrisans-regular'}}>
                <div className="card-header">
                    <div className="row">
                        <div className="col-3">
                            <span className="text-muted">{tender.id}</span>                    
                        </div>
                        <div className="col-9">
                            <span className="float-right text-muted">
                            <i className="mr-1 fe fe-clock text-warning"></i>
                            {tender.created_at}</span>
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    <h6 className="text-muted mb-1 ml-3">
                        <span className="text-dark" >{tender.name}</span>
                    </h6>
                    <h6 className="text-muted mb-1 ml-3">
                        <span className="fe fe-user text-warning"></span>
                        <span className="ml-3 text-muted">Start bid of: {tender.price} by {tender.user}</span>
                    </h6>
                    <h6 className="text-muted mb-1 ml-3">
                        <span className="fe fe-user text-warning"></span>
                        <span className="ml-3 text-muted">Status: {tender.status}</span>
                    </h6>
                    <h6 className="text-muted mb-1 ml-3">
                        <span className="fe fe-dollar-sign text-warning"></span>
                        <span className="ml-3 text-muted">Payment owing: $175,760.99 </span>
                    </h6>
                    <h6 className="text-muted mb-1 ml-3">
                        <a href="" data-toggle="modal" className="" data-target={"#seeMoreModal"+ tender.id}>
                            <span className="fe fe-eye text-primary"></span>
                            <span className="ml-3 text-underline">See more</span>
                        </a>
                        <SeeMoreModal key={tender.id} tender={tender}/>
                    </h6>

                    <h6 className="mb-1 ml-3">
                        <a href="#" data-toggle="collapse" data-target={"#collapseOne"+tender.id} aria-expanded="true" aria-controls="collapseOne">
                            <span className="fe fe-dollar-sign text-primary"></span>
                            <span className="ml-3 text-primary text-underline">Bids</span>
                        </a>
                    </h6>
                    <div className="card" id='accordionExample' style={{ fontFamily: 'cerebrisans-regular'}}>
                        <div id={"collapseOne"+tender.id} className="collapse" aria-labelledby="headingOne" aria-expanded='true'>
                            <div className="card-body">
                                {
                                    tender.bids.length != 0 ?

                                    tender.bids.map((bid) => 
                                    <div key={bid.id} className='card'>
                                        <div className="card-body">
                                        <React.Fragment>
                                            <div className="row">
                                                <div className="col-auto">
                                                    {/* <span className='text-primary h4'> {bid.supplier.name}</span> */}
                                                    <span className="text-primary h4 font-weight-2">{"Supplier Name"}</span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col">
                                                    <span className="text-muted">Offer made : {bid.price}</span>
                                                    <pre className='text-muted'>
                                                        {bid.description}
                                                    </pre>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    
                                        </div>
                                    </div>
                                )
                                    : <span className="text-muted h5">There are no bids yet</span>
                                }
                            </div>
                            <form onSubmit={(e) => this.addBid(e,tender)}>
                                <div className="row mb-2">
                                    <div className="col-9">
                                        <textarea className="form-control " placeholder="Bid Description"
                                        name='description' col='30' row='10' style={{resize:"none"}}
                                        onChange={(e) => this.getFormValues(e)}></textarea>
                                        <span className="font-13 text-muted" >include description for each</span>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-9">
                                        <input type="text" className="form-control mb-2" placeholder="Bid offer | price" name='price' onChange={(e) => this.getFormValues(e)} autoComplete='false'/>
                                        <input type="hidden" defaultValue={2} className="form-control" name='supplier_id'/>
                                    </div>
                                    <div className="col-2">
                                        <button className="btn btn-round btn-outline-secondary" type='submit'>
                                            <i className="fe fe-dollar-sign"></i> Bid
                                        </button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    
}

class SeeMoreModal extends React.Component{
    render(){
        const {tender} = this.props
        return(
        <div className="modal fade" id={"seeMoreModal" + tender.id} tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title"> Information for {tender.name +" "+ tender.id}</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body m-3">
                        <div className="row">
                            <div className="col">
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="text-dark" >{tender.name}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="fe fe-warning text-warning"></span>
                                    <span className="ml-3 text-muted">Status: {tender.status}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="fe fe-user text-warning"></span>
                                    <span className="ml-3 text-muted">Start bid of: {tender.price} </span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="fe fe-edit text-warning"></span>
                                    <span className="ml-3 text-muted">Brief description: {tender.description}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="fe fe-clock text-warning"></span>
                                    <span className="ml-3 text-muted">Offer created at: {tender.created_at}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="fe fe-clock text-warning"></span>
                                    <span className="ml-3 text-muted">Offer expires at: {tender.deadline}</span>
                                </h6>
                                <h6 className="text-muted mb-1 ml-3">
                                    <span className="ml-3 text-muted">Requested materials :</span>
                                    <pre className=' ml-3 text-dark p-1 h5' style={{fontFamily: 'cerebrisans-regular'}}> 
                                        {tender.description}
                                    </pre>
                                </h6>

                            </div>

                        </div>
                        <div className="mt-3">
                            <button type="button" className="btn btn-outline-secondary mr-3 float-right" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
}