import React, { Component } from 'react'

export default class Completed extends Component {
  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ border: 'none', borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>Possesed Material</td>
                        <td>Quantity</td>
                        <td>Requested on</td>
                        <td>Completed on</td>
                        <td>Status</td>
                    </tr>
                    {this.props.requests.map(request => (
                        <tr key={request.id}>
                            <td className="text-muted">{request.material}</td>
                            <td className="text-muted">{request.quantity}</td>
                            <td className="text-muted">{request.created_at}</td>
                            <td className="text-muted">{request.completed_at}</td>
                            {(request.status == 'accepted') ? 
                                    <td className="text-primary">{request.status}</td> :
                                    <td className="text-danger">{request.status}</td>
                                }
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
  }
}
