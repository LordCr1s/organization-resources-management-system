import React, { Component } from 'react'
import { notify } from '../../Utilities'

export default class OnPossesion extends Component {

    requestForReturn = request => {

        fetch('/api/store_department/onpossession', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => console.log(response.json())).catch(error => console.log(error))
        // make api request here
        this.props.updatePossesedMaterialRequests(request)
        notify('success', 'request for returning '+ 
                request.material +' has been sent successfully', 'success')
    }


  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ border: 'none', borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>Possesed Material</td>
                        <td>Quantity</td>
                        <td>Requested on</td>
                        <td>Verification Code</td>
                        <td>Request for return</td>
                    </tr>
                    {this.props.requests.map(request => (
                        <tr key={request.id}>
                            <td className="text-muted">{request.material}</td>
                            <td className="text-muted">{request.quantity}</td>
                            <td className="text-muted">{request.created_at}</td>
                            <td className="text-danger">{request.verification_code}</td>
                            <td className="text-muted">
                                <button className="btn btn-outline-primary" 
                                    onClick={() => this.requestForReturn(request)}>
                                    <i className="fe fe-send mr-1 align-midle"></i>
                                    <span className="align-midle">return</span>
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
  }
}
