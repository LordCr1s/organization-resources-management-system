import React, { Component } from 'react'
import { notify } from '../../Utilities'

export default class PendingContainer extends Component {


    updatePendingRequests = request => {
        this.props.updatePendingRequests(request)
        notify('success', 'request for'+ request_for_cancel.material +' has been canceled', 'success')
    }

    verifyReturningMaterial = (request) => {
        const verification_code = $('#verification_code' + request.id)
        if (verification_code == "") {
            notify('Failed', 'request could not be sent, quantity set is too low', 'error')
        } else {
            // make api request with the code here and remove this line after adding fetch api
            notify('success', 'request for'+ request.material +' has been verified', 'success')
        }
        this.props.updatePendingRequests(request)
    }

  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ border: 'none', borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>Requested Material</td>
                        <td>Quantity</td>
                        <td>Requested on</td>
                        <td>Request stage</td>
                        <td>Verification Code</td>
                        <td>Cancel request</td>
                    </tr>
                    {this.props.requests.map(request => (
                        <tr key={request.id}>
                            <td className="text-muted" style={{width: 20 + '%'}}>{request.material}</td>
                            <td className="text-muted" style={{width: 5 + '%'}}>{request.quantity}</td>
                            <td className="text-muted" style={{width: 16 + '%'}}>{request.created_at}</td>
                            
                            {   (request.request_stage == 1) ? 
                                    <React.Fragment>
                                        <td className="text-primary" style={{width: 17 + '%'}}>requesting material</td>
                                        <td className="text-muted" style={{width: 16 + '%'}}>none</td>
                                    </React.Fragment> : 
                                (request.request_stage == 2) ?
                                    <React.Fragment>
                                        <td className="text-secondary" style={{width: 17 + '%'}}>receiving material</td>
                                        <td className="text-primary" style={{width: 16 + '%'}}>{request.verification_code}</td>
                                    </React.Fragment> :
                                (request.request_stage == 3) ?
                                    <React.Fragment>
                                        <td className="text-warning" style={{width: 17 + '%'}}>return material</td>
                                        <td className="text-muted" style={{width: 16 + '%'}}>none</td>
                                    </React.Fragment> :
                                (request.request_stage == 4) ?
                                    <React.Fragment>
                                        <td className="text-info" style={{width: 17 + '%'}}>returning material</td>
                                        <td className="text-primary" style={{width: 16 + '%'}}>
                                            <div style={{display: 'flex', flexDirection: 'row', width: 'auto'}}>
                                                <input type="text" className="form-control" style={{width: 50 + '%'}}
                                                    id={"verification_code" + request.id} placeholder="code" />
                                                <button className="btn btn-sm btn-primary ml-2" onClick={() => this.verifyReturningMaterial(request)}>
                                                    <i className="fe fe-check mr-1 align-midle"></i>
                                                    <span className="align-midle">verify</span>
                                                </button>
                                            </div>
                                        </td>
                                    </React.Fragment> : <React.Fragment></React.Fragment>
                            }
                            <td className="text-muted">
                                <button className="btn btn-outline-danger" data-toggle="modal" 
                                    data-target={"#verificationModal" + request.id}>
                                    <i className="fe fe-x-circle mr-1 align-midle"></i>
                                    <span className="align-midle">cancel</span>
                                </button>
                                <VerificationModal request={request} updatePendingRequests={this.updatePendingRequests}/>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
  }
}


class VerificationModal extends Component {

    cancelRequest = (request) => {
        // make api request here
        this.props.updatePendingRequests(request)
    }

    render() {

        const request = this.props.request

        return (
            <div className="modal fade" id={"verificationModal" + request.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-sm" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <p>Are you sure you want to cancel your equest for {request.material}?</p>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">No</button>
                                <button type="button" className="btn btn-danger" data-dismiss="modal"
                                    onClick={() => this.cancelRequest(request)}>
                                    Yes
                            </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}