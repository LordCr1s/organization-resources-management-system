import React, { Component } from 'react'
import PendingContainer from './PendingContainer'
import OnPossesion from './OnPossesion'
import Completed from './Completed'

export default class RequestsContainer extends Component {

    updateRequestsList = request_for_cancel => {
        this.setState({
            ...this.state,
            pending_requests: this.state.pending_requests.filter(
                request => request.id != request_for_cancel.id
            )
        })
    }

  render() {
    return (
        <div className="row">
            <div className="col-12">
                <div className="card mb-2">
                    <div className="card-header">
                        <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" data-toggle="tab" 
                                    href="#tab-4">
                                    <i className="align-middle fe fe-loader"></i>
                                    <span className="align-middle ml-2">Pending</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href="#tab-5">
                                    <i className="align-middle fe fe-pocket"></i>
                                    <span className="align-middle ml-2">On possesion</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href="#tab-6">
                                    <i className="align-middle fe fe-check-circle"></i>
                                    <span className="align-middle ml-2">Completed</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="card-body">
                        <div className="tab-content">
                            <div className="tab-pane fade show active" id="tab-4" role="tabpanel">
                                <PendingContainer requests={this.state.pending_requests}
                                                updatePendingRequests={this.updateRequestsList}/>
                            </div>
                            <div className="tab-pane fade" id="tab-5" role="tabpanel">
                                <OnPossesion requests={this.state.pending_requests}
                                        updatePossesedMaterialRequests={this.updateRequestsList}
                                    />
                            </div>
                            <div className="tab-pane fade" id="tab-6" role="tabpanel">
                                <Completed requests={this.state.completed_requests}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }

  state = {
    pending_requests: [
        {id: 1, user: "Christopher Shoo", material: 'Switch', request_stage: 1, quantity: '03', created_at: '07 April 2019, 13:30', verification_code: 3456},
        {id: 2, user: "Christopher Shoo", material: 'Projector', request_stage: 2, quantity: '03', created_at: '07 April 2019, 13:30', verification_code: 3456},
        {id: 3, user: "Christopher Shoo", material: 'Cable Tester', request_stage: 3, quantity: '03', created_at: '07 April 2019, 13:30', verification_code: 3456},
        {id: 4, user: "Christopher Shoo", material: 'Crimping Tool', request_stage: 4, quantity: '03', created_at: '07 April 2019, 13:30', verification_code: 3456},
        {id: 5, user: "Christopher Shoo", material: 'RJ 45 connectors', request_stage: 1, quantity: '03', created_at: '07 April 2019, 13:30', verification_code: 3456},
    ],
    completed_requests: [
        {id: 1, user: "Christopher Shoo", material: 'Switch', quantity: '03', created_at: '07 April 2019, 13:30', completed_at: '07 April 2019, 13:30', status: "accepted"},
        {id: 2, user: "Christopher Shoo", material: 'Switch', quantity: '03', created_at: '07 April 2019, 13:30', completed_at: '07 April 2019, 13:30', status: "rejected"},
        {id: 3, user: "Christopher Shoo", material: 'Switch', quantity: '03', created_at: '07 April 2019, 13:30', completed_at: '07 April 2019, 13:30', status: "accepted"},
        {id: 4, user: "Christopher Shoo", material: 'Switch', quantity: '03', created_at: '07 April 2019, 13:30', completed_at: '07 April 2019, 13:30', status: "accepted"},
        {id: 5, user: "Christopher Shoo", material: 'Switch', quantity: '03', created_at: '07 April 2019, 13:30', completed_at: '07 April 2019, 13:30', status: "rejected"},
    ]
  }
}
