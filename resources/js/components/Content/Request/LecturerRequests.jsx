import React, { Component } from 'react'
import RequestsContainer from './Lecturer/RequestsContainer'

export default class LecturerRequests extends Component {

  render() {
    return (
        <React.Fragment>
            <RequestsContainer />
        </React.Fragment>
    )
  }
}
