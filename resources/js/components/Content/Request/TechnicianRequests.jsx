import React, { Component } from 'react'
import RequestsContainer from './Technician/RequestsContainer'

export default class TechnicianRequests extends Component {
  render() {
    return (
        <React.Fragment>
            <RequestsContainer />
        </React.Fragment>
    )
  }
}
