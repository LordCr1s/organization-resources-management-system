import React, { Component } from 'react'

export default class ProfileSettings extends Component {

    saveNewInfo = () => {
        let requestBody = {
            first_name: $('#profile_image').val(), 
            first_name: $('#first_name').val(),
            first_name: $('#midle_name').val(),
            first_name: $('#last_name').val(),
            first_name: $('#mobile_number').val(),
        }

        // make post request with the body here
    }

  render() {

    const user = this.props.user
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-header">
                    <h5 className="card-title text-dark">Profile settings</h5>
                </div>
                <div className="card-body">
                    <form>
                        <div className="form-group row">
                            <label className="col-form-label col-sm-2 text-sm-right">Profile image</label>
                            <div className="col-sm-6">
                                <input type="file" className="form-control" id="profile_image" style={{border: "none"}} 
                                name="validation-required" placeholder="first name"/>
                                
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-form-label col-sm-2 text-sm-right">First name</label>
                            <div className="col-sm-6">
                                <input type="text" className="form-control" id="first_name" 
                                name="validation-required" placeholder="first name" defaultValue={user.first_name}/>
                                
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-form-label col-sm-2 text-sm-right">Middle name</label>
                            <div className="col-sm-6">
                                <input type="text" className="form-control" id="middle_name" 
                                name="validation-required" placeholder="middle name" defaultValue={user.middle_name}/>
                                
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-form-label col-sm-2 text-sm-right">Last name</label>
                            <div className="col-sm-6">
                                <input type="text" className="form-control" id="last_name" 
                                name="validation-required" placeholder="last name" defaultValue={user.last_name}/>
                                
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-form-label col-sm-2 text-sm-right">Mobile Phone</label>
                            <div className="col-sm-6">
                                <input type="text" className="form-control" id="mobile_phone" 
                                name="validation-required" placeholder="mobile phone" defaultValue={user.mobile_phone}/>
                                
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-10 ml-sm-auto">
                                <button type="submit" className="btn btn-primary" onClick={event => this.saveNewInfo(event)}>Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
  }
}
