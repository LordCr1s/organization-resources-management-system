import React, { Component } from 'react'

export default class ProfileInfo extends Component {
  render() {

    const user = this.props.user
    return (
        <div className="card mb-2">
            <div className="card-body p-2">
                <div className="row"> 
                    <div className="col-auto">
                        <div className="text-left">
                            <img className="rounded mt-2 ml-2" src='img/dit_logo.jpg' alt="Placeholder" width="170" height="170"/>
                        </div>
                    </div>
                    <div className="col-8 p-1 ml-2">
                        <h2 className=" mb-1 mt-1 text-primary"> 
                            {user.firstname} {user.lastname} {user.surname}
                         </h2>
                        <h6 className="text-dark mb-1"> 
                            <span className="text-muted ml-1">Dar es salaam Institute of Technology</span> 
                        </h6>
                        <h6 className="text-muted mb-1 ml-1"> 
                            <span className="fe fe-phone-call text-secondary">
                                <span className="ml-2 text-dark" style={{fontFamily: 'cerebrisans-regular'}}>
                                    (+255) 738 223 987
                                </span>
                            </span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1"> 
                            <span className="fe fe-mail text-secondary">
                                <span className="ml-2 text-dark" style={{fontFamily: 'cerebrisans-regular'}}>
                                    {user.email}
                                </span>
                            </span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1"> 
                        <span className="fe fe-target text-secondary">
                            <span className="ml-2 text-dark" style={{fontFamily: 'cerebrisans-regular'}}>
                                {user.status}
                            </span>
                        </span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1"> 
                            <span className="fe fe-award text-secondary">
                                <span className="ml-2 text-dark" style={{fontFamily: 'cerebrisans-regular'}}>2019</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-auto">
                        {/* <div><span className="text-muted"> Profile Completed </span> </div>
                        <div data-label="40%" className="doughnut mt-3 ml-3 doughnut-success doughnut-40"></div> */}
                    </div>
                </div>
            </div>
        </div>
    )
  }
}
