import React,{Component} from 'react'
import Material from './PMUmaterials/Material';
import SideBarBaseTemplate from './SideBarBaseTemplate';
import { notify } from './Utilities';
import Axios from 'axios';


export default class PMUmaterials extends Component {


    componentDidMount() {
        this.getMaterials()
        this.getStores()
        this.getGrn()
        this.getDepartments()

    }

    getStores = () => {
        fetch('api/stores',{
            method: 'GET'
        })
        .then( res => res.json())
        .then( res => {
            this.setState({
                stores: res
            })
        })
    }
   
    getMaterials = async () => {
        await 
        fetch(`api/equipments`,{
            method: 'GET'
        })
        .then( res => res.json())
        .then( res => {
            this.setState({
                isFetching: false,
                materials: res
            })
        })
    }
    getGrn = () => {
        fetch(`api/grns`,{
            method: 'GET',
        })
        .then( res => res.json())
        .then( res => {
            this.setState({
                receivedNotes: res
            })
        })
    }

    getDepartments = async () => {
        fetch('api/departments',{
            method: 'GET',
        }).then( res => res.json())
        .then( response => {
            this.setState({
                departments: response
            })
        })
    }


  render() {
    const {materials,stores,receivedNotes,isFetching,departments} = this.state
    const actions = { 
        getMaterials :this.getMaterials,
        getStores: this.getStores,
        getGrn: this.getGrn,
        getDepartments: this.getDepartments,
    }
    const formData = {
        departments: departments,
        stores: stores,
        receivedNotes: receivedNotes
    }
    const context = {
        heading: "Store materials",
        search_placeholder: "Store materials",
        isFetching: isFetching,
        setComponent: item => <Material key={item.id} material={item} formData={formData} actions={actions}/>,
    }

    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <div className="col-auto mr-3">
                        <button className="btn btn-secondary mb-3" data-toggle="modal" 
                                data-target={"#addmaterialmodal"}>
                            <i className="fe fe-plus mr-2 align-midle"></i>
                            <span className="align-midle">add material</span>
                        </button>
                        <AddMaterialModal formData={formData} actions={actions}/>
                    </div>
                </div>
                <div className="row">
                    <SideBarBaseTemplate context={context} items={materials}/>
                </div>
            </div>
        </React.Fragment>
    )
  }

  state = {
      isFetching: true,
      materials: [
        //        {
        //         id: 1,
        //         name: "Projector",
        //         image: "img/projector.jpg",
        //         available: "6",
        //         unavailable: "0",
        //         category: "Multimedia",
        //         department: "Computer Department"
        //     },
        ],
        departments:[],
        stores:[],
        receivedNotes:[],
    }
}

export class AddMaterialModal extends React.Component {

    formValues = {}
    formData = new FormData()
    // getFormValues = (e) => {
    //     this.formValues ={
    //         ...this.formValues,//unpacking the existing contents
    //         [e.target.name] : e.target.value
    //     }
    // }
    // getFileValue = (e) => {
    //     console.log(e.target.files[0])
    //     this.formValues ={
    //         ...this.formValues,//unpacking the existing contents
    //         'image_path': e.target.files[0]
    //     }
    //     $('.fileInfo:visible').text(e.target.value)
        
    // }
    getFormValues = (e) =>{
        this.formData.append([e.target.name],e.target.value)
        console.log(this.formData)
    }
    getFileValue = (e) => {
        if(e.target.files.length > 0){
            e.target.files.forEach( (file) =>
                this.formData.append([e.target.name], file, file.name)
            )
        }
        console.log('File Values : ',this.formData)
        $('.fileInfo:visible').text(this.formData)
    }
    
    addMaterial = (e) => {
        e.preventDefault()
        let body = {
            ...this.formValues
        }
        console.log(this.formData)
        // fetch('api/equipment',{
        //     // body: JSON.stringify(body),
        //     body: this.formData,
        //     method: 'POST',
        //     headers:{
        //         // 'Content-Type': 'application/json'
        //         "Content-Type":"application/x-www-form-urlencoded",
        //     }
        // })
        Axios.post('api/equipment',this.formData,{
            "Content-Type":"application/x-www-form-urlencoded"
        })
        .then((response) => {
                if (response.status == 200) {
                    this.clearInputs()
                    $('#closeBtn').trigger('click')
                    this.props.actions.getMaterials()
                    notify('success', ' material added successful ' , 'success')
                    
                }
                else {
                    $('#closeBtn').trigger('click')
                    notify('Failed', 'request could not be updated', 'error')
                }
            },
            (error) => (console.error(error))
        )
    }

    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        $('.fileInfo:visible').val('')
        this.formValues = {}
    }
    fileInputLabel={
        position: 'relative',
        top: 0,
        left: 0,
        textAlign: 'center',
        cursor: 'pointer',
        background: '#fff',
        color: '#47bac1',
        width: '48%',
        padding: '3px',
        border:'thin solid #47bac1',
        borderRadius: '10px',
    }
    fileInput ={
        ...this.fileInputLabel,
        zIndex: '-1',
        opacity: '0'
    }
    

    render() {
        
        const {receivedNotes,stores,departments} = this.props.formData
        return (
            <div className="modal fade" id={"addmaterialmodal"} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"> Add new Material </h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id='closeBtn'>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body m-3">
                            <div className="tab ">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item ">
                                        <a className="nav-link active" href="#tab-1" data-toggle="tab" onClick={() => this.clearInputs()} role="tab">Add</a></li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#tab-2" data-toggle="tab" onClick={() => this.clearInputs()} role="tab">Add From File</a></li>
                                </ul>
                                <div className="tab-content">
                                    <div className="tab-pane active" id="tab-1" role="tabpanel">
                                        <h4 className="tab-title">Default Add</h4>
                                        <form onSubmit={(e) => this.addMaterial(e)} encType='multipart/form-data'>
                                            <div className="form-group">
                                                <label className="form-label">Equipment Name</label>
                                                <input type="text" name="name" className="form-control" onChange={(e) => this.getFormValues(e)}/>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label">Store</label>
                                                <select name="store_id" id='store_id' className="form-control select2" data-toggle="select2" 
                                                    onChange={(e) => this.getFormValues(e)}>
                                                    <option defaultValue>~~department stores~~</option>
                                                    {stores.map( (store) => 
                                                        <option key={store.id} value={store.id}>{store.name +" of "+ store.department.name}</option>
                                                        
                                                    )}
                                                </select>
                                                <input type="hidden" name="apiUrl" defaultValue="api/equipment"/>
                                            </div>
                                           
                                            <div className="form-group">
                                                <label className="form-label">Good Received Note</label>
                                                <select name="grn_id" className="form-control select2" 
                                                    data-toggle="select2" onChange={(e) => this.getFormValues(e)}>
                                                    <option defaultValue>~~Equipment Received Note~~</option>
                                                    {receivedNotes.map( (grn) => (
                                                        <option value={grn.id} key={grn.id}>{grn.grn_no +" of "+ grn.created_at }</option>
                                                    ))}
                                                </select>
                                            </div>
                                            
                                            <div className="form-group">
                                                <div className="row">
                                                    <div className="col-7">
                                                        <label className="form-label">Equipment image</label>
                                                        <label className="badge badge-sm badge-warning ml-4">Optional</label>
                                                        <label htmlFor='file' style={this.fileInputLabel}>SELECT AN IMAGE</label>
                                                        <input type="file" id='file' style={this.fileInput} name="image_path" 
                                                            onChange={(e) => this.getFileValue(e)} />
                                                        <label className="text-muted fileInfo">no file selected</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="mt-3">
                                                <button className="btn btn-primary" type="submit">
                                                    <i className="fe fe-upload mr-2 align-midle"></i>
                                                    <span className="align-midle">add</span>
                                                </button>
                                            </div>
                                        </form>                                     
                                    </div>
                                    {/* bulk add material */}
                                    <div className="tab-pane" id="tab-2" role="tabpanel">
                                        <h4 className="tab-title">Bulk add Upload Form</h4>
                                        <form onSubmit={(e) => this.addMaterial(e)}>
                                        <div className="form-group">
                                                <label className="form-label">Store</label>
                                                <select name="store_id" id='store_id' className="form-control select2" data-toggle="select2" 
                                                    onChange={(e) => this.getFormValues(e)}>
                                                    <option defaultValue>~~department stores~~</option>
                                                    {stores.map( (store) => 
                                                        <option key={store.id} value={store.id}>{store.name +" of "+ store.department.name}</option>
                                                        
                                                    )}
                                                    
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label">Good Received Note</label>
                                                <select name="grn_id" className="form-control select2" 
                                                    data-toggle="select2" onChange={(e) => this.getFormValues(e)}>
                                                    <option defaultValue>~~Equipment Received Note~~</option>
                                                    {receivedNotes.map( (grn) => (
                                                        <option value={grn.id} key={grn.id}>{grn.grn_no +" of "+ grn.created_at }</option>
                                                    ))}
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <div className="row">
                                                    <div className="col-4">
                                                        <h4>
                                                            <button className="btn btn-sm btn-pill btn-danger" disabled>PDF</button>
                                                            <button className="btn btn-sm btn-pill btn-success" disabled>EXCEL</button>
                                                            <button className="btn btn-sm btn-pill btn-primary" disabled>JSON</button>
                                                        </h4>
                                                    </div>
                                                    <div className="col-6">
                                                       
                                                        <label htmlFor='equipmentList' style={this.fileInputLabel}>SELECT FILE</label>
                                                        <input type="file" id='equipmentList' name="equipmentList" style={this.fileInput} 
                                                            onChange={(e) => this.getFileValue(e)} />
                                                        <label className="text-muted fileInfo">no file selected</label>
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                <button className="btn btn-primary" type="submit">
                                                    <i className="fe fe-upload mr-2"></i>
                                                    <span className="align-midle">add</span>
                                                </button>
                                            </div>
                                            </div>
                                        </form>
                                    
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-12">
                                        <button type="button" className="btn btn-secondary float-right"
                                            data-dismiss="modal" onClick={() => this.clearInputs()}>Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}