import React, { Component } from 'react'
import ProfileInfo from './Account/ProfileInfo'
import ProfileSettings from './Account/ProfileSettings'
import { authHeader } from "../helpers/authHeader";
import { Redirect } from 'react-router-dom'

export default class Account extends Component {

    componentDidMount() {
      
      this.requestUserInformation()
        
    }

    requestUserInformation = () => {
        fetch('api/auth/me',{
          method: 'POST',
          headers: authHeader()
      })
      .then( res => res.json())
      .then( res => {
        if(res.status == 200){
          this.setState({
              user: res.user
          })
        }else{
            this.setState({
              redirect: true
          })
        }
          console.log(this.state.user)
      })
    }

  render() {
    if (this.state.redirect) {
      return <Redirect to='/'/>;
      }
    return (
      <div className="container-fluid">
        <div className="row">
            <div className="col-12">
                <ProfileInfo user={this.state.user}/>
            </div>
        </div>
        <div className="row">
            <div className="col-12">
                {/* <ProfileSettings user={this.state.user} /> */}
            </div>
        </div>
      </div>
    )
  }

  state = {
      user : {
          first_name: 'Michael ',
          middle_name: 'Omakei',
          last_name: 'Shoo',
          profile_image: 'img/avatar.jpg',
          signature: 'img/signature.jpg',
          email: 'rishaelibenson@gmail.com',
          mobile_phone: '(+255) 738 223 987',
          organisation: 'Dar es salaam Institute of Technology'
      },
      redirect: false
  }
}
