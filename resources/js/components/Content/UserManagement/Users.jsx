import React, { Component } from 'react'
import ManagementModal from './ManagementModal'

export default class Users extends Component {
    
  render() {
    return (
        <div className="card">
            <table className="table">
                <tbody>
                    <tr style={{ border: 'none', borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                        <td>First Name</td>
                        <td>Surname</td>
                        <td>Email</td>
                        <td>Department</td>
                        <td>Created At</td>
                        <td>Status</td>
                        <td>Manage User</td>
                    </tr>
                    {
                        this.props.users.map(user =>(
                            <tr key={user.id}>
                            <td>{user.firstname}</td>
                            <td>{user.surname}</td>
                            <td>{user.email}</td>
                            <td>{user.department.name}</td>
                            <td>{user.created_at}</td>
                            <td className="text-primary">{user.status}</td>
                            <td>
                                <button className="btn btn-outline-primary" data-toggle="modal" data-target={"#managementModal" + user.id}>
                                    <i className="fe fe-toggle-right mr-2 align-midle"></i>
                                    <span className="align-midle">manage</span>
                                </button>
                                <ManagementModal roles={this.props.roles} permissions={this.props.permissions} userRole={user.roles} userId={user.id}/>
                            </td>
                        </tr>
                        ))
                    }
                   
                </tbody>
            </table>
        </div>
    )
  }
}
