import React, { Component } from 'react'
import ActivationCodeModal from './ActivationCodeModal'
import {authHeader} from '../../helpers/authHeader'

export default class ActivationCodes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            codes: [],
         
        };
    }

    componentDidMount = () => {
        fetch('/api/users/activationc_codes', {
            method: 'GET',
            headers: authHeader()
        })
        .then((resp) => resp.json())
        .then((data) => {
            this.setState({
                codes: data.codes,
            })
            // console.log(JSON.stringify(data))
        })
        .catch(error => console.log(error))
    }

    deleteCode = (id) => {
        fetch('/api/users/activationc_codes/delete/' + id, {
            method: 'GET',
            headers: authHeader(),
        })
        .then((res) => res.json())
        .then((res) => {
            console.log('deleted')
        })
        .catch(error => console.log(error))
    }

  render() {
    return (
        <div className="card mb-2">
            <div className="card-header">
                <h6>
                    Activation codes management
                    <button className="btn btn-secondary" style={{float: 'right'}} 
                        data-toggle="modal" data-target="#activation-code-modal">
                        <span className="fe fe-plus mr-2"></span>
                        <span>create code</span>
                    </button>
                    <ActivationCodeModal roles={this.props.roles}/>
                </h6>
            </div>
            <div className="card-body p-0 m-0">
                <table className="table">
                    <tbody>
                        <tr style={{ border: 'none', borderBottom: 'thin solid rgb(219, 225, 230)'}}>
                            <td>Activation code</td>
                            <td>created at </td>
                            <td>expired</td>
                            <td>action</td>
                        </tr>
                        {this.state.codes.map((code) =>
                            (
                            <tr key={code.id}>
                            <td>{code.activation_code}</td>
                            <td>{code.created_at}</td>
                            <td>{code.expire}</td>
                            <td>
                                <button className="btn btn-outline-danger" onClick={() => this.deleteCode(code.id)}>
                                    <i className="fe fe-trash-2 mr-2 align-midle"></i>
                                    <span className="align-midle">delete</span>
                                </button>
                            </td>
                        </tr>
                     ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
  }
}
