import React, { Component } from 'react';
import {authHeader} from '../../helpers/authHeader'

export default class ManagementModal extends Component {

    setPermissionsForRoles = role_id => {
        let is_selected = $(role_id).prop("checked")
        console.log(is_selected)
    }
    activate = ()=>{
        fetch('api/users/change/activate/' + this.props.userId,{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            console.log('activated')
        })
    }

    deactivate = ()=>{
        fetch('api/users/change/suspend/' + this.props.userId,{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            console.log('suspended')
        })
    }

    render() {
        return (
            <div className="modal fade" id={"managementModal" + this.props.userId} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <div className="card mb-0">
                                <div className="card-header">
                                    <h5 className="card-title mb-0">Manage user</h5>
                                </div>
                                <div className="card-body">
                                <h5 className="card-title mb-0">Click the button to Activate or Deactivate user</h5><br/>
                                <div className="row">
                                
                                <button className="btn btn-danger mt-2 mr-2 ml-4" style={{float: 'right'}} onClick={this.deactivate}>
                                    <span className="fe fe-plus mr-2"></span>
                                    <span>Deactivate</span>
                                </button>
                                <button className="btn btn-primary mt-2 mr-2 ml-4" style={{float: 'right'}} onClick={this.activate}>
                                    <span className="fe fe-plus mr-2"></span>
                                    <span>Activate</span>
                                </button>
                                </div>
                                <hr/>
                                <h5 className="card-title mb-0">Mark or Unmark the checkbox to change user role</h5>
                                <hr/>
                                    <div className="row">
                                        <div className="col-5">
                                            <h5 className="card-title mb-3">User Roles</h5>
                                            {/* {
                                                this.props.roles.map(role => (
                                                    this.props.userRole.forEach(element => {
                                                        if (element.name == role.name)
                                                            return (
                                                                <label key={role.id} className="custom-control custom-checkbox">
                                                                    <input type="checkbox" className="custom-control-input" value={role.id} defaultChecked/>
                                                                    <span className="custom-control-label">{role.name} -> {role.description}</span>
                                                                </label>
                                                            )
                                                        else
                                                            return (
                                                                <label key={role.id} className="custom-control custom-checkbox">
                                                                    <input type="checkbox" className="custom-control-input" value={role.id} />
                                                                    <span className="custom-control-label">{role.name} -> {role.description}</span>
                                                                </label>
                                                            )
                                                    })
                                                    

                                                ))
                                            } */}
                                            {this.props.roles.map( role => (
                                                
                                                this.props.userRole.forEach(userRole => (
                                                  userRole.name == role.name ? 
                                                  <label key={role.id} className="custom-control custom-checkbox">
                                                  <input type="checkbox" className="custom-control-input" value={role.id} defaultChecked/>
                                                  <span className="custom-control-label">{role.name} -> {role.description}</span>
                                              </label> :
                                              <label key={role.id} className="custom-control custom-checkbox">
                                                  <input type="checkbox" className="custom-control-input" value={role.id} />
                                                  <span className="custom-control-label">{role.name} -> {role.description}</span>
                                              </label>  
                                                ))
                                           ))}
                                        </div>
                                        <div className="col-7">
                                            <h5 className="card-title mb-3"></h5>
                                            {this.props.permissions.map(permission => (
                                                <React.Fragment key={permission.id}>
                                                    <label className="custom-control custom-checkbox">
                                                        <input type="checkbox" className="custom-control-input" />
                                                        <span className="custom-control-label">
                                                            {permission.name}
                                                        </span>
                                                    </label>
                                                    <div className="ml-4 text-muted">
                                                        <ul>
                                                            {permission.descriptions.map(description => (
                                                                <li key={"decscription-" + description.length}>{description}</li>
                                                            ))}
                                                        </ul>
                                                    </div>
                                                </React.Fragment>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style={{float: 'right'}}>
                                <button type="button" className="btn btn-warning mr-3" data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}