import React, { Component } from 'react';
import {authHeader} from '../../helpers/authHeader'

export default class ActivationCodeModal extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        let role; 
        $.each($("input[name='role']:checked"), function(){ return role = $(this).val()})

        let requestBody = {
            role: role, 
        }
        
        fetch('api/users/activationc_codes/create',{
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then( res => res.json())
        .then( res => {
            console.log('activation code added')
        })
    }


    createActivationCode = () => {
        const roles_input = $('.roles')
        roles_input.map(role => console.log(roles_input))

       
    }

    render() {
        return (
            <div className="modal fade" id="activation-code-modal" 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <div className="card mb-0">
                                <div className="card-header">
                                    <h5 className="card-title mb-0">Manage user roles </h5>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-5">
                                            <h5 className="card-title mb-3">User Role (select one role only)</h5>
                                            <form onSubmit={this.handleSubmit}>
                                                {this.props.roles.map(role => (
                                                    <label key={role.id} className="custom-control custom-checkbox">
                                                        <input type="checkbox" className="custom-control-input roles" name ="role" value={role.id}/>
                                                        <span className="custom-control-label">{role.name} -> {role.description}</span>
                                                    </label>
                                                ))}
                                                 <button type="submit" className="btn btn-primary">
                                                    Create code
                                                </button>
                                            </form>
                                        </div>
                                        {/* <div className="col-7">
                                            <h5 className="card-title mb-3">User Permissions</h5>
                                            
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                            <div style={{float: 'right'}}>
                                <button type="button" className="btn btn-warning mr-3" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}