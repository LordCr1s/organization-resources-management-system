import React from 'react'
import Tender from './TenderPortal/Tender'
import SideBarBaseTemplate from './SideBarBaseTemplate';
import { notify } from './Utilities';

export default class TenderPortal extends React.Component{

    componentDidMount(){
        this.getTenders()
        this.getSuppliers()
    }
    getSuppliers = async () => {
       await fetch('api/suppliers',{
            method: 'GET'
        })
        .then( res => res.json())
        .then( response => {
            this.setState({
                suppliers: response
            })
        })
    }
    getTenders = async () => {
        await fetch('api/tenders',{
             method: 'GET'
         })
         .then( res => res.json())
         .then( response => {
             this.setState({
                 isFetching:false,
                 tenders: response
             })
         })
     }
    render(){
        const {isFetching,tenders,suppliers} = this.state
        const actions={
            getSuppliers: this.getSuppliers,
            getTenders : this.getTenders
        }
        const formData={}
        const context = {
            heading: "Tender Portal",
            search_placeholder: "Search Tenders",
            isFetching: isFetching,
            setComponent: item => <Tender key={item.id} tender={item} formData={formData} actions={actions}/>,
        }
        return(
            <div className="container">
                <div className="row">
                    <div className="col-auto mr-3">
                        <button className="btn btn-secondary mb-3" data-toggle="modal" 
                                data-target={"#addTenderModal"}>
                            <i className="fe fe-plus mr-2 align-middle"></i>
                            <span className="align-middle">Create a new tender</span>
                        </button>
                        <AddTenderModal formData={formData} actions={actions} suppliers={suppliers}/>
                    </div>
                </div>
                <div className="row">
                    <SideBarBaseTemplate context={context} items={tenders}/>
                </div>
            </div>
        )
    }
    state ={
        isFetching:true,
        tenders:[
            // {
            //     id: 2,
            //     name: "AK-47",
            //     price: '$149,900.00',
            //     user: 'Chris Shoo',
            //     status: 'Live',
            //     created_at: 'Fri,2nd 2017'
            // },
            // {
            //     id: 3,
            //     name: "B-127",
            //     price: '$300,500.00',
            //     user: 'Mike Will',
            //     status: 'Complete',
            //     created_at: 'Mon,16th 2016'
            // }
        ],
        suppliers:[],
    }
}

class AddTenderModal extends React.Component{
    formValues ={}

    addTender = (e) => {
        e.preventDefault()
        let body = {
            ...this.formvalues
        }
        console.log(body)
        fetch('api/tender',{
            method:'POST',
            body: JSON.stringify(body),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(response => {
            if(response.status == 200){
                $('#close').trigger('click')
                notify('success','Added successful','success')
                this.props.actions.getTenders()
            }else{
                $('#close').trigger('click')
                notify('Failed','Failed to add','error')
            }
        })
    }
    getValues = (e) =>{
        this.formValues ={
            ...this.formValues,
            [e.target.name] : e.target.value
        }
    }
    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }
    togglePrivateTender = (e) => {
        this.setState({
            privateTender: !this.state.privateTender
        })
    }
    toggleTenderFile = (e) => {
        this.setState({
            fromFile: !this.state.fromFile
        })
    }
    
    state ={
        privateTender: false,
        fromFile: false
    }
    fileInputLabel={
        position: 'absolute',
        top: 0,
        left: 0,
        textAlign: 'center',
        cursor: 'pointer',
        background: '#fff',
        color: '#47bac1',
        width: '48%',
        padding: '3px',
        border:'thin solid #47bac1',
        borderRadius: '10px',
    }
    fileInput ={
        ...this.fileInputLabel,
        zIndex: '-1',
        opacity: '0'
    }

    render(){
        const {privateTender,fromFile} = this.state
        const {suppliers} = this.props
        return (
            <div className="modal fade" id={"addTenderModal"} 
                    tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"> Create New Tender</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" id="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <div className="modal-body">
                                <form onSubmit={(e) => this.addTender(e)}>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender name</label>
                                                <input name="name" className="form-control" onChange={(e) => this.getValues(e)}></input>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender status :</label>
                                                <input name="status" type='text' className="form-control" onChange={(e) => this.getValues(e)}></input>
                                                <span className="font-13 text-muted">Eg :'pending', 'available', 'on-going'</span>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender deadline :</label>
                                                <input type="date" name="deadline" className="form-control select2" onChange={(e) => this.getValues(e)}></input>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Price Offered :</label>
                                                <input type="text" className="form-control " name="price"
                                                    data-mask="000,000,000,000,000.00" data-reverse="true"
                                                    autoComplete="off" maxLength="22" onChange={(e) => this.getValues(e)} />
                                                <span className="font-13 text-muted">e.g "###,000,000.00"</span>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender description :</label>
                                                <textarea name="description" cols="30" rows="5" 
                                                    style={{resize: "none"}} className="form-control" onChange={(e) => this.getValues(e)}></textarea>
                                                    <small className="form-text text-muted">describe equipment list</small>
                                            </div>

                                        </div>
                                    </div>
                                    
                                    {/* <div className="form-group">
                                        <div className="row">
                                            <div className="col-5">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" onChange={(e) => this.toggleTenderFile(e)}/>
                                                    <span className="custom-control-label">Get tender from file</span>
                                                </label>
                                                <small className="form-text text-muted">Upload a formated file list with the equipment list</small>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        fromFile ? 
                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-4">
                                                    <h4>
                                                        <label className="badge badge-sm badge-danger">PDF</label>
                                                        <label className="badge badge-sm badge-success">EXCEL</label>
                                                        <label className="badge badge-sm badge-primary">JSON</label>
                                                    </h4>
                                                </div>
                                                <div className="col-6">
                                                    <input type="hidden" name="apiUrl" defaultValue="api/equipmentList"/>
                                                    <label htmlFor='file' style={this.fileInputLabel}>SELECT A FILE</label>
                                                    <input type="file" id='file' name="equipmentList" style={this.fileInput} onChange={(e) => this.getValues(e)} />
                                                </div>
                                            </div>
                                        </div> :
                                        <React.Fragment></React.Fragment>
                                    } 
                                     <div className="form-group">
                                        <div className="row">
                                            <div className="col-5">
                                                <label className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" onChange={(e) => this.togglePrivateTender(e)}/>
                                                    <span className="custom-control-label">make tender private</span>
                                                </label>
                                                <small className="form-text text-muted">Send tender to a selected list of suppliers</small>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        privateTender ?
                                        <div className="form-group">
                                            <label className="font-weight-bold" htmlFor="">Suppliers</label>
                                            <div className="mb-3">
                                                <select className="form-control select2" data-toggle="select2" size='5' name='supplier' multiple>
                                                   {suppliers.map( supplier => <option key={supplier.id} value={supplier.id}>{supplier.name}</option>)}
                                                </select>
                                            </div>
                                        </div> :
                                        <React.Fragment></React.Fragment>
                                    } */}
                                    <button type="submit" className="btn btn-success m-2" data-dismiss="modal">Create</button>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-danger" data-dismiss="modal" onClick={() => this.clearInputs()}>Close</button>
                            </div>
                    </div>
                </div>
            </div>
        )
    }

}