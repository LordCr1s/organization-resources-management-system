import React,{Component} from 'react'

export default class QrCode extends Component{
    
    getQrCode = async () => {
        fetch('api/getQrCode/'+id,{
            method: 'GET',
            Headers:{
                'Content-Type': 'application/json'
            }
        }).then( res => res.json())
        .then(response => this.setState({
            isFetching: false,
            QrCodePath: response
        }))
        .catch(err => console.error(err))
    }

    state ={
        isFetching: true,
        QrCodePath: ''
    }
    render() {
        const {QrCodePath,isFetching} = this.state
        return(

            <React.Fragment>
            {
                isFetching ?
                <span className="loader loader-primary"></span>  :
                <img src={QrCodePath} alt="QrCodePath" width="110" height="110"/>
            }
            </React.Fragment>
        )
    }
}