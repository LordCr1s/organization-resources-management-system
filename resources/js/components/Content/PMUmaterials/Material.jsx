import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { notify } from '../Utilities'


export default class Material extends Component {

    render() {
        const material = this.props.material

        return (
            <div className="card mb-2">
                <div className="card-body p-2">
                    <div className="row">
                        <div className="col-auto">
                            <div className="text-left">
                                {/* <img className="rounded mt-2 ml-2" src={material.image} alt="Placeholder" width="110" height="110" /> */}
                                <h4 className="text-muted">{material.id}</h4>
                            </div>
                        </div>
                        <div className="col p-1">
                            <h5 className="card-title mb-1 mt-1 text-primary ml-1"> {material.name} </h5>
                            <h6 className="text-dark mb-1 ml-1">
                                <i className="fe fe-check-circle text-success"></i>
                                <span className="text-muted ml-2" style={{ fontFamily: 'cerebrisans-regular' }}>Status : Available</span>    
                                
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <i className="align-middle fe fe-download text-warning"></i>
                                <span className="text-muted ml-2">Store : {material.store.name}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-link text-warning"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Department : {material.store.department.name}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-clock text-warning"></span>
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>Date Added : {material.created_at  }</span>
                            </h6>
                            <div className="pt-1">
                                <a href="" data-toggle="modal" data-target={"#seeMoreModal" + material.id}>
                                    <span className="fe fe-eye text-primary ml-1"></span>
                                    <span className="ml-2" style={{ fontFamily: 'cerebrisans-regular', textDecoration: 'underline' }}>see more</span>
                                </a>
                                <SeeMoreModal item={material} />
                            </div>
                        </div>
                        <div className="col-auto mr-3 pt-4">
                            <button className="btn btn-outline-secondary  ml-5 mt-4" data-toggle="modal" 
                                    data-target={"#materialmodal"+material.id}>
                                <i className="fe fe-edit-3 mr-2 align-midle"></i>
                                <span className="align-midle">edit</span>
                            </button>
                            <UpdateMaterialModal 
                                key={material.id} 
                                material={material} 
                                formData={this.props.formData}
                                actions={this.props.actions}/>

                            <button className="btn btn-outline-danger  ml-3 mt-4" data-toggle="modal" 
                                    data-target={"#verificationModal" + material.id}>
                                <i className="fe fe-trash-2 mr-2 align-midle"></i>
                                <span className="align-midle">delete</span>
                            </button>
                            <VerificationModal material={material} actions={this.props.actions}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class UpdateMaterialModal extends React.Component{
    addMaterial = (e) => {
        e.preventDefault()
        const {name,store_id,grn_id} = e.target
        const id = e.target.id.value
        let body = {
            name : name.value,
            store_id : store_id.value,
            grn_id : grn_id.value
        }
        // console.log(id)
        fetch(`api/equipment/`+id,{
            body: JSON.stringify(body),
            method: 'PUT',
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then((response) => {
                if (response.status == 200) {
                    $(`#close`+id).trigger('click')
                    this.props.actions.getMaterials()
                    notify('success', ' material updated successful ' , 'success')
                    
                }
                else {
                    $(`#close`+id).trigger('click')
                    notify('Failed', 'request could not be updated', 'error')
                }
            },
            (error) => (console.error(error))
        )
    }
    render(){
        const {stores,receivedNotes} = this.props.formData
        const material = this.props.material
        return(
            <div className="modal fade" id={"materialmodal"+material.id} 
                    tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                    <div className="modal-dialog modal-sm" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                                <h4 className="modal-title"> Edit Material {material.name +" "+material.id}</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" id={"close"+material.id}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form onSubmit={(e) => this.addMaterial(e)}>
                                <div className="modal-body m-3">
                                    <div className="form-group">
                                        <label className="form-label">Equipment Name</label>
                                        <input type="text" name="name" className="form-control" id="name" defaultValue={material.name}/>
                                        <input type="hidden" name="id" defaultValue={material.id}/>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Store</label>
                                        <select name="store_id" id='store_id' className="form-control select2" data-toggle="select2">
                                            {stores.map( (store) => 
                                                <option 
                                                    key={store.id} 
                                                    value={store.id} 
                                                    selected={(store.id == material.store_id) ? true : false}>
                                                    {store.name} of {store.department.name}
                                                </option>
                                            )}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Good Received Note</label>
                                        <select name="grn_id" className="form-control select2" data-toggle="select2" id="grn_id" >
                                            {receivedNotes.map( (grn) => (
                                                <option 
                                                    key={grn.id}
                                                    value={grn.id}
                                                    selected={(grn.id == material.grn_id) ? true : false}>
                                                    {grn.grn_no +" of "+ grn.created_at }
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="mt-3">
                                        <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                                        <button className="btn btn-primary" type="submit">
                                            <i className="fe fe-upload mr-2 align-midle"></i>
                                            <span className="align-midle">Update</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                    
                        </div>
                    </div>
                </div>
            )
        }
}
class VerificationModal extends Component {

    deleteMaterial = (deletedMaterial) => {
        // perform api request here

        fetch(`api/equipment/`+deletedMaterial.id,{
            method: 'DELETE',
            // method: 'PUT'
            // body: JSON.stringify({'status': 0}),
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then( res => {
            if (res.status == 200) {
                this.props.actions.getMaterials()
                notify('success', 'deleted '+ deletedMaterial.name +' successfull', 'success')
            }else{
                notify('Failed', 'request could not be updated', 'error')
            }
        })
    }

    render() {

        const material = this.props.material

        return (
            <div className="modal fade" id={"verificationModal" + material.id} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-sm modal-center" role="document">
                    <div className="modal-content">
                        <div className="modal-body m-3">
                            <p>Are you sure you want to delete {material.name}?</p>
                            <div className="mt-3">
                                <button type="button" className="btn btn-secondary mr-3" id="closeBtn" data-dismiss="modal">No</button>
                                <button 
                                    type="button" 
                                    className="btn btn-danger"
                                    data-dismiss="modal"
                                    onClick={ () => this.deleteMaterial(material) }>
                                    Yes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class SeeMoreModal extends React.Component{
    render(){
        const {item} = this.props
        return(
        <div className="modal fade" id={"seeMoreModal" + item.id} tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
            <div className="modal-dialog modal-md" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title"> Information for {item.name +" "+ item.id}</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body m-3" style={{ fontFamily: 'cerebrisans-regular' }}>
                    <div className="row">
                        <div className="col-auto">
                            <div className="text-left">
                                {/* <h4 className="text-muted">{item.id}</h4> */}
                                <img className="rounded mt-2 ml-2" src='img/camera.jpg' alt="Placeholder" width="110" height="110" />
                            </div>
                        </div>
                        <div className="col-5 p-1">
                            <h3 className="mb-1 mt-1 text-primary ml-1"> {item.name} </h3>
                            <h6 className="text-dark mb-1 ml-1">
                                <i className="fe fe-check-circle text-success"></i>
                                <span className="text-muted ml-2" >Status : Available</span>                        
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <i className="align-middle fe fe-download text-warning"></i>
                                <span className="text-muted ml-2">Store : {item.store.name}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-link text-warning"></span>
                                <span className="ml-2 text-muted" >Department : {item.store.department.name}</span>
                            </h6>
                            <h6 className="text-muted mb-1 ml-1">
                                <span className="fe fe-clock text-warning"></span>
                                <span className="ml-2 text-muted" >Date added : {item.created_at  }</span>
                            </h6>
                            
                        </div>
                        <div className="col">
                            <img src="img/code.png" className="rounded mt-2 ml-2 float-right" alt="Placeholder" width="110" height="110" />
                        </div>
                        
                    </div>
                    <div className="row">
                        <div className="col-7 float-right">
                            <button className="btn btn-outline-secondary ml-3" 
                                data-dismiss="modal" data-toggle="modal" 
                                data-target={"#materialmodal"+item.id}>Edit
                            </button>
                            <button className="btn btn-outline-danger ml-2" 
                                data-dismiss="modal" data-toggle="modal" 
                                data-target={"#verificationModal"+item.id}>Delete
                            </button>
                        </div>
                    </div>

                        <div className="mt-3">
                            <button type="button" className="btn btn-outline-warning mr-3 float-right" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
}