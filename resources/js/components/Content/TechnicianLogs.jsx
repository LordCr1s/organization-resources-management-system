import React, { Component } from 'react'

export default class TechnicianLogs extends Component {

    searchLogs = (event) => {
        event.preventDefault()
        let searchInputText = $('#searchInput').val()
        this.setState({
            ...this.state,
            search_results: this.state.logs.filter(log => {
                if (log.action.includes(searchInputText) 
                    || log.date.includes(searchInputText))
                    return log
            }),
            user_is_searching: true
        })
    }
 
  render() {
    return (
        <div className="card mb-2">
            <div className="card-header p-3">
                <div className="row">
                    <div className="col">
                        <h3 className="text-dark ml-3">
                            Technician Activity Logs
                        </h3>
                    </div>
                    <div className="col-auto">
                        <div className="form-group mb-0">
                            <div className="input-group">
                            <input className="form-control" placeholder="search logs"
                                type="text" id="searchInput" onKeyUp={(event) => this.searchLogs(event)} />
                            <span className="input-group-prepend">
                                <button className="btn btn-secondary" type="button" 
                                    onClick={(event) => this.searchLogs(event)}>Go!
                                </button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-body">
                <table className="table">
                    <tbody>
                        <tr>
                            <td style={{border: 'none'}}>Technician</td>
                            <td style={{border: 'none'}}>Date</td>
                            <td style={{border: 'none'}}>Action</td>
                        </tr>
                            { this.state.user_is_searching ? 
                                this.state.search_results.map(log => (
                                    <tr key={log.id}>
                                        <td className="text-muted">{log.technician}</td>
                                        <td className="text-muted">{log.date}</td>
                                        <td className="text-muted">{log.action}</td>
                                    </tr>
                                )) :
                                this.state.logs.map(log => (
                                    <tr key={log.id}>
                                        <td className="text-muted">{log.technician}</td>
                                        <td className="text-muted">{log.date}</td>
                                        <td className="text-muted">{log.action}</td>
                                    </tr>
                                ))
                            }
                    </tbody>
                </table>
            </div>
        </div>
    )
  }

  state = {
      logs: [
          {id: 1, technician: 'mansoor mansoor', date: '01 May 2019', action: 'set 2 projectors unavailable'},
          {id: 2, technician: 'mansoor mansoor', date: '05 May 2019', action: 'set 2 switches unavailable'},
          {id: 3, technician: 'mansoor mansoor', date: '18 May 2019', action: 'accepted request for camera(Nikon)'},
          {id: 4, technician: 'mansoor mansoor', date: '24 May 2019', action: 'rejected request for Rj45 connectors'},
          {id: 5, technician: 'mansoor mansoor', date: '30 May 2019', action: 'announced for returning of materials'},
      ],
      search_results: [],
      user_is_searching: false
  }
}
