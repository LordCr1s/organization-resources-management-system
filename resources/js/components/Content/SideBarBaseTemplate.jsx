import React, { Component } from 'react'

export default class SideBarBaseTemplate extends React.Component {

    state = {
        search_results: [],
        user_is_searching: false
    }

    //  for now this search only works if the search attribute is named `name`
    searchItems = event => {
        event.preventDefault()
        let searchInputText = $('#searchInput').val()
        this.setState({
            ...this.state,
            search_results: this.props.items.filter(item => {
                if (item.name.toLowerCase().includes(searchInputText.toLowerCase()))
                    return item
            }),
            user_is_searching: true
        })
    }
   

  render() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    <div className="card" style={{background: 'transparent',}}>
                        <div className="card-header p-3">
                            <div className='row'>
                                <div className="col">
                                    <h4 className="card-title mb-0 mt-1 text-dark" 
                                        style={{fontWeight: 500, fontSize: 1.5 + 'em'}}> {this.props.context.heading} 
                                    </h4>
                                </div>
                                <div className="col-auto">
                                    <div className="form-group mb-0">
                                        <div className="input-group">
                                           
                                            <input className="form-control" 
                                                placeholder={this.props.context.search_placeholder}
                                                type="text" id="searchInput" 
                                                onKeyUp={(event) => this.searchItems(event)} />
                                        <span className="input-group-prepend">
                                            <button className="btn btn-secondary" type="button" 
                                                onClick={(event) => this.searchItems(event)}>Go!
                                            </button>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    {   
                        this.props.context.isFetching ?
                        <div style={{ marginLeft: 40 + "%", marginTop: 20 + "%"}} id="loaderDiv">
                            <div className="spinner-grow text-primary">
                                <span className="sr-only">Loading....</span>
                            </div>
                            <div className="spinner-grow text-secondary">
                                <span className="sr-only">Loading....</span>
                            </div>
                            <div className="spinner-grow text-warning">
                                <span className="sr-only">Loading....</span>
                            </div>
                        </div>
                        :
                        <div className="card-body p-0 pt-3" id="materialDiv" >
                            
                            <div className="container-fluid pl-0 pr-0">
                                { this.state.user_is_searching ? 
                                    this.state.search_results.map(item => this.props.context.setComponent(item)) :
                                    this.props.items.map(item => this.props.context.setComponent(item))
                                    // <React.Fragment></React.Fragment>
                                }
                            </div>
                        </div>
                    }
                    
                    
                    </div>
                </div>
            </div>
        </div>
    )
  }
}


