import React, { Component } from 'react'
import SideBarBaseTemplate from './SideBarBaseTemplate'
import Department from './Departments/Department'
import { notify} from './Utilities'


export default class Departments extends Component {

    componentDidMount() {
        // perform fetch here

        // this.getStores()
        this.getDepartments()
        this.getCategories()
    }
    getDepartments =  () => {
        fetch('api/departments',{
            method: 'GET',
        }).then( res => res.json())
        .then( response => {
            this.setState({
                isFetching: false,
                departments: response
            })
        })
    }
    getCategories = async () => {
        fetch('api/categories',{
            method: 'GET',
        }).then( res => res.json())
        .then( response => {
            this.setState({
                isFetching: false,
                categories: response
            })
        })
    }    

    getStores = () => {
        fetch('api/stores',{
            method: 'GET'
        })
        .then( res => res.json())
        .then( res => {
            this.setState({
                stores: res
            })
        })
    }
   
    
  render() {

    const {stores,isFetching,departments,categories} = this.state
    const actions = { 
        getStores: this.getStores,
        getDepartments: this.getDepartments,
        
    }
    const formData = {
        departments: departments,
        // stores: stores,
        categories: categories,
    }
    const context = {
        heading: "Departments",
        isFetching: isFetching,
        search_placeholder: "Search Department",
        setComponent: item => <Department key={item.id} department={item} actions={actions} formData={formData} />
    }

    return (
        <React.Fragment>
             <div className="row">
                    <div className="col-auto mr-3">
                        <button className="btn btn-secondary mb-3" data-toggle="modal" 
                                data-target={"#addmaterialmodal"}>
                            <i className="fe fe-plus mr-2 align-midle"></i>
                            <span className="align-midle">Add Department</span>
                        </button>
                        <AddDepartmentModal formData={formData} actions={actions}/>
                    </div>
                </div>
                <div className="row">
                    <SideBarBaseTemplate context={context} items={departments}/>
                </div>

        </React.Fragment>
    )
  }

  state = {
    // update this object with real data from the API
    isFetching: true,
    departments: [],
    categories:[],
    stores:[],
    materials:[],
}
}

export class AddDepartmentModal extends React.Component{
    formValues = {}
    getFormValues = (e) => {
        this.formValues ={
            ...this.formValues,//unpacking the existing contents
            [e.target.name] : e.target.value
        }
    }
    addDepartment = (e) => {
        e.preventDefault()
        let body = {
            ...this.formValues
        }
        console.log(body)
        fetch('api/department',{
            body: JSON.stringify(body),
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
                if (response.status == 200) {
                    this.clearInputs()
                    $('#closeBtn').trigger('click')
                    this.props.actions.getDepartments()
                    notify('success', ' material added successful ' , 'success')
                    
                }
                else {
                    $('#closeBtn').trigger('click')
                    notify('Failed', 'request could not be updated', 'error')
                }
            },
            (error) => (console.error(error))
        )
    }

    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }

    render(){
        const {categories} = this.props.formData
        return(
            <div className="modal fade" id={"addmaterialmodal"} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"> Add new Department </h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id='closeBtn'>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body m-3">
                            <form onSubmit={(e) => this.addDepartment(e)}>
                                <div className="form-group">
                                    <label className="form-label">Department Name</label>
                                    <input type="text" name="name" className="form-control" onChange={(e) => this.getFormValues(e)}/>
                                </div>
                            
                                <div className="form-group">
                                        <label className="form-label">description</label>
                                        <textarea name="description" id="" cols="30" rows="10"
                                            className="form-control" onChange={(e) => this.getFormValues(e)}></textarea>
                                        
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">category</label>
                                        <select name="category_department_id" id="" className="form-control select2" onChange={(e) => this.getFormValues(e)}>
                                            {categories.map( (category) => <option key={category.id} value={category.id}>{category.name}</option>)}
                                        </select>
                                    </div>
                                <div className="mt-3">
                                    <button className="btn btn-primary" type="submit">
                                        <i className="fe fe-upload mr-2 align-midle"></i>
                                        <span className="align-midle">add</span>
                                    </button>
                                </div>
                            </form>                                     
                        </div>
                        <div className="modal-footer">
                            <div className="row mt-3">
                                <div className="col-12">
                                    <button type="button" className="btn btn-secondary float-right"
                                        data-dismiss="modal" onClick={() => this.clearInputs()}>Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                   
        )
    }
}