import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SideBarBaseTemplate from './SideBarBaseTemplate'
import Store from './DepartmentStore/Store'
import { notify } from './Utilities'


export default class Stores extends Component {

    componentDidMount() {
        // perform fetch here
        this.getStores()
        this.getDepartments()
    }
    getStores = async () => {
        fetch('api/stores',{
            method: 'GET',
        }).then( res => res.json())
        .then( response => {
            this.setState({
                isFetching: false,
                stores: response
            })
        })
    }
    getDepartments = async () => {
        fetch('api/departments',{
            method: 'GET'
        })
        .then( res => res.json())
        .then( res => {
            this.setState({
                departments: res
            })
        })
        .catch(error => console.error(error))
    }
  render() {

    const {stores,departments,isFetching} = this.state
    const actions = {
        getDepartments: this.getDepartments,
        getStores: this.getStores
    }
    const context = {
        heading: "Stores",
        isFetching: isFetching,
        search_placeholder: "Search Stores",
        setComponent: item => <Store key={item.id} store={item} departments={departments} actions={actions}/>
    }

    return (
        <React.Fragment>
             <div className="container">
                <div className="row">
                    <div className="col-auto mr-3">
                        <button className="btn btn-secondary mb-3" data-toggle="modal" 
                                data-target={"#addmaterialmodal"}>
                            <i className="fe fe-plus mr-2 align-midle"></i>
                            <span className="align-midle">add material</span>
                        </button>
                        <AddStoreModal departments={departments} actions={actions}/>
                    </div>
                </div>
                <div className="row">
                    <SideBarBaseTemplate context={context} items={stores}/>
                </div>
            </div>
        </React.Fragment>
    )
  }

  state = {
    // update this object with real data from the API
    stores: [],
    isFetching: true,
    departments: []
    }
}


export class AddStoreModal extends React.Component{
    formValues = {}
    getFormValues = (e) => {
        this.formValues ={
            ...this.formValues,//unpacking the existing contents
            [e.target.name] : e.target.value
        }
    }
    addStore = (e) => {
        e.preventDefault()
        let body = {
            ...this.formValues
        }
        console.log(body)
        fetch('api/store',{
            body: JSON.stringify(body),
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
                if (response.status == 200) {
                    this.clearInputs()
                    $('#closeBtn').trigger('click')
                    this.props.actions.getStores()
                    notify('success', ' material added successful ' , 'success')
                    
                }
                else {
                    $('#closeBtn').trigger('click')
                    notify('Failed', 'request could not be updated', 'error')
                }
            },
            (error) => (console.error(error))
        )
    }

    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }

    render(){
        const {departments} = this.props
        return(
            <div className="modal fade" id={"addmaterialmodal"} 
                tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"> Add new Store </h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id='closeBtn'>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body m-3">
                            <form onSubmit={(e) => this.addStore(e)}>
                                <div className="form-group">
                                    <label className="form-label">Store Name</label>
                                    <input type="text" name="name" className="form-control" onChange={(e) => this.getFormValues(e)}/>
                                </div>
                            
                                <div className="form-group">
                                    <label className="form-label">description</label>
                                    <textarea name="description" id="" cols="30" rows="10"
                                        className="form-control" onChange={(e) => this.getFormValues(e)}></textarea>
                                </div>
                                <div className="form-group">
                                    <label className="form-label">Store department</label>
                                    <select name="department_id" className="form-control select2" 
                                        data-toggle="select2" onChange={(e) => this.getFormValues(e)}>
                                        <option defaultValue>~~departments~~</option>
                                        {departments.map( (department) => (
                                            <option value={department.id} 
                                                key={department.id}>{department.name +' of '+ department.category.name  }
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="mt-3">
                                    <button className="btn btn-primary" type="submit">
                                        <i className="fe fe-upload mr-2 align-midle"></i>
                                        <span className="align-midle">add</span>
                                    </button>
                                </div>
                            </form>                                     
                        </div>
                        <div className="modal-footer">
                            <div className="row mt-3">
                                <div className="col-12">
                                    <button type="button" className="btn btn-secondary float-right"
                                        data-dismiss="modal" onClick={() => this.clearInputs()}>Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                   
        )
    }
}
