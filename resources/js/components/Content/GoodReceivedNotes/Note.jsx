import React, { Component } from 'react'
import {notify} from '../Utilities'

export default class Note extends Component {
  render() {

    const note = this.props.report || this.props.aquisition_request
    const formData ={
        reportType: [
            {
                id: 1,
                type: 'Good Received Note'
            },
            {
                id: 2,
                type: 'Requisition Report'
            },
            {
                id: 3,
                type: 'Acquisition Report'
            },
            {
                id: 4,
                type: 'Others'
            },
        ]
    }
    const actions = {}

    return (
        <div className="card">
            <div className="card-body">
                <div className="row">
                    <div className="col-9 ml-1">
                        <h4 className="text-dark">Report Information :</h4>
                        <h5 className="card-title mb-1 mt-1 text-primary"> {note.received_user.name || note.verified_user.name ||  "Received user"} </h5>
                        <h6 className="text-dark mb-1">
                            <span className="text-muted ml-1 small"> serial number : {note.grn_no}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            <span className="text-muted ml-1 small">{note.status}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            {/* <span className="text-muted ml-1 small">{note.department.name || 'Department name'}</span> */}
                        </h6>
                        <h4 className="text-dark">Supplier Information :</h4>
                        <h6 className="text-muted mb-1">
                            {/* <span className="text-muted ml-1 small">{note.supplier.name || "Supplier name"}</span> */}
                        </h6>
                        <h6 className="text-muted mb-1">
                            <span className="text-muted ml-1 small">
                                <i className="fe fe-email"></i>Email :{note.supplier.email}</span>
                        </h6>
                        <h6 className="text-muted mb-1">
                            <span className="text-muted ml-1 small">
                                <i className="fe fe-phonebook"></i> Phone number :{note.supplier.phone_number}</span>
                        </h6>
                        <h6 className="text-muted mb-1 ml-1 small">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{note.created_at || note.modified_at}</span>
                            </span>
                        </h6>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-outline-primary btm-sm mt-4" data-toggle="modal" 
                                data-target={"#generateReportModal"+note.id}>
                            <i className="fe fe-plus mr-2 align-middle"></i>
                            <span className="align-middle">Create a report</span>
                        </button>
                        <GenerateReport formData={formData} actions={actions} note={note}/>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

export class GenerateReport extends Component{
    formValues ={}

    generateReport = (e) => {
        e.preventDefault()
        let body = {
            ...this.formvalues
        }
        console.log(body)
        fetch('api/createReport',{
            method:'POST',
            body: JSON.stringify(body),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(response => {
            if(response.status == 200){
                $('#close').trigger('click')
                notify('success','Added successful','success')
                // this.props.actions.getTenders()
            }else{
                $('#close').trigger('click')
                notify('Failed','Failed to add','error')
            }
        })
    }
    getValues = (e) =>{
        this.formValues ={
            ...this.formValues,
            [e.target.name] : e.target.value
        }
    }
    clearInputs =() => {
        $('input').val('')
        $('select').val('')
        this.formValues = {}
    }
    render(){
        // const {} = this.state
        const {note} = this.props
        return(
        <React.Fragment>
            <div className="modal fade" id={"generateReportModal"+note.id} 
                    tabIndex="-1" role="dialog" aria-hidden="true" style={{zIndex: 9999}}>
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"> Create New Tender</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" id="close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <div className="modal-body">
                                <form onSubmit={(e) => this.generateReport(e)}>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Report Type</label>
                                                <select name="type" id="type" className='form-control'>
                                                    <option defaultValue=''></option>
                                                </select>
                                                
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender status :</label>
                                                <input name="status" type='text' className="form-control" onChange={(e) => this.getValues(e)}></input>
                                                <span className="font-13 text-muted">Eg :'pending', 'available', 'on-going'</span>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender deadline :</label>
                                                <input type="date" name="deadline" className="form-control select2" onChange={(e) => this.getValues(e)}></input>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Price Offered :</label>
                                                <input type="text" className="form-control " name="price"
                                                    data-mask="000,000,000,000,000.00" data-reverse="true"
                                                    autoComplete="off" maxLength="22" onChange={(e) => this.getValues(e)} />
                                                <span className="font-13 text-muted">e.g "###,000,000.00"</span>
                                            </div>
                                            <div className="form-group mt-2">
                                                <label className="font-weight-bold">Tender description :</label>
                                                <textarea name="description" cols="30" rows="5" 
                                                    style={{resize: "none"}} className="form-control" onChange={(e) => this.getValues(e)}>
                                                </textarea>
                                                <small className="form-text text-muted">describe equipment list</small>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-success m-2" data-dismiss="modal">Create</button>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-danger" data-dismiss="modal" onClick={() => this.clearInputs()}>Close</button>
                            </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
        )
    }
}
