import React, { Component } from 'react'
import RequestManagerRoot from './RequestManagement/RequestManagerRoot'

export default class RequestManagement extends Component {
  render() {
    return (
      <React.Fragment>
        <RequestManagerRoot />
      </React.Fragment>
    )
  }
}
