import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class CoversationContainer extends Component {

    state = {
        conversation: {id: 1, name: 'christopher shoo', image : "img/avatar.jpg", last_chat: '09:37 14 May 2019'},
        texts: []
    }

    addText = (content) => {

        let new_texts = [...this.state.texts]
        new_texts.push({id: 1 + new_texts.length, content: content})

        this.setState({
            ...this.state,
            texts: new_texts
        })
    }

  render() {
    return (
      <div className="container">
        <div className="row">
            <div className="col-12">
                <Conversation conversation={this.state.conversation} texts={this.state.texts} addText={this.addText}/>
            </div>
        </div>
      </div>
    )
  }
}

class Conversation extends Component {

    sendText = () => {
        this.props.addText($('#text-area').val())
        $('#text-area').val("")
    }

  render() {

    const conversation = this.props.conversation

    return (
        <div className="card mb-2">
            <div className="card-header p-2">
                <div className="row">
                    <div className="col-auto">
                        <div className="text-left">
                            <img className="avatar img-fluid rounded-circle mt-2 ml-2" src={conversation.image} alt="Placeholder" width="42" height="42" />
                        </div>
                    </div>
                    <div className="col-auto p-1">
                        <Link to='/conversation'>
                            <span className="align-middle">{conversation.name}</span>
                        </Link>                        
                        <h6 className="text-muted mb-1 ml-1">
                            <span className="fe fe-watch text-warning">
                                <span className="ml-2 text-muted" style={{ fontFamily: 'cerebrisans-regular' }}>{conversation.last_chat}</span>
                            </span>
                        </h6>
                        
                    </div>
                </div>
            </div>
            <div className="card-body" style={{background: '#eaeaea', height: '65vh', overflowY: 'scroll'}}>
                { this.props.texts.map(text => 
                        <div className="row" key={text.id}>
                            <div className="col-4">
                                <div className="card mb-1" style={{ width: 'auto', borderRadius: '20px', background: '#0cc2aa'}}>
                                    <div className="card-body text-white" style={{padding: .9 + 'rem'}}>
                                        {text.content}<br />
                                        <span className="fe fe-clock small">
                                            <span className="ml-2 text-white" style={{ fontFamily: 'cerebrisans-regular', fontSize: '76%' }}>
                                                {conversation.last_chat}
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
            </div>
            <div className="card-footer">
                <div className="form-group mb-0">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="type text here" id="text-area"/>
                        <span className="input-group-append">
                            <button className="btn btn-secondary" type="button" onClick={() => this.sendText()}>send</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        )
  }
}
