import React, { Component } from 'react'
import Users from './UserManagement/Users'
import ActivationCodes from './UserManagement/ActivationCodes'
import {authHeader} from '../helpers/authHeader'

export default class UserManagement extends Component {
    componentDidMount() {
        this.getUsers()
        this.getRoles()
        this.getPermissions()
    }

    getUsers = () => {
        fetch('api/users/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    users: res.users
                })
            }
            console.log(res.users)
        })
    }
    getRoles = () => {
        fetch('api/users/roles/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    roles: res.roles
                })
            }
            console.log(res.roles)
        })
    }
    getPermissions = () => {
        fetch('api/users/permissions/all',{
            method: 'GET',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
            if( res.status == 200){
                this.setState({
                    permissions: res.permissions
                })
            }
            console.log(res.permissions)
        })
    }

  render() {
    return (
        <div className="row">
            <div className="col-12">
                <div className="card mb-2">
                    <div className="card-header">
                        <ul className="nav nav-pills card-header-pills pull-right" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" data-toggle="tab" 
                                    href="#tab-4">
                                    <i className="align-middle fe fe-users"></i>
                                    <span className="align-middle ml-2">Users</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href="#tab-5">
                                    <i className="align-middle fe fe-slack"></i>
                                    <span className="align-middle ml-2">Activation Codes</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" data-toggle="tab" 
                                    href="#tab-6">
                                    <i className="align-middle fe fe-file-text"></i>
                                    <span className="align-middle ml-2">Reports</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="card-body">
                        <div className="tab-content">
                            <div className="tab-pane fade show active" id="tab-4" role="tabpanel">
                                <Users roles={this.state.roles} permissions={this.state.permissions} users={this.state.users} />
                            </div>
                            <div className="tab-pane fade" id="tab-5" role="tabpanel">
                                <ActivationCodes roles={this.state.roles}/>
                            </div>
                            <div className="tab-pane fade" id="tab-6" role="tabpanel">
                                <center className="text-muted ml-2">Currently not available</center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }


  state = {
      users:[],
        roles: [
            {id: 1, name: 'Super user'},
            {id: 2, name: 'Admin'},
            {id: 3, name: 'Principal'},
            {id: 4, name: 'Head of porcurement department'},
            {id: 5, name: 'Head of department'},
            {id: 6, name: 'Tender Board'},
            {id: 7, name: 'Evaluation Comitee'},
            {id: 8, name: 'Lectures'}
        ],
        permissions: [
            {
                id: 1,
                name: "can manage users",
                descriptions: [
                    "user can activate and deactivate other users",
                    "user can generate activation codes for new users"
                ]
            },
            {
                id: 2,
                name: "can manage requests",
                descriptions: [
                    "user can can create and cancel requests",
                ]
            },
            {
                id: 3,
                name: "can manage workflow",
                descriptions: [
                    "can configure requests types and templates",
                    "can control people who can sign the documents"
                ]
            },
            {
                id: 4,
                name: "can configure system",
                descriptions: [
                    "can edit organization info, departments, departments categories, and departments sections",
                ]
            },
            {
                id: 5,
                name: "can aprove requests",
                descriptions: [
                    "can accept or reject request",
                ]
            }
        ]
    }
}
