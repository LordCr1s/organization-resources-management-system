import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Content/Home'
import NotFound from './NotFoundPage'
import Request from './Content/Request'
import UserManagement from './Content/UserManagement'
import TechnicianLogs from './Content/TechnicianLogs'
import RequestManagement from './Content/RequestManagement'
import Account from './Content/Account'
import GoodReceivedNotes from './Content/GoodReceivedNotes'
import AquisitionRequest from './Content/AquisitionRequest'
import PMUmaterials from './Content/PMUmaterials'
import ChatRoom from './Content/ChatRoom'
import CoversationContainer from './Content/CoversationContainer'
import Login from './Auth/Login'
import Register from './Auth/Register'
import Activation from './Auth/Activation'
import ForgetPassword from './Auth/ForgetPassword'
import ResetPassword from './Auth/ResetPassword'
import Verify from './Auth/Verify'
import TenderPortal from './Content/TenderPortal'
import Departments from './Content/Departments'
import Department from './Content/DepartmentStore/Department'
import Stores from './Content/Stores'

export default class Content extends Component {
  render() {
    return (
        <Switch>
            {/* Materials view pages */}
            <Route exact path='/materials' component={Home}/>


            {/* 
                BY NO MEANS THE UNCOMMENTED PARTS WILL CONTRIBUTE TO THE ERRORS OR BUGS YOU ENCOUNTER

                This route is commented because the web part of this feature is
                currently not needed

                Teaching materials requests management pages
                <Route exact path='/requests' component={Request}/>

                Chat room 
                <Route exact path='/chatroom' component={ChatRoom}/>

                Technician logs
                <Route exact path='/technicianlogs' component={TechnicianLogs}/>

            */}

            {/* User management pages */}
            <Route exact path='/usermanagement' component={UserManagement}/>

            

            {/* Conversation view */}
            <Route exact path='/conversation' component={CoversationContainer}/>


            {/* Account views */}
            <Route exact path='/useraccount' component={Account}/>

            {/* Good Received notes */}
            <Route exact path='/grn' component={GoodReceivedNotes}/>

            {/* Aquisition Requests */}
            <Route exact path='/aquisitions' component={AquisitionRequest}/>
            
            {/* Materials from pmu store view pages */}
            <Route exact path='/pmumaterials' component={PMUmaterials}/>

            {/* Requests flow management pages */}
            <Route exact path='/requestManagement' component={RequestManagement}/>

            {/* tender Portal view */}
            <Route exact path='/tenderportal' component={TenderPortal}/>

            {/* Departments sidebar link */}
            <Route exact path='/departments' component={Departments}/>

            {/* single department view */}
            <Route path='/department/:id' exact component={Department} />

            {/* Store sidebar link */}
            <Route exact path='/stores' component={Stores}/>

            {/* if there is no matching route show 404 */}
            <Route component={NotFound}/>

            {/* login view */}
            <Route exact path='/' component={Login}/>

            {/* register view */}
            <Route exact path='/register' component={Register}/>

             {/* forget password view */}
             <Route exact path='/forgetpassword' component={ForgetPassword}/>

              {/* reset password view */}
            <Route exact path='/resetpassword' component={ResetPassword}/>

             {/* email verification view */}
             <Route exact path='/verify' component={Verify}/>

              {/* registration activation view */}
            <Route exact path='/activation' component={Activation}/>

        </Switch>
    )
  }
}
