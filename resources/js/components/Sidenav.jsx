import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { authHeader } from  './helpers/authHeader'

export default class Sidenav extends Component {
    state={
        user:{},
        roles:[]
    }
    componentDidMount() {
      
        this.requestUserInformation()
          
      }
  
      requestUserInformation = () => {
          fetch('api/auth/me',{
            method: 'POST',
            headers: authHeader()
        })
        .then( res => res.json())
        .then( res => {
          if(res.status == 200){
            this.setState({
                user: res.user,
                roles:res.role
            })
          }
           
            console.log(this.state.user)
        })
      }
  render() {
    return (
        (!!localStorage.getItem('access_token') && !!localStorage.getItem('user')) ?  
        <React.Fragment>
            <nav className="sidebar">
                <div className="sidebar-content" id="sidebar">
                    <a className="sidebar-brand" href="">
                        <i className="align-middle fe fe-database mr-2 text-warning"></i>
                        <span className="align-middle mt-2 pt-1">OREMS</span>
                    </a>
                    <ul className="sidebar-nav">
                        <li className="sidebar-header">
                            OREMS navigations
                        </li>
                        <li className="sidebar-item active">
                            <Link to='/materials' className="sidebar-link">
                                <i className="align-middle fe fe-git-pull-request mr-2"></i> 
                                <span className="align-middle">Materials Tracking</span>
                            </Link>
                        </li>

                        {/* 
                            Teaching materials requests management pages

                            This route is commented because the web part of this feature is
                            currently not needed
                        
                            <li className="sidebar-item">
                                <Link to='/requests' className="sidebar-link">
                                    <i className="align-middle fe fe-feather mr-2"></i> <span className="align-middle">Materials Requests</span>
                                </Link>
                            </li>

                        */}
                        {
                            this.state.roles.map(role => (
                                role.name == 'HOD' || role.name == 'PR' ?

                                <li className="sidebar-item">
                                    <Link to='/usermanagement' className="sidebar-link">
                                        <i className="align-middle fe fe-user mr-2"></i> <span className="align-middle">User Management</span>
                                    </Link>
                                </li>: null
                            ))
                            
                        }
                        

                        {/* 
                            This route is commented because the web part of this feature is
                            currently not needed

                            <li className="sidebar-item">
                                <Link to='/chatroom' className="sidebar-link">
                                    <i className="align-middle fe fe-message-circle mr-2"></i> <span className="align-middle">Chat Room</span>
                                </Link>
                            </li>
                         
                            This route is commented because the web part of this feature is
                            currently not needed

                            <li className="sidebar-item">
                                <Link to='/technicianlogs' className="sidebar-link">
                                    <i className="align-middle fe fe-bookmark mr-2"></i> <span className="align-middle">Technician Logs</span>
                                </Link>
                            </li>
                        */}
                        
                        <li className="sidebar-item">
                            <Link to='/useraccount' className="sidebar-link">
                                <i className="align-middle fe fe-settings mr-2"></i> <span className="align-middle">Account</span>
                            </Link>
                        </li>
                        {/* <li className="sidebar-item">
                            <Link to='/aquisitions' className="sidebar-link">
                                <i className="fe fe-toggle-right mr-2"></i>
                                <span className="align-middle">Aquisition Requests</span>
                            </Link>
                        </li> */}
                        <li className="sidebar-item">
                            <Link to='/grn' className="sidebar-link">
                                <i className="align-middle fe fe-package mr-2"></i> <span className="align-middle">Report Generation</span>
                            </Link>
                        </li>
                        <li className="sidebar-item">
                            <Link to='/departments' className="sidebar-link">
                                <i className="align-middle fe fe-bookmark mr-2"></i> <span className="align-middle">Departments</span>
                            </Link>
                        </li>
                        <li className="sidebar-item">
                            <Link to='/requestManagement' className="sidebar-link">
                                <i className="align-middle fe fe-toggle-left mr-2"></i> <span className="align-middle">Request Management</span>
                            </Link>
                        </li>
                        <li className="sidebar-item">
                            <Link to='/pmumaterials' className="sidebar-link">
                                <i className="align-middle fe fe-list mr-2"></i> <span className="align-middle">PMU materials</span>
                            </Link>
                        </li>
                        <li className="sidebar-item">
                            <Link to='/stores' className="sidebar-link">
                                <i className="align-middle fe fe-bookmark mr-2"></i> <span className="align-middle">Stores</span>
                            </Link>
                        </li>
                        <li className="sidebar-item">
                            <Link to='/tenderportal' className="sidebar-link">
                                <i className="align-middle fe fe-feather mr-2"></i> 
                                <span className="align-middle">Tender Portal</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </React.Fragment>: null
    )
  }
}
