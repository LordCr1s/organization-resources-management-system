import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Register from './Register'
import Activation from './Activation'
import ForgetPassword from './ForgetPassword'
import ResetPassword from './ResetPassword'
import Verify from './Verify'


export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
          email: '',
          password: '',
          register: false,
          forgetpassword: false,
          login: true 
        }
        

        localStorage.clear();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let requestBody = {
            email: $('input[name=email]').val(), 
            password: $('input[name=password]').val()
        }

        fetch('/api/auth/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody) 
        })
        .then((resp) => resp.json())
        .then((resp) => {
            if(resp.status == 200){
                localStorage.setItem('access_token', resp.access_token);
                localStorage.setItem('user',JSON.stringify(resp.user));
                localStorage.setItem('roles',resp.roles );
                this.setState({redirect: true})
                console.log(resp.status)
            }
            console.log(resp.status)
        
        })
        .catch(error => console.log(error))
    }

    forget = (e)=>{
        e.preventDefault();
        this.setState({
            forgetpassword: true,
            login:false
        })
    }

    register = (e)=>{
        e.preventDefault();
        this.setState({
            register: true,
            login:false
        })
    }

render() {
    if (this.state.redirect) {
        return <Redirect to='/useraccount'/>;
        }
        if(this.state.login){
            return (
                <React.Fragment>
                    <div className="container h-100">
                        <div className="row h-100">
                            <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                                <div className="d-table-cell align-middle">
                                    <div className="text-center mt-4">
                                        <h1 className="h2">Welcome OREMS</h1>
                                        <p className="lead">
                                            Sign in to your account to continue
                                        </p>
                                    </div>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="m-sm-4">
                                                <div className="text-center">
                                                    <img src="img/dit_logo.jpg" alt="Chris Wood" className="img-fluid rounded-circle" width="132" height="132" />
                                                </div>
                                                <form onSubmit={this.handleSubmit}>
                                                    <div className="form-group">
                                                        <label>Email</label>
                                                        <input className="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Password</label>
                                                        <input className="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" />
                                                        <small>
                                                            <a  onClick={this.forget}>Forgot password?</a>
                                                        </small>
                                                    </div>
                                                    <div>
                                                        </div>
                                                        <div className="text-center mt-3">
                                                            <button type="submit" className="btn btn-lg btn-primary">Sign in</button>
                                                        </div>
                                                </form>
                                                <div className="text-center mt-3">
                                                    <a onClick={this.register}>Register here</a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </React.Fragment>
            )
        }else if(this.state.register){
            console.log('register')
            return(<Activation/>)
            
        }else if(this.state.forgetpassword){
            console.log('forgetpassword')
            return(<ForgetPassword/>) 
        }
        
    }

}