import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { authHeader } from './../helpers/authHeader'

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
          redirect: false
        };
    }

    handleSubmit=(e)=> {
        e.preventDefault();
        let requestBody = {
            email: $('input[name=email]').val(), 
            password: $('input[name=password]').val(),
            firstname: $('input[name=firstname]').val(),
            lastname: $('input[name=lastname]').val(), 
            surname: $('input[name=surname]').val(), 
            password_confirmation: $('input[name=password_confirmation]').val(),
            department_id: this.props.data.department,
            role_id: this.props.data.role, 
        }

        fetch('/api/auth/register', {
            method: 'POST',
            headers:authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status == 200){
                this.setState({
                    redirect: true,
                })
            }
        
        })
        .catch(error => console.log(error))
    }

    render() {
        if(this.state.redirect){
            return(<Redirect to='/'/>)
        }else{
        return (
            <React.Fragment>
                <div className="container h-100">
                    <div className="row h-100">
                        <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                            <div className="d-table-cell align-middle">

                                <div className="text-center mt-4">
                                    <h1 className="h2">OREMS</h1>
                                    <p className="lead">
                                        Finish up your registration to start using the system.
                                    </p>
                                </div>

                                <div className="card">
                                    <div className="card-body">
                                        <div className="m-sm-4">
                                            <form onSubmit={this.handleSubmit}>
                                                <div className="form-group">
                                                    <label>First Name</label>
                                                    <input className="form-control form-control-lg" type="text" name="firstname" placeholder="First Name" />
                                                </div>
                                                <div className="form-group">
                                                    <label>Last Name</label>
                                                    <input className="form-control form-control-lg" type="text" name="lastname" placeholder="Last Name" />
                                                </div>
                                                <div className="form-group">
                                                    <label>Surname</label>
                                                    <input className="form-control form-control-lg" type="text" name="surname" placeholder="Surname" />
                                                </div>                                              
                                                <div className="form-group">
                                                    <label>Email</label>
                                                    <input className="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" />
                                                </div>
                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <input className="form-control form-control-lg" type="password" name="password" placeholder="Enter password" />
                                                </div>
                                                <div className="form-group">
                                                    <label>Confrim Password</label>
                                                    <input className="form-control form-control-lg" type="password" name="password_confirmation" placeholder="Confirm your Password" />
                                                </div>
                                                <div className="text-center mt-3">
                                                    <button type="submit" className="btn btn-lg btn-primary">Register</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )}
    }
}