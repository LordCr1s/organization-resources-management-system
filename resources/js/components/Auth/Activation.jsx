import React, { Component } from 'react'
import { Redirect } from 'react-router'
import { authHeader } from './../helpers/authHeader'
import Register from './Register'

export default class Activation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            codeValid: false,
            data:[],
            error:''
        };
    }

    handleSubmit = (e) =>{
        e.preventDefault();
        let requestBody = {
            activation_code: $('input[name=activation_code]').val(), 
        }

        fetch('/api/auth/activation', {
            method: 'POST',
            headers: authHeader(),
            body: JSON.stringify(requestBody) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if(data.status == 200){
                this.setState({
                    codeValid: true,
                    data: data.data
                })
            }else{
                this.setState({
                   error: 'invalid activation code'
                })
            }
        })
        .catch(error => console.log(error))
    }

    render() {
        if(!this.state.codeValid){
            return (
                <React.Fragment>
                    <div className="container h-100">
                        <div className="row h-100">
                            <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                                <div className="d-table-cell align-middle">
    
                                    <div className="text-center mt-4">
                                        <h1 className="h2"> Activation Code</h1>
                                        <p className="lead">
                                            Enter your Activation inorder to get the registration page.
                                        </p>
                                        {
                                            this.state.error == ''? null:
                                            <div class="alert alert-primary alert-outline alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
											<div class="alert-icon">
												<i class="far fa-fw fa-bell"></i>
											</div>
											<div class="alert-message">
												<strong>Error</strong> {this.state.error} Try again
											</div>
										</div>
                                        }
                                    </div>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="m-sm-4">
                                                <form onSubmit={this.handleSubmit}>
                                                    <div className="form-group">
                                                        <label>Activation Code</label>
                                                        <input className="form-control form-control-lg" type="text" name="activation_code" placeholder="Enter your Activation Code" />
                                                    </div>
                                                    <div className="text-center mt-3">
                                                        <button type="submit" className="btn btn-lg btn-primary">Verify Code</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            )
        }else{
            return( <Register data={this.state.data}/>)
        }
        
    }
}
