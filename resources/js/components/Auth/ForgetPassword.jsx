import React, { Component } from 'react'
import { Redirect } from 'react-router'

export default class ForgetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:false
        };
    }

    handleSubmit=(e)=> {
        e.preventDefault();
        let requestBody = {
            email: $('input[name=email]').val(), 
        }

        fetch('/api/forgetpassword', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody) 
        })
        .then((resp) => resp.json())
        .then((data) => {
            if( data.status == 200){
                this.setState({
                    redirect: true
                })
            }
        })
        .catch(error => console.log(error))
    }

    render() {
        if(this.state.redirect){
            return(<Redirect to='/'/>)
        }
        return (
            <React.Fragment>
               	<div className="container h-100">
                    <div className="row h-100">
                        <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                            <div className="d-table-cell align-middle">
                                <div className="text-center mt-4">
                                    <h1 className="h2">Forget password</h1>
                                    <p className="lead">
                                        Enter your email to get the link for password reset.
                                    </p>
                                </div>
                                <div className="card">
                                    <div className="card-body">
                                        <div className="m-sm-4">
                                            <form onSubmit={this.handleSubmit}>
                                                <div className="form-group">
                                                    <label>Email</label>
                                                    <input className="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" />
                                                </div>
                                                <div className="text-center mt-3">
                                                    <button type="submit" className="btn btn-lg btn-primary">Send Password Reset Link</button> 
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
		        </div>
            </React.Fragment>

        )
    }
}