import { BrowserRouter } from 'react-router-dom'
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Container from './Container'
import { Provider } from 'react-redux'
import { tmms_store } from './Data'

export default class Root extends Component {

    render() {
        return (
            <BrowserRouter>
                <Provider store={tmms_store}>
                    <Container />
                </Provider>
            </BrowserRouter>
        );
    }
}

const renderJpgs = () => {
    if (document.getElementById('root')) {
        ReactDOM.render(<Root />, document.getElementById('root'));
    }
}
renderJpgs()

// update the the DOM whenever the store is updated
// jpgs_store.subscribe(renderJpgs)

